from glumpy import app, gl, glm, gloo, log
from glumpy.gloo import *

from model.SML import SML
from render import programs, glooda
from render.rendercontext import RenderContext
from utils import *
import torch
import logging


class Renderer(app.window.EventDispatcher):
    # Some properties for convenience. (Basically because we can't subclass app.Window)
    @property
    def width(self):
        return self._window.width

    @property
    def height(self):
        return self._window.height

    @staticmethod
    def get_plane_faces(model : SML):
        # We append the plane vertices after the 3d body model vertices
        # Hence they have to be offset by the number of vertices.
        plane_faces = model.f.new_tensor([[0, 1, 2], [2, 1, 3]]) + model.num_vertices
        return plane_faces

    def __init__(self, model, camera_params, *args, **kwargs):
        super().__init__()
        # Try to make the window the same size as the camera parameters suggests.
        # This may not be possible if the screen is too small, but it is rendered in a framebuffer offscreen
        #  so it is not a problem. (This is an edge case that will likely result in bugs where renderer.width
        #  is used instead of camera_params.width, or height.) #FIXME?
        self._window = app.Window(camera_params.width, camera_params.height, *args, **kwargs)
        self._window.attach(self)
        if not self._window._visible:
            self._window.swap = lambda: None # This saves around 10-40% time.
        log.setLevel(logging.WARNING) # No reason to print into, "Running at full speed each time it renders."
        # Very important. Has to be done after GL context is created (& before we create the interop objects, I think.)
        self._cuda_context = glooda.__init__()
        # WARNING: We have not taken multi-threaded OpenGL contexts into account. One may have to initialize a seperate
        # Cuda-OpenGL interop context for each OpenGL context.

        # FRAMEBUFFER OBJECT
        self.channels = 1
        self.dtype = np.float32 # np.uint8, np.uint32, np.float32
        format = gloo.Texture._gpu_float_formats[self.channels] # Make sure GPU uses floats. (For best bit depth)
        #if np.issubdtype(self.dtype, np.integer):
        #    format = gloo.Texture._gpu_formats[self.channels]
        depth = ColorBuffer(self.width, self.height, format)
        fb_depth = DepthBuffer(self.width, self.height, gl.GL_DEPTH_COMPONENT32F) # Some say its overkill. But it's not really slower.
        self._framebuffer = FrameBuffer(color=[depth], depth=fb_depth)

        model_faces = model.f
        plane_faces = Renderer.get_plane_faces(model)
        self._indexbuffer = np.uint32(to_numpy(torch.cat([model_faces, plane_faces]))).view(IndexBuffer)
        self._body_indices = np.uint32(to_numpy(model_faces)).view(IndexBuffer)
        self._plane_indices = np.uint32(to_numpy(plane_faces)).view(IndexBuffer)

        # PROGRAM
        self._program = programs.BaseProgram(
                programs.vertex_shader_depth,
                programs.fragment_shader_depth)
        dtype = [attribute.dtype for attribute in self._program._attributes.values()]
        self._vertexbuffer = glooda.DynamicBuffer(np.zeros(0, dtype).view(glooda.CudaVertexBuffer))
        self._render_context = RenderContext(self.width, self.height, self.channels, self.dtype) # 1 because we use 1 channel only.

        # 0.1, 10 from https://www.intelrealsense.com/depth-camera-d435i/
        self.near = 0.3
        self.far = 2.5 # Used in PoseDataset multiple places.
        self._projection = camera_params.perspective(self.near, self.far)
        self._viewport = camera_params.viewport()
        # We could also get viewport parameters from the OpenGL API.
        # Source: https://www.khronos.org/opengl/wiki/Vertex_Post-Processing#Viewport_transform
        # self._program['u_zNear'] = near
        self._program['u_zFar'] = self.far
        self._program['u_P'] = self._projection
        self.camera_params = camera_params

    def project_points(self, points):
        return self.camera_params.project_points(self._projection, self._viewport, points)

    def unproject_points(self, points):
        return self.camera_params.unproject_points(self._projection, self._viewport, points)

    def project_z(self, z):
        return self.camera_params.project_z(self._projection, self._viewport, z)

    def on_close(self):
        exit() # Let the system handle any clean-up. (OpenGL and CUDA contexts)

    def on_init(self):
        #gl.glEnable(gl.GL_CULL_FACE)  # Do not draw away-facing triangles. (i.e. the backside of the baby.)
        #gl.glFrontFace(gl.GL_CW) # Needed because we don't flip image anymore I think.
        # Culling faces fails if we mirror the model, as with flip augmentation.
        gl.glEnable(gl.GL_DEPTH_TEST)  # Do not draw occluded triangles.
        #gl.glPolygonMode(gl.GL_FRONT_AND_BACK, gl.GL_LINE) # Draw wireframe.
        # enviro: PYOPENGL_ERROR_CHECKING=0 for a little more performance.

    def on_draw(self, dt):
        if self._render_context.full():
            app.quit()
            return None

        self._framebuffer.activate()
        self._window.clear()
        self._program.draw_base(self._render_context.next(), indices=self._indexbuffer)
        self._render_context.put() # Must be done while framebuffer is still active.
        if self._window._visible:
            if False:
                # This will reveal if the body clips through the plane.
                self._window.clear()
                self._program.draw_base(self._render_context.next(), indices=self._plane_indices)
                gl.glClear(gl.GL_COLOR_BUFFER_BIT)
                gl.glDepthFunc(gl.GL_GREATER)
                self._program.draw_base(self._render_context.next(), indices=self._body_indices)
                gl.glDepthFunc(gl.GL_LESS)

            # TODO Blit the framebuffer at 60 frames per. second. for performance
            # Detach framebuffer as draw target to draw to screen.
            gl.glBindFramebuffer(gl.GL_DRAW_FRAMEBUFFER, 0) # 0 is the handle of default framebuffer.
            gl.glBlitFramebuffer(0, 0, self.width, self.height,
                                 0, 0, self.width, self.height,
                                 gl.GL_COLOR_BUFFER_BIT,
                                 gl.GL_NEAREST)
        self._framebuffer.deactivate()

    def render(self, vertex_batch, print_framerate=False):
        dim = vertex_batch.shape[-1]
        num_vertices = vertex_batch.shape[-2]
        num_poses = np.prod(vertex_batch.shape[:-2])

        vertex_batch = vertex_batch.reshape(-1, dim)
        self._program.bind(self._vertexbuffer.update(vertex_batch))

        self._render_context.update(num_poses, num_vertices)

        if print_framerate:
            with get_framerate(num_poses):
                app.run()
        else:
            app.run()

        return self._render_context.result()

def plot1():
    device = torch.device('cpu')  # torch.device('cuda')
    dtype = torch.float
    sparse = True

    if dtype is torch.half:
        sparse = False  # sparse Half Tensors are not supported (yet).

    from dataset.minirgbd import MINIRGBDDataset
    from render.cameraparameters import CameraParameters
    import matplotlib.pyplot as plt
    import cv2

    dataset = MINIRGBDDataset()

    pose, trans, beta, plane = dataset[0]

    model = SML(nose_vertex=False).to(device, dtype)

    print(pose.shape, trans.shape, beta.shape, plane.shape)
    transfer = lambda t: torch.as_tensor(t, device=device, dtype=dtype)

    cp = CameraParameters(640, 480, preset='MINIRGBD')
    renderer = Renderer(model, cp)

    def render(pose, trans, beta, planes, simplify=False):
        planes = transfer(planes)
        inputs = tuple(map(transfer, (beta[None], pose, trans)))
        vertices, Jtr = model(*inputs, simplify=simplify)

        # Get their position in 3d-space.
        far_edges = np.array(
            [[0, 0, 1, 1],  # far corner 1
             [0, 1, 1, 1],  # far corner 2
             [1, 0, 1, 1],  # far corner 3
             [1, 1, 1, 1]], np.float64)  # cube_end corner 4
        far_edges[:, :2] *= renderer.width, renderer.height
        far_edges = transfer(renderer.unproject_points(far_edges))
        # Add some plane verts such that the indexbuffer does not access memory
        #  that does not exists. (Would probably result in shinanigans)
        z = torch.tensordot(planes[:, :2], far_edges[:, :2], ([1], [1]))  # a*x+b*y
        z = (-planes[:, 3].unsqueeze(-1) - z) / planes[:, 2].unsqueeze(-1)  # planes[:, -1] = -d

        far_edges = far_edges.unsqueeze(0).expand(len(z), -1,
                                                  -1).clone()  # This doesn't work without clone. (Worst bug)
        # Now z.shape = [N, 4] and far_edges.shape = [N, 4, 4].
        far_edges[:, :, 2] = z
        # Now that we have the far edges of the planes for each sequence we want to copy.
        # them to each frame in the sequences, concatenating them to the vertex set.
        # far_edges = far_edges.unsqueeze(1).expand(*vertices.shape[:1], -1, -1)  # [N, self._seq_len, 4, 4]
        vertices = torch.cat([vertices, far_edges], 1)  # [N, self._seq_len, 6890+4, 4]

        # vertices[..., :3] += transfer(trans) # Add translation
        image = np.copy(renderer.render(vertices))[0, ..., 0]

        Jtr2d = renderer.project_points(Jtr)
        image = image.astype(np.float) * renderer.far
        return image, Jtr, Jtr2d

    # torch.Size([1000, 6894, 4])
    depth, _, _ = render(pose, trans, beta, plane[None])
    true_depth = cv2.imread(os.path.join('MINI-RGBD_web', '01', 'depth', 'syn_00000_depth.png'), cv2.IMREAD_UNCHANGED)
    true_depth = true_depth.astype(np.float) / 1000

    if False:
        plt.imshow(depth, cmap='gray')
        plt.show()
        exit()

    fig, axes = plt.subplots(nrows=1, ncols=2)
    # plt.tight_layout()
    # plt.subplots_adjust(left=0, right=1)
    fig.subplots_adjust(left=0.05, right=0.95)
    vmin = min(true_depth.min(), depth.min())
    vmax = max(true_depth.max(), depth.max())
    axes[0].imshow(true_depth, cmap='gray', vmin=vmin, vmax=vmax, interpolation='none')
    im = axes[1].imshow(depth, cmap='gray', vmin=vmin, vmax=vmax, interpolation='none')

    axes[0].set_xticks([])
    axes[0].set_yticks([])

    axes[1].set_xticks([640])
    axes[1].set_yticks([0])
    axes[1].set_yticklabels([480])

    for ax in axes:
        # ax.axis('image')

        ax.set_aspect("equal")
        ax.yaxis.set_ticks_position('right')

    axes[0].set_title('MINI-RGBD')
    axes[1].set_title('Our rendition')
    # plt.xticks([640])
    # plt.yticks(480)
    plt.draw()
    p0 = axes[0].get_position().get_points().flatten()
    p1 = axes[1].get_position().get_points().flatten()
    cbar_ax = fig.add_axes([p0[0], p0[1] - 0.05 * 2, p1[2] - p0[0] - 0.05, 0.05])
    fig.colorbar(im, ax=axes.ravel().tolist(), cax=cbar_ax, orientation='horizontal').set_label(
        'depth [m]')  # ax=axes.ravel().tolist()
    plt.savefig('oursvsminirgbd.png', bbox_inches='tight')
    plt.show()

if __name__ == "__main__":
    # torch.set_default_tensor_type(torch.cuda.FloatTensor)
    plot1()
    device = torch.device('cpu')  # torch.device('cuda')
    dtype = torch.float
    sparse = True

    if dtype is torch.half:
        sparse = False  # sparse Half Tensors are not supported (yet).

    from dataset.minirgbd import MINIRGBDDataset
    from render.cameraparameters import CameraParameters
    import matplotlib.pyplot as plt
    import torch.nn.functional as F
    import cv2

    dataset = MINIRGBDDataset()

    pose, trans, beta, plane = dataset[0]

    model = SML(nose_vertex=False).to(device, dtype)

    print(pose.shape, trans.shape, beta.shape, plane.shape)
    transfer = lambda t: torch.as_tensor(t, device=device, dtype=dtype)

    cp = CameraParameters(640, 480, preset='MINIRGBD')
    renderer = Renderer(model, cp)

    def render(pose, trans, beta, planes, simplify=False):
        planes = transfer(planes)
        inputs = tuple(map(transfer, (beta[None], pose, trans)))
        vertices, Jtr = model(*inputs, simplify=simplify)

        # Get their position in 3d-space.
        far_edges = np.array(
            [[0, 0, 1, 1],  # far corner 1
             [0, 1, 1, 1],  # far corner 2
             [1, 0, 1, 1],  # far corner 3
             [1, 1, 1, 1]], np.float64)  # cube_end corner 4
        far_edges[:, :2] *= renderer.width, renderer.height
        far_edges = transfer(renderer.unproject_points(far_edges))
        # Add some plane verts such that the indexbuffer does not access memory
        #  that does not exists. (Would probably result in shinanigans)
        z = torch.tensordot(planes[:, :2], far_edges[:, :2], ([1], [1]))  # a*x+b*y
        z = (-planes[:, 3].unsqueeze(-1) - z) / planes[:, 2].unsqueeze(-1)  # planes[:, -1] = -d

        far_edges = far_edges.unsqueeze(0).expand(len(z), -1, -1).clone()  # This doesn't work without clone. (Worst bug)
        # Now z.shape = [N, 4] and far_edges.shape = [N, 4, 4].
        far_edges[:, :, 2] = z
        # Now that we have the far edges of the planes for each sequence we want to copy.
        # them to each frame in the sequences, concatenating them to the vertex set.
        #far_edges = far_edges.unsqueeze(1).expand(*vertices.shape[:1], -1, -1)  # [N, self._seq_len, 4, 4]
        vertices = torch.cat([vertices, far_edges], 1)  # [N, self._seq_len, 6890+4, 4]

        #vertices[..., :3] += transfer(trans) # Add translation
        image = np.copy(renderer.render(vertices))[0, ..., 0]

        Jtr2d = renderer.project_points(Jtr)
        image = image.astype(np.float) * renderer.far
        return image, Jtr, Jtr2d

    # torch.Size([1000, 6894, 4])
    depth, _, _ = render(pose, trans, beta, plane[None])
    true_depth = cv2.imread(os.path.join('MINI-RGBD_web', '01', 'depth', 'syn_00000_depth.png'), cv2.IMREAD_UNCHANGED)
    true_depth = true_depth.astype(np.float) / 1000

    depth, true_depth = map(transfer, (depth, true_depth))
    true_depth = F.interpolate(true_depth[None, None], scale_factor=1/2, mode='area')[0, 0]
    depth = F.interpolate(depth[None, None], scale_factor=1/2, mode='area')[0, 0]

    noise = (torch.rand_like(depth) - 0.5) / 100  # +- 0.5 cm noise
    depth += noise / renderer.far

    fig, axes = plt.subplots(nrows=1, ncols=2)
    #plt.tight_layout()
    #plt.subplots_adjust(left=0, right=1)
    fig.subplots_adjust(left=0.05, right=0.95)
    vmin = min(true_depth.min(), depth.min())
    vmax = max(true_depth.max(), depth.max())
    axes[0].imshow(true_depth, cmap='gray', vmin=vmin, vmax=vmax, interpolation='none')
    im = axes[1].imshow(depth, cmap='gray', vmin=vmin, vmax=vmax, interpolation='none')

    axes[0].set_xticks([])
    axes[0].set_yticks([])

    axes[1].set_xticks([depth.shape[1]])
    axes[1].set_yticks([0])
    print(depth.shape)
    axes[1].set_yticklabels([depth.shape[0]])

    for ax in axes:
        #ax.axis('image')

        ax.set_aspect("equal")
        ax.yaxis.set_ticks_position('right')

    axes[0].set_title('MINI-RGBD')
    axes[1].set_title('Our rendition')
    #plt.xticks([640])
    #plt.yticks(480)
    plt.draw()
    p0 = axes[0].get_position().get_points().flatten()
    p1 = axes[1].get_position().get_points().flatten()
    cbar_ax = fig.add_axes([p0[0]-0.02, p0[1]-0.05*2, p1[2]-p0[0]-0.05, 0.05])
    fig.colorbar(im, ax=axes.ravel().tolist(), cax=cbar_ax, orientation='horizontal').set_label('depth [m]') # ax=axes.ravel().tolist()
    #plt.savefig('oursvsminirgbd.png', bbox_inches='tight')
    plt.show()

    exit()
    for i in range(100):
        cv2.putText(true_depth, 'Hesse et al.', (0, 35), cv2.FONT_HERSHEY_SIMPLEX, 1.5, color=255, thickness=2)
        img = cv2.imshow('mask', true_depth)
        cv2.waitKey(0)
        cv2.imshow('mask', np.not_equal(depth, 0).astype(np.float))
        cv2.waitKey(0)
    print(image.shape)
    plt.imshow(image)
    plt.show()
    # Load pose data.
    # Render.