import numpy as np
import torch
from glumpy import glm


class CameraParameters(object):
    # width=640//2, height=480//2
    def __init__(self, width=368, height=368, focalx=None, focaly=None,
                 centerx=None, centery=None, preset=None):
        # preset should be 'MINIRGBD' or 'MIKKEL'
        # Since CPM was originally trained on a resolution of 368x368 and because of memory limits
        #  we try to use resolution not as large as 640x480, but settle with half.
        scalex = width / 640
        scaley = height / 480

        # The presets are created so they adapt if the resolution is different.
        if preset == 'MIKKEL':
            # Mikkel intrinsic camera parameters (Kinect camera)
            centerx, centery = width/2, height/2

            focaly = 571.2653 * scalex
            focalx = 571.2653 * scaley
            #focaly *= -1
        elif preset == 'MINIRGBD':
            # The preset is made so they are indifferent to resizing as long as the aspect
            #  ratio is preserved. (Should still be 4/3)
            centerx = (322.22048191353628 - 640/2) * scalex + width/2
            centery = (237.46785983766890 - 480/2) * scaley + height/2
            focalx = 588.67905803875317 * width / 640
            focaly = 590.25690113005601 * height / 480
        else:
            centerx = width / 2 if centerx is None else centerx
            centery = height / 2 if centery is None else centery
            focalx = 580 * scalex if focalx is None else focalx
            focaly = 580 * scalex if focaly is None else focaly # Also multiply by scalex

        self.focalx, self.focaly = focalx, focaly
        self.centerx, self.centery = centerx, centery
        self.width, self.height = width, height

    def viewport(self):
        # OpenGL matrices are laid out in Column Major format.
        # The returned matrix should be pre-multiplied on vectors.
        return glm.scale(glm.translate(np.eye(4), 1, 1, 1), self.width / 2, self.height / 2, 1 / 2)

    def resolution(self):
        return self.width, self.height

    def perspective(self, near, far):
        # OpenGL matrices are laid out in Column Major format.
        # https://en.wikipedia.org/wiki/Row-_and_column-major_order#Programming_languages_and_libraries
        # Hence the returned perspective matrix here should be pre-multiplied on vectors.
        # Source: https://stackoverflow.com/a/22064917 but with no shear (s)
        return np.array([[2 * self.focalx / self.width, 0, 0, 0],
                         [0, 2 * self.focaly / self.height, 0, 0],
                         [2 * (self.centerx / self.width) - 1,
                          2 * (self.centery / self.height) - 1,
                          (far + near) / (far - near), 1],
                         [0, 0, 2 * far * near / (near - far), 0]], dtype=np.float64)

    # It should probably be a module-level function instead of staticmethod.
    # A holdover from Java, where functions are usually class-based.

    # https://www.khronos.org/opengl/wiki/GluProject_and_gluUnProject_code
    @staticmethod
    def project_points(projection, viewport, points):
        # OpenGL matrices are column-major.
        if isinstance(points, torch.Tensor):
            projection = points.new_tensor(projection)
            viewport = points.new_tensor(viewport)

        points2d = points @ projection
        points2d = points2d / points2d[..., None, -1]  # perspective divide.
        # https://en.wikibooks.org/wiki/GLSL_Programming/Rasterization - center of pixels
        # https://www.khronos.org/opengl/wiki/GluProject_and_gluUnProject_code
        points2d = points2d @ viewport

        return points2d

    def unproject_points(self, projection, viewport, points):
        # OpenGL matrices are column-major.
        proj_viewport_inv = np.linalg.inv(projection @ viewport)
        if isinstance(points, torch.Tensor):
            proj_viewport_inv = points.new_tensor(proj_viewport_inv)

        points3d = points @ proj_viewport_inv
        points3d = points3d / points3d[..., None, -1]  # perspective divide.

        return points3d

    def project_z(self, projection, viewport, z):
        # Projects only the depth values.
        if isinstance(z, torch.Tensor):
            projection = z.new_tensor(projection)
            viewport = z.new_tensor(viewport)

        z = (projection[2, 2] * z + projection[3, 2]) / z
        z = z * viewport[2, 2] + viewport[3, 2]
        return z