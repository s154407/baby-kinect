from glumpy import gloo, gl
import numpy as np

vertex_shader_depth = """
    uniform mat4 u_P; // The projection matrix.
    attribute vec4 a_position; // Vertex position.
    varying vec3 v_position;

    // TODO: https://www.khronos.org/opengl/wiki/GLSL_Optimizations
    void main()
    {
        v_position = a_position.xyz;
        gl_Position = u_P * a_position;
    } """

# Used information from https://learnopengl.com/Advanced-OpenGL/Depth-testing
fragment_shader_depth = """
    uniform float u_zFar;
    varying vec3 v_position;
    
    void main()
    {
        float z = v_position.z/u_zFar;
        // A simple optimization would be to store the reciprocal of u_zFar and multiply.
        // https://stackoverflow.com/questions/226465/should-i-use-multiplication-or-division/226610#226610
        
        // The depth can actually be stored directly in the depthbuffer:
        //gl_FragDepth = z;
        // But downsides are that it disables some optimizations and also the depth buffer cant be multisampled.
        
        gl_FragColor = vec4(vec3(z), 1.0);
        // gl_FragData[0] is the same as gl_FragColor
        //float distance = length(v_position) / u_zFar;
        // gl_FragData[0] = vec4(z, distance, 1.0, 1.0); // Third coord could be used for mask.
        //gl_FragData[1] = vec4(1.0); // TODO: Color output
        //gl_FragData[2] = vec4(0.5); // Could be used to output segmentation
        // Important: The number of channels must be the same for all ColorBuffers.
        
        // To render a segmentated/masked version one could just add a color attribute to the vertices.
        //  This will create a lot of redundant data, as only the position attribute changes between frames.
        //  A better approach would be to just render each body-part separately with the advanced function
        //   glMultiDrawElementsIndirect
        //  This way multiple geometries are drawn in a single call reusing a command already placed on the GPU.
        //  With this method it will be very fast to render a frame (one api call) and it will only use the
        //  additional memory of the draw commands, but they only have to be uploaded once and can be referred to
        //  henceforth indrectly (between rendering sessions even).
    } """

class BaseProgram(gloo.Program):
    def draw_base(self, base, mode=gl.GL_TRIANGLES, indices=None):
        self.activate()
        if isinstance(indices, gloo.IndexBuffer):
            indices.activate()
            gltypes = {np.dtype(np.uint8): gl.GL_UNSIGNED_BYTE,
                       np.dtype(np.uint16): gl.GL_UNSIGNED_SHORT,
                       np.dtype(np.uint32): gl.GL_UNSIGNED_INT}
            gl.glDrawElementsBaseVertex(mode, indices.size, gltypes[indices.dtype], None, base)
            indices.deactivate()
        else:
            print("BaseProgram.draw_base with indices not specified: This is not implemented!")
        self.deactivate()