import OpenGL.error
import numpy as np
from glumpy import gloo, gl
from glumpy.gloo import ColorBuffer

from render import glooda

class RenderContext(object):
    def __init__(self, width, height, channels=4, dtype=np.uint8):
        dtype = np.dtype(dtype)
        self._idx = 0
        self._num_poses = 0
        self._num_vertices = 0
        self._pixelbuffer = np.zeros((0, height, width, channels), dtype).view(glooda.PixelBuffer)

        self._gpu_format = gloo.Texture._gpu_formats[channels]
        self._gtype = gloo.Texture._gtypes[dtype]

    def colorbuffer(self):
        return ColorBuffer(self._pixelbuffer.shape[2],
                         self._pixelbuffer.shape[1],
                         self._gpu_format)

    def update(self, num_poses, num_vertices):
        self._idx = 0
        self._num_poses = num_poses
        self._num_vertices = num_vertices
        if self._pixelbuffer.shape[0] < num_poses:
            # need resize
            newshape = (num_poses,) + self._pixelbuffer.shape[1:]
            # We experienced issues with OpenGL if the size is over 2^31-1
            numbytes = self._pixelbuffer.dtype.itemsize * np.prod(newshape[1:], dtype=np.uint64)
            print(num_poses*numbytes)
            if int(num_poses*numbytes) >= (1 << 31) - 1:
                print('Number of bytes in pixelbuffer', num_poses*numbytes,
                      'is greater than 2^31 - 1. This may cause issues.',
                      'It will most likely cause issues if it is also over 2^32.')
                print('Try with a render_batch_size of ', int((1<<30)/numbytes))


            self._pixelbuffer.delete()
            self._pixelbuffer = np.zeros(newshape, self._pixelbuffer.dtype).view(type(self._pixelbuffer))

    def put(self):
        self._pixelbuffer.activate()
        try:
            # ReadPixels stores in PixelBuffer if one is active.
            gl.glReadPixels(0, 0,
                            self._pixelbuffer.shape[2], self._pixelbuffer.shape[1],
                            self._gpu_format, self._gtype, self._idx * self._pixelbuffer.strides[0])
        except OpenGL.error.GLError as e:
            # (Bug should be fixed now by the few first lines in Renderer.on_draw)
            print('Error in pixelbuffer!')
            print(self._idx, self._num_poses, self._num_vertices)
            raise e
        finally:
            self._pixelbuffer.deactivate()
        self._idx += 1

    def full(self):
        return self._idx >= self._num_poses

    def result(self):
        return self._pixelbuffer[:self._num_poses].read()

    def next(self):
        return self._idx * self._num_vertices

    def delete(self):
        self._pixelbuffer.delete()