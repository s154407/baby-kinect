from glumpy import gloo, gl, log
import numpy as np
import torch
from utils import to_numpy, timing
from contextlib import contextmanager

# Makes sense assuming that Cuda access follows same pattern as opengl access.
read_usage = [gl.GL_STATIC_READ, gl.GL_DYNAMIC_READ, gl.GL_STREAM_READ, 'READ']
write_usage = [gl.GL_STATIC_DRAW, gl.GL_DYNAMIC_DRAW, gl.GL_STREAM_DRAW, 'WRITE']
any_usage = [None, 'READWRITE']

CUDA_FLAGS = dict.fromkeys(read_usage + write_usage + any_usage)  # Map all to None.

cuda_enabled = torch.cuda.is_available()
if cuda_enabled:
    import pycuda.driver as cuda
    import pycuda.gl as cudagl
    from pycuda.gl import graphics_map_flags
    # Add values to entries.
    CUDA_FLAGS.update(dict.fromkeys(read_usage, graphics_map_flags.READ_ONLY))
    CUDA_FLAGS.update(dict.fromkeys(write_usage, graphics_map_flags.WRITE_DISCARD))
    CUDA_FLAGS.update(dict.fromkeys(any_usage, graphics_map_flags.NONE))

cuda_context = None
def __init__():
    # Must be initialized after a OpenGL context is created! Usage example:
    # window = app.Window(...) # Creates OpenGL context.
    # glooda.__init__() # Enable interop.
    global cuda_context
    if cuda_enabled and cuda_context is None:
        cuda_context = CudaContext()
    return cuda_context

def nbytes(arr):
    if isinstance(arr, torch.Tensor):
        return arr.element_size() * arr.nelement()
    else:
        return arr.nbytes

class CudaContext(object):
    # Fixes bugs from hell.
    # Use "with context:" when operating with pycuda.
    #  Make sure not to use pytorch, and especially - do not try to create/allocate new tensors in this scope.
    #   It will throw an error ONLY in case PyTorch tries to allocate memory, which it rarely does.

    def __init__(self):
        cuda.init()
        # noinspection PyUnresolvedReferences
        from pycuda.tools import make_default_context
        self.context = make_default_context(lambda dev: cudagl.make_context(dev))
        try:
            self.device = self.context.get_device()
            self.pytorch_device = torch.device('cuda')
            # If one must ensure they run on the same device, check out:
            #  https://stackoverflow.com/questions/52815708/order-of-cuda-devices
        finally:
            # Since make_default_context pushes automatically, we must undo this.
            self.context.pop()

    def __enter__(self):
        self.context.push()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.context.pop()

class PixelBuffer(gloo.Buffer):
    def __init__(self):
        gloo.Buffer.__init__(self, gl.GL_PIXEL_PACK_BUFFER, gl.GL_STREAM_READ)

    def read(self):
        # Sync. read from GPU into self.
        offset = 0
        length = self.nbytes
        self.activate()
        gl.glGetBufferSubData(self.target, offset, length, self.ravel().view(np.ubyte))
        self.deactivate()
        return self




class CudaBuffer(gloo.Buffer):
    def __init__(self, target, usage):
        gloo.Buffer.__init__(self, target, usage)
        self._cuda_registration = None
        self._cuda_flags = CUDA_FLAGS[usage]

    @property
    def cuda_registration(self):
        if isinstance(self.base, CudaBuffer):
            return self.base._cuda_registration
        else:
            return self._cuda_registration

    def _create(self):
        """ Create buffer on GPU, and register with CUDA. """
        super()._create()
        if cuda_context is not None:
            log.debug("GPU: CUDA registering buffer (id=%d)" % self._id)
            # WRITE_DISCARD means we may not read with CUDA, only write.
            with cuda_context:
                self._cuda_registration = cudagl.RegisteredBuffer(
                    int(self._handle), self._cuda_flags)

    def read_sync(self):
        # Sync. read from GPU into self. (We will probably only use for debugging.)
        offset = 0
        length = self.nbytes
        self.activate()
        gl.glGetBufferSubData(self.target, offset, length, self.ravel().view(np.ubyte))
        self.deactivate()
        return self

    def _delete(self):
        """ CUDA unregister and then delete buffer from GPU. """
        if self._cuda_registration is not None:
            with cuda_context:
                self._cuda_registration.unregister()
            self._cuda_registration = None
        super()._delete()

    @contextmanager
    def map_cuda(self):
        # Returns a pointer to the OpenGL (GPU) memory that can be used by CUDA.
        torch.cuda.synchronize()
        with cuda_context:
            if self.cuda_registration is None:
                self.activate()  # Force creation
                self.deactivate()

            mapping = self.cuda_registration.map()
            ptr, size = mapping.device_ptr_and_size()
            #assert size == self.nbytes # FIXME
            yield ptr
            mapping.unmap()  # Unmap, so OpenGL may use the memory again. (This synchronizes cuda)

    @contextmanager
    def map_gl(self):
        # Returns af pointer to the OpenGL (CPU) memory that can be used by numpy.
        ptr = gl.glMapBuffer(self.target, gl.GL_READ_ONLY)
        yield ptr
        gl.glUnmapBuffer(self.target)

    @timing
    def read(self, out=None):
        #print(self.base.cuda_registration, self._extents, self.base._extents)
        #print(self.cuda_registration, self._extents, self._extents)
        if cuda_context is None or out is not None and not out.is_cuda:
            print(type(out))
            return torch.as_tensor(self.read_sync())

        if out is None:
            out = torch.zeros(self.shape, dtype=torch.uint8, device=cuda_context.pytorch_device)

        with self.map_cuda() as ptr:
            if not out.is_contiguous():
                # Also the case where the tensor is on another device is untested. (May need peer transfer)
                print('Warning: Noncontiguous inputs are not handled correctly yet!')
            cuda.memcpy_dtod(out.data_ptr(), ptr, self.nbytes)
        return out

    def write(self, tensor):
        # If tensor is on CPU: Sync. write to self, transfered to GPU when self is activated.
        # If tensor is on GPU: Async. write to GPU. Self is unchanged.

        if not isinstance(tensor, torch.Tensor) or not tensor.is_cuda:
            tensor = to_numpy(tensor).ravel().view(self.dtype)
            self[tuple(slice(axis) for axis in tensor.shape)] = tensor
            return self
        else: # it is a cuda tensor
            with self.map_cuda() as ptr:
                if not tensor.is_contiguous():
                    print('Warning: Noncontiguous inputs are not handled correctly yet!')
                cuda.memcpy_dtod(ptr, tensor.data_ptr(), min(nbytes(tensor), nbytes(self)))

class CudaPixelReadBuffer(CudaBuffer):
    def __init__(self, usage=gl.GL_STREAM_READ):
        CudaBuffer.__init__(self, gl.GL_PIXEL_PACK_BUFFER, usage)

class CudaVertexBuffer(CudaBuffer, gloo.VertexBuffer): # TODO: Use vertexarray instead for OS X compatability?
    # Must inherit from VertexBuffer for isinstance(x, VertexBuffer) to be true in program.py.
    def __init__(self, usage=gl.GL_DYNAMIC_DRAW):
        CudaBuffer.__init__(self, gl.GL_ARRAY_BUFFER, usage)

class DynamicBuffer(object):
    def __init__(self, initialbuffer):
        # (Renderer.position_name, np.float32, dim)
        self._buffer = initialbuffer

    def update(self, newbuffer):
        if nbytes(newbuffer) <= nbytes(self._buffer):
            self._buffer.write(newbuffer)
        else:
            # Have to resize to fit.
            # One way would be to just ask for a new buffer with gl.glBufferData, but since we can't change the size
            # of the backing Numpy array, this will likely lead to issues if one is not careful.
            # Quite inefficient, but will not be done often, likely only once in our use case:
            self._buffer.delete()
            self._buffer = to_numpy(newbuffer).ravel().view(self._buffer.dtype).view(type(self._buffer))

        return self._buffer

if __name__ == "__main__":
    from glumpy import app
    window = app.Window(640, 480, color=(1,0,1,1), visible=True)
    #window.swap = lambda: None
    __init__()

    pixelread = np.zeros((1200, window.height, window.width, 4), np.uint8).view(CudaPixelReadBuffer)
    out = torch.zeros(pixelread.shape, dtype=torch.uint8, device=cuda_context.pytorch_device)
    c = 0
    time = 0
    @window.event
    def on_draw(dt):
        global c, time
        pixelread.activate()
        window.clear()
        gl.glReadPixels(0, 0, window.width, window.height, gl.GL_RGBA, gl.GL_UNSIGNED_BYTE, c * out.stride()[0])
        pixelread.deactivate()

        #pixelread.read(out)
        c += 1
        time += dt
        if c >= len(pixelread):
            c = 0
        #print(np.count_nonzero(texture.read()))
        if time > 3:
            #print(pixelread.read(out))
            app.quit()
    app.run()


