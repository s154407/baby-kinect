import zipfile
from functools import wraps
from time import time
from multiprocessing.pool import Pool
import torch
import numpy as np
from contextlib import contextmanager
import os

def timing(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        time1 = time()
        ret = f(*args, **kwargs)
        time2 = time()
        print('{:s} function took {:.3f} s'.format(f.__name__, time2-time1))
        return ret
    return wrap

# Make multi key map?
dtype_np_pytorch = {np.float32: torch.float32,
                        np.float64: torch.float64,
                        np.float16: torch.float16,
                        np.uint8: torch.uint8,
                        np.int8: torch.int8,
                        np.int16: torch.int16,
                        np.int32: torch.int32,
                        np.int64: torch.int64}
dtype_np_pytorch = {np.dtype(k):v for k, v in dtype_np_pytorch.items()}

@contextmanager
def get_framerate(num_frames):
    time1 = time()
    yield None
    time2 = time()
    print('rendering took {:.3f} s, FPS: {:.0f} for {:.0f} frames'.format(time2 - time1,
                                                                          num_frames/(time2-time1),
                                                                          num_frames))

def to_numpy(tensor, copy=True):
    if isinstance(tensor, torch.Tensor):
        if tensor.requires_grad:
            tensor = tensor.detach()
        if tensor.is_cuda:
            tensor = tensor.cpu()
    return np.array(tensor, copy=copy)

def determinism(seed=777, speed=False):
    print('Setting random seed:', seed, 'Deterministic: ', not speed)
    torch.manual_seed(seed)
    np.random.seed(seed)  # To be safe. We made sure to only use pytorch PRNG in our code.
    if not speed:
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False  # Significantly slower (4.8 it/s -> 3.5 it/s)
    else:
        torch.backends.cudnn.benchmark = True

def map_parallel(storage_class, datafiles, parallel=False):
    if not parallel:
        return list(map(storage_class, datafiles))

    # Using multiple CPUs since each can handle a separate file. (Task is CPU-bound on SSD)
    with Pool(initializer=try_set_low_priority) as pool:
        datafiles = pool.map(storage_class, datafiles, chunksize=64)
    return list(datafiles)  # Collect results and make into list

# We want the child process to lower their priority if possible.
# If this is not possible they should still run.
def try_set_low_priority():
    try:
        set_low_priority()
    except:
        pass

def set_low_priority():
    import psutil, sys
    # Sets this process to low priority and returns the old priority value.
    process = psutil.Process()
    old_priority = process.nice()

    # Check if Windows or Unix
    try:
        sys.getwindowsversion()
    except AttributeError:
        windows = False
    else:
        windows = True

    if windows:
        process.nice(psutil.BELOW_NORMAL_PRIORITY_CLASS)
    else:
        process.nice(1)

    return old_priority

def find_mini_rgbd(path):
    datafiles = []
    # Find all data files, given as (path, XXXXX):(str,str), where XXXXX is the number of the image, e.g. XXXXX_pose.png
    with os.scandir(path) as it:
        for entry in it:
            # Get directories /XX/
            if entry.is_dir() and entry.name.isdigit():
                for file in os.listdir(os.path.join(entry, 'smil_params')):
                    if file.endswith("_pose.txt") and file[:5].isdigit():
                        datafiles.append((entry.path, file[:5]))
    return datafiles

# We want the child process to lower their priority if possible.
# If this is not possible they should still run.
def try_set_low_priority():
    try:
        set_low_priority()
    except:
        pass

def set_low_priority():
    import psutil, sys
    # Sets this process to low priority and returns the old priority value.
    process = psutil.Process()
    old_priority = process.nice()

    # Check if Windows or Unix
    try:
        sys.getwindowsversion()
    except AttributeError:
        windows = False
    else:
        windows = True

    if windows:
        process.nice(psutil.BELOW_NORMAL_PRIORITY_CLASS)
    else:
        process.nice(1)

    return old_priority

def npz_headers(npz, filter = lambda name: True):
    """Takes a path to an .npz file, which is a Zip archive of .npy files.
    Generates a sequence of (name, shape, np.dtype).
    """
    # # https://stackoverflow.com/questions/35990775/finding-shape-of-saved-numpy-array-npy-or-npz-without-loading-into-memory
    with zipfile.ZipFile(npz) as archive:
        for name in archive.namelist():
            if not name.endswith('.npy') or not filter(name):
                continue

            npy = archive.open(name)
            version = np.lib.format.read_magic(npy)
            shape, fortran, dtype = np.lib.format._read_array_header(npy, version)
            yield name[:-4], shape, dtype