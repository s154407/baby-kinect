import torch
from torch import nn
import torch.nn.functional as F
from utils import timing, to_numpy
import matplotlib.pyplot as plt


class ConvBlock(nn.Module):
    # (Unused)
    def __init__(self, in_channels, out_channels, kernel_size=3, stride=1, padding=1):
        super().__init__()
        # inplace doesn't make a difference, as a fc connection ALWAYS copies?
        conv = nn.Conv2d(in_channels, out_channels,
                         kernel_size=kernel_size, stride=stride, padding=padding)
        chain = [conv, nn.ReLU()]
        self.chain = nn.Sequential(*chain)

    def forward(self, input):
        return self.chain(input)

class CPM(nn.Module):
    # R - (2D) Regressor
    def __init__(self, num_joints):
        # TODO also add num_channels input, and maybe a stride getter.
        super().__init__()
        self.conv1_1 = nn.Conv2d(1,64,kernel_size=3,stride=1,padding=1)
        self.conv1_2 = nn.Conv2d(64,64,kernel_size=3,stride=1,padding=1)
        self.max_1   = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv2_1 = nn.Conv2d(64,128,kernel_size=3,stride=1,padding=1)
        self.conv2_2 = nn.Conv2d(128,128,kernel_size=3,stride=1,padding=1)
        self.max_2   = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv3_1 = nn.Conv2d(128,256,kernel_size=3,stride=1,padding=1)
        self.conv3_2 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv3_3 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv3_4 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.max_3   = nn.MaxPool2d(kernel_size=2, stride=2)
        self.conv4_1 = nn.Conv2d(256,512,kernel_size=3,stride=1,padding=1)
        self.conv4_2 = nn.Conv2d(512,512,kernel_size=3,stride=1,padding=1)
        #
        self.conv4_3 = nn.Conv2d(512, 256,kernel_size=3,stride=1,padding=1)
        self.conv4_4 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_5 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_6 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_7 = nn.Conv2d(256,128,kernel_size=3,stride=1,padding=1)
        #self.fc = nn.Linear(128, num_joints + 1)
        self.fc = nn.Conv2d(128, num_joints + 1, kernel_size=1)

    def forward(self, image):
        batch_size = image.shape[0]
        x = F.relu(self.conv1_1(image))
        x = F.relu(self.conv1_2(x))
        x = self.max_1(x)
        x = F.relu(self.conv2_1(x))
        x = F.relu(self.conv2_2(x))
        x = self.max_2(x)
        x = F.relu(self.conv3_1(x))
        x = F.relu(self.conv3_2(x))
        x = F.relu(self.conv3_3(x))
        x = F.relu(self.conv3_4(x))
        x = self.max_3(x)
        x = F.relu(self.conv4_1(x))
        x = F.relu(self.conv4_2(x))
        x = F.relu(self.conv4_3(x))
        x = F.relu(self.conv4_4(x))
        x = F.relu(self.conv4_5(x))
        x = F.relu(self.conv4_6(x))
        features = self.conv4_7(x)
        heatmaps = self.fc(F.relu(features))
        #confmap = self.fc(features.permute(0, 2, 3, 1).reshape(batch_size * 46 * 46, -1))
        #confmap = confmap.reshape(batch_size, 46, 46, -1).permute(0, 3, 1, 2)
        return heatmaps, features

    @staticmethod
    def show_confmap(confmap, groundtruth):
        vmax = confmap.max()
        vmin = confmap.min()
        print(confmap, vmax, vmin)
        plt.subplot(1, 2, 1)
        plt.imshow(confmap, cmap='hot', interpolation='nearest', vmin=vmin, vmax=vmax)
        #plt.colorbar()
        plt.subplot(1, 2, 2)
        plt.imshow(groundtruth, cmap='hot', interpolation='nearest', vmin=vmin, vmax=vmax)
        #plt.subplot(1, 3, 3)
        #plt.imshow((confmap-groundtruth)**2, cmap='hot', interpolation='nearest')
        plt.show()


class Pose2Dto3D(nn.Module):
    # T - Transformer
    def __init__(self, num_joints, channels=128, linear_size=1024):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=5,stride=2,padding=1)
        self.conv2 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=5,stride=2,padding=1)
        self.maxpool = nn.MaxPool2d(kernel_size=2,stride=2)
        # Calculate 5 * 5 from some input resolution or something.
        self.fc1 = nn.Linear(in_features=channels * 5 * 5,out_features=linear_size)
        self.lstm = nn.LSTM(input_size=linear_size,hidden_size=linear_size,num_layers=2,batch_first=True)
        self.fc2 = nn.Linear(in_features=linear_size, out_features= 3 * num_joints)

    @timing
    def forward(self, joints2d_features):
        batch_size, seq_len = joints2d_features.shape[:2]
        x = joints2d_features.reshape(seq_len*batch_size, *joints2d_features.shape[2:]) # To vector
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x), inplace=True)
        x = self.maxpool(x)
        x = x.reshape(batch_size * seq_len, -1) # To vector.
        x = F.relu(self.fc1(x), inplace=True)
        x = x.reshape(batch_size, seq_len, -1)
        # Slowly iterate over each batch, clearing the hidden state between each:

        results = []
        for batch in x:
            batch = batch.unsqueeze(0)
            lstm_out, _ = self.lstm(batch)
            results.append(lstm_out)
        x = torch.cat(results)
        x = x.reshape(batch_size * seq_len, -1)
        x = F.relu(self.fc2(x), inplace=True)
        return x
 
def train_CPM(device):
    num_joints = 24
    batch_size = 16
    cpm = CPM(num_joints).to(device=device)
    from dataset import PoseGenerator
    from trainer import Trainer
    import torch.optim as optim
    pg = PoseGenerator()

    #dataset = PrePreprocess(MiniRGBDDataset, './preloaddataset.pkl', pg.generated_poses.permute(0, 2, 3, 1),
    #                        pg.joints_2d_position)

    # TODO add some kind of transform to the tensordataset
    train_set = torch.utils.data.TensorDataset(
        torch.as_tensor(pg.generated_poses, dtype=torch.float) / 255,
        torch.as_tensor(pg.heatmaps, dtype=torch.float) / 255)
    split_point = int(len(train_set) * 11 / 12)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    #loss = nn.MSELoss(reduction='none')
    trainer = Trainer(train_set, val_set,
                      loss_fn=lambda output, target: ((output[0][:, :-1] - target[:, :-1]) ** 2).mean(-3).mean([-1, -2]),
                      reduction_fn=lambda batch: batch.mean())

    #loss_fn = lambda output, target: ((output[0][:, :-1] - target[:, :-1]) ** 2).mean(-3).mean([-1, -2]),
    #crossentropy = nn.CrossEntropyLoss(reduction='none')
    #trainer = Trainer(train_set, val_set,
    #                  loss_fn=lambda output, target: crossentropy(output[0][:, :-1], torch.argmax(target[:, :-1], dim=1)).mean([-1, -2]),
    #                  reduction_fn=lambda batch: batch.mean())

    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = optim.Adam(cpm.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(cpm, batch_size, optimizer, scheduler, max_epochs=250, patience=10)
        Trainer.plot_training(*train_result, offset=5)


    # images = to_numpy(pg.generated_poses[val_set.indices])
    # images = images.transpose((0, 3, 2, 1)).repeat(3, axis=-1)  # Channels are usually last in images.
    # visualize3Dto2D(images, P, val_set)

def plot_CPM(device):
    from dataset import PoseGenerator

    num_joints = 24
    batch_size = 16
    from model import cpm
    cpm = cpm.CPM(num_joints).to(device=device)

    pg = PoseGenerator()
    limit = 10
    set = torch.utils.data.TensorDataset(
        torch.as_tensor(pg.generated_poses[:limit], dtype=torch.float) / 255,
        torch.as_tensor(pg.heatmaps[:limit], dtype=torch.float))

    savedmodel = torch.load('./trainednetworks/cpmverygood.pt', map_location=device)
    print(savedmodel.keys(), cpm.first10)
    cpm.load_state_dict(savedmodel)

    sample = 0
    channel = 0
    input, target = set[None, sample]
    output = cpm(input)
    print(output[0].shape, input.shape, target.shape)
    CPM.show_confmap(to_numpy(output[0][0, channel]), to_numpy(target[0, channel]))

if __name__ == "__main__":
    device = torch.device('cpu')  # 'cuda' or 'cpu'
    #train_CPM(device)
    plot_CPM(device)
    exit()

    # train_projector(device)
    C = PoseRegressor(num_joints)
    P = PoseProjector(num_joints)
    module1 = Pose2Dto3D(num_joints).to(device=device)
    module2 = Pose3Dto2D(C, P).to(device=device)
    #print(list(module2.parameters())
    for i in range(10):
        x = torch.zeros(5, 10, 128, 46, 46, device=device)
        x = module1(x)
        x = module2(x)
        print(i)