import torch, torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F

import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim
import os
from tqdm import tqdm
from utils import timing
import datetime


class Statistic(object):
    def __init__(self):
        self.means = []
        self.stds = []
        self.counts = []

    def add(self, loss_tensor):
        with torch.no_grad():
            mean_loss = loss_tensor.mean(0).item()
            self.means.append(mean_loss)
            self.stds.append(loss_tensor.std(0).item())
            self.counts.append(len(loss_tensor))
        return mean_loss

    def get(self, index_to):
        self.means = self.means[:index_to]
        self.stds = self.stds[:index_to]
        self.counts = self.counts[:index_to]
        return self

    def __len__(self):
        return len(self.means)



class Trainer(object):
    def to_device(self, tensor, device):
        if isinstance(tensor, torch.Tensor):
            return tensor.to(device, non_blocking=self.non_blocking)
        else:
            return [t.to(device, non_blocking=self.non_blocking) for t in tensor]

    @timing
    def train_epoch(self, model, train_loader, optimizer, device):
        model.train()
        train_losses = []
        # https://github.com/davidlmorton/learning-rate-schedules/blob/master/increasing_batch_size_without_increasing_memory.ipynb
        for batch_idx, (data, target) in enumerate(tqdm(train_loader, smoothing=0.05)):
        #for batch_idx, (data, target) in enumerate(train_loader):
            data = self.to_device(data, device)
            target = self.to_device(target, device)
            optimizer.zero_grad()
            output = model(data)
            loss = self.loss_fn(output, target)
            train_losses.append(loss.detach())
            # https://pytorch.org/docs/stable/notes/faq.html#pack-rnn-unpack-with-data-parallelism
            loss.mean(0).backward()  # get average (or other reduction) over batch.
            optimizer.step()
        return torch.cat(train_losses)

    @timing
    def test_epoch(self, model, test_loader, device):
        model.eval()
        test_losses = []
        val_loss = 0
        with torch.no_grad():
            for batch_idx, (data, target) in enumerate(test_loader):
                data = self.to_device(data, device)
                target = self.to_device(target, device)
                output = model(data)
                loss = self.loss_fn(output, target)
                if self.val_fn is not None:
                    val_loss += self.val_fn(output, target).item()
                test_losses.append(loss)
        return torch.cat(test_losses), val_loss

    @staticmethod # can be used as loss_fn together with (default) mean reduction, if accuracy is of interest.
    def class_loss_fn(output, target):
        """ NOT to be used for training - only validation! """
        # Returns the loss in addition to the error on every sample.
        pred = output.max(1, keepdim=True)[1]  # get the index of the max log-probability
        return pred.eq(target.view_as(pred))

    def __init__(self, train_set, test_set,
                 loss_fn=lambda x, y: F.cross_entropy(x,y,reduction='none'),
                 val_fn=None, non_blocking=True, pin_memory=False, seed=None):
        # try pin_memory if transfer to GPU is bottleneck
        self.loss_fn = loss_fn
        # val_fn is an additional loss function that doesn't have to be differentiable
        # if used, it should reflect the true target. It should be reduced by a sum.
        self.val_fn = val_fn
        self.non_blocking = non_blocking
        self.pin_memory = pin_memory
        self.train_set = train_set
        self.test_set = test_set
        self.seed = seed

    @staticmethod
    def find_max_batch(model, dataset, initial, loss_fn, device):
        # This function should return a batch size that guarantees that there will be no
        #  RuntimeError when training with that size. This size should be as large as possible.

        # If dataset is extremely large, this may run out of (RAM) memory:
        data_set, target_set = zip(*dataset)
        data_set, target_set = torch.stack(data_set), torch.stack(target_set)
        losses = []
        n = initial
        while n <= len(dataset):
            try:
                data = data_set[:n, ...].to(device, non_blocking=True)
                target = target_set[:n, ...].to(device, non_blocking=True)
                output = model(data)

                loss = loss_fn(output, target)
                torch.mean(loss).backward()

                losses.append(loss)
                torch.mean(torch.cat(losses)).item()
                n *= 2
            except RuntimeError:
                if n == 0:
                    # If we could not even have one sample on the GPU.
                    raise
                else:
                    break

        if 2 <= n:  # Be conservative (in case we are at the limit).
            n //= 2
            if 2 <= n: #FIXME: Delete?
                n //= 2

        return n

    def resume_training(self, model, optimizer, scheduler, load_optim_state_dict=True, filename='bestmodel.pt', callback=lambda:None, **kwargs):
        print('Resuming training')
        try:
            resume = torch.load(filename)
            model.load_state_dict(resume['model_state_dict'])
            if load_optim_state_dict:
                optimizer.load_state_dict(resume['optimizer_state_dict'])
            # TODO: Add scheduler state dict
            resume = resume['trainer_state']
            resume.update(kwargs)
            Trainer.plot_training(resume['train_losses'], resume['test_losses'], offset=1)
        except FileNotFoundError:
            print('Failed to resume training from:', filename, '(file not found)')
            resume = kwargs

        return self.train(model=model, optimizer=optimizer, scheduler=scheduler, callback=callback, filename=filename, **resume)

    def train(self, model, batch_size, optimizer, scheduler=None, max_epochs=10,
              patience=10, rel_threshold=0.001, max_batch_size=None, callback=lambda: None,
              filename='bestmodel.pt', num_bad_epochs = 0,
              epoch = 0, val_losses = [], best_val_loss = None,
              train_losses = Statistic(), test_losses = Statistic()):
        # optimizer.step is called on every mini-batch.
        # scheduler.step is called on every epoch, given val_loss and epoch

        # Later we can pass a train-supervisor as argument. This class should have a function to register
        #  each iteration of training. And it should return whether the training should break early.
        #  it should be given train_losses, val_losses, model, optimizer, batch_size so it can checkpoint all this.
        #  this mid-train checkpoint is only if model takes days to train. Otherwise it should just be saved when
        #  the training is done, and this function returns.
        #   The training supervisor could also stop training after a specific time has passed!
        device = next(model.parameters()).device  # Put tensors on same device as model.(Could also be arg)
        # Find maximal batch size.
        if max_batch_size is None:
            dataset = self.train_set if len(self.train_set) >= len(self.test_set) else self.test_set
            max_batch_size = Trainer.find_max_batch(model, dataset, batch_size, self.loss_fn, device)
            print('Found max batch size:', max_batch_size)

        # Start by finding largest possible batch size.
        # Simply save seed, instead of weights, so we can re-do the training.
        # Should be given a test and validation set (generated for K-fold cross-validation)
        #  - and the training set should then be randomized.
        if batch_size > len(self.train_set):
            print('Warning: Batch size greater than length of trainnig dataset')
        train_loader = torch.utils.data.DataLoader(self.train_set, batch_size=batch_size,
                                                   pin_memory=self.pin_memory)
        test_loader = torch.utils.data.DataLoader(self.test_set, batch_size=min(max_batch_size, len(self.test_set)),
                                                  pin_memory=self.pin_memory)
        #best_val_loss = None
        #num_bad_epochs = 0

        #train_losses, test_losses = Statistic(), Statistic()
        #val_losses = []
        for epoch in tqdm(range(epoch, max_epochs), unit='epochs'):
            train_loss = train_losses.add(self.train_epoch(model, train_loader, optimizer, device))
            test_loss, val_loss = self.test_epoch(model, test_loader, device)

            test_loss = test_losses.add(test_loss)
            if self.val_fn is None:
                val_loss = test_loss
            print('Epoch:', epoch, 'Train loss:', train_loss, 'Test loss:', val_loss)

            callback()
            if scheduler is not None:
                try:
                    should_stop = scheduler.step(val_loss)
                except TypeError:
                    should_stop = scheduler.step()
                if should_stop is not None and should_stop:
                    break
            # Move the logic below here into the scheduler.
            if best_val_loss is None or val_loss < best_val_loss * (1 - rel_threshold):
                best_val_loss = val_loss
                num_bad_epochs = 0
                print('Best loss:', best_val_loss)
                # Save checkpoint.
                torch.save({
                    'trainer_state': {
                        'epoch': epoch,
                        'best_val_loss': best_val_loss,
                        'val_losses': val_losses,
                        'train_losses': train_losses,
                        'test_losses': test_losses,
                        'batch_size': batch_size,
                        'max_batch_size': max_batch_size,
                        'patience': patience,
                        'rel_threshold': rel_threshold,
                        'num_bad_epochs': num_bad_epochs,
                },
                    'model_state_dict': model.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                }, filename)
            else:
                num_bad_epochs += 1
                if num_bad_epochs >= patience:
                    print("Early stopping in epoch", epoch)
                    break
        else:
            num_bad_epochs = 0  # late stopping, haven't hit plateau.
            print("Late stopping.")
        epochs = epoch - num_bad_epochs  # We do not care for after the plateau.
        print("Training done. Best train loss, test loss and val loss:",
              min(train_losses.means), min(test_losses.means), best_val_loss)
        return train_losses.get(epochs), test_losses.get(epochs), val_losses

    def save_network(self, model, optimizer, scheduler=None, train_losses=None, val_losses=None, train_set=None,  val_set=None):
        train_indices = None
        val_indices = None
        try:
            train_indices = train_set.indices
            val_indices = val_set.indices
        except AttributeError:
            pass
        torch.save({
            'epoch': len(train_losses),
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'scheduler_state_dict': scheduler.state_dict(),
            'train_losses': np.stack(train_losses),
            'val_losses': np.stack(val_losses),
            'train_indices': train_indices,
            'val_indices': val_indices
        },f'{model.__class__.__name__}–{datetime.datetime.now()}.tar')
        # gem epoch og alt state i train()-funktion. Tager imod nn-Module eller en liste af modules. (netværk / models, optimizer) og gemmer både model og optimizer statedicts og hvis muligt gemme reduction og loss også – navngiv efter dato og module navn.
        # losses er tensors, men stack dem med 0 


    def load_network(self):
        pass

    @staticmethod
    def plot_training(train_losses : Statistic, test_losses : Statistic, offset=3, save=True):
        if len(train_losses) <= offset:
            offset = max(len(train_losses), 2)

        epochs = len(train_losses)
        #fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1)
        ax1 = plt.subplot()
        # ax1.set_xlabel('epoch')
        ax1.set_ylabel('loss')
        color = 'tab:red'
        ax1.plot(range(offset, len(train_losses)), train_losses.means[offset:], '-',
                 range(offset, len(test_losses)), test_losses.means[offset:], '--', color=color)
        ax1.legend(['training', 'validation'])
        ax1.tick_params(axis='y', labelcolor=color)
        # Training seems to follow a power law curve: https://en.wikipedia.org/wiki/Learning_curve

        # Additionally we could plot more statistics: like how concave the function is, moving averages, etc.
        # ax2 = ax1.twinx()
        # color = 'tab:blue'
        # ax2.set_ylabel('acc')  # we already handled the x-label with ax1
        #  ax2.plot(epoch_range, [-math.log(1/x-1) for x in test_accs[offset:epochs]], '--', color=color)
        # ax2.plot(range(epochs), test_accs[:epochs], '--', color=color)
        # ax2.plot([offset + math.log(x/offset)/math.log(epochs/offset) * (epochs-offset) for x in range(offset, epochs)],
        #         [math.log(x) for x in val_losses[offset:]], '--', color=color)
        # ax2.tick_params(axis='y', labelcolor=color)
        # ax2.invert_yaxis()
        # We want statistics for the lowest/highest weight, also lowest absolute.

        #ax2.set_xlabel('epoch')
        #ax2.set_ylabel('loss')
        #ax2.loglog(range(offset, epochs), train_losses.means[offset:], '-',
        #           range(offset, epochs), test_losses.means[offset:], '--', color=color)
        #ax2.legend(['training', 'validation'])
        ax1.tick_params(axis='y', labelcolor=color)
        plt.grid(True)

        #fig.tight_layout()
        if save:
            plt.savefig(os.path.join('.', 'figures', 'learning_curve.svg'), bbox_inches='tight')
        plt.show()


if __name__ == '__main__':
    torch.manual_seed(4)  # reproducible, since we shuffle in DataLoader.

    # Load and and preprocess dataset (or load pre-preprocessed)
    data_root = 'mnist'
    train_set = PrePreprocess(torchvision.datasets.MNIST,
                              os.path.join(data_root, 'preprocessed', 'training.pt'),
                              data_root, train=True, download=True, transform=transforms.ToTensor())
    test_set = PrePreprocess(torchvision.datasets.MNIST,
                             os.path.join(data_root, 'preprocessed', 'test.pt'),
                             data_root, train=False, download=True, transform=transforms.ToTensor())

    # For test loader, the batch_size should just be as large as can fit in memory.
    # train_batch_size = 256 * 3
    # test_batch_size = 2 << 9

    # Try getting gradient over training set, and then scale by size of gradient over test set. / should not work

    # Batch size should be as large a possible (but limited by memory) for speed
    #  Then to (hyper) optimize it should be halved until that does not improve scores.
    # train_loader = torch.utils.data.DataLoader(train_set, batch_size=train_batch_size, shuffle=True, pin_memory=False)
    # test_loader = torch.utils.data.DataLoader(test_set, batch_size=test_batch_size, shuffle=False, pin_memory=False)

    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    #model = MNISTnet().to(device, non_blocking=True)
    #optimizer = optim.Adagrad(model.parameters(), lr=0.1)
    #Trainer(train_set, test_set).train(model, optimizer, batch_size=128*2, max_epochs=200)
    split_point = int(len(train_set) * 5/6)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    trainer = Trainer(train_set, val_set)
    for lr_i in [1, 0.1]:
        # If model is extremely large, this may run out of memory.
        model1 = MNISTnet2().to(device, non_blocking=True)
        optimizer = optim.Adagrad(model1.parameters(), lr=lr_i)
        #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, 200)
        train_result = trainer.train(model1, 128*2*2, optimizer, scheduler, max_epochs=200, patience=10)
        Trainer.plot_training(*train_result)
    #print("Number of parameters:", list(map(len, list(model.parameters()))))
    # train(model, train_loader, optimizer, device=device)
    # test_loss, test_acc = test(model, train_loader, device=device)
    # print(test_acc)
    # We need to optimize hyper parameters. Also a way of stopping early.
