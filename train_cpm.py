import os

from model.cpm import train_synthetic_cpm, train_mikkel_cpm
from render.cameraparameters import CameraParameters
from utils import determinism

def train_cpm_on_minirgbd_1():
    print('Starting training cpm on the minirgbd dataset (1)')
    # This is to test training with no flip augmentation.
    # This should be the closest to the real pose data distribution.
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-1.pt'),
                        max_epochs=50)

def train_cpm_on_minirgbd_2():
    print('Starting training cpm on the minirgbd dataset (2)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=5, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM5-SYNTHETIC(NOSE)-MIKKEL-SSL-2.pt'),
                        max_epochs=50)

def train_cpm_on_minirgbd_3():
    print('Starting training cpm on the minirgbd dataset (3)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=9, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM9-SYNTHETIC(NOSE)-MIKKEL-SSL-3.pt'),
                        max_epochs=50)

def train_cpm_on_minirgbd_4():
    print('Starting training cpm on the minirgbd dataset (4)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005,
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-4.pt'),
                        max_epochs=50)


def train_cpm_on_minirgbd_5():
    print('Starting training cpm on the minirgbd dataset (5)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=4, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-BG-5.pt'),
                        max_epochs=50, background_heatmap=True)

def train_cpm_on_minirgbd_6():
    print('Starting training cpm on the minirgbd dataset (6)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005, pretrained='VGG19',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-VGG19-6.pt'),
                        max_epochs=50)

def train_cpm_on_minirgbd_7():
    # This is to test training with flip augmentation.
    # This doubles the training data, but the pose distribution may not completely match the
    #  true poses of infants, since there may be natural assymmetry, e.g. left/right-handedness.
    print('Starting training cpm on the minirgbd dataset (7)')
    determinism()
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7.pt'),
                        max_epochs=50, flip=True)

def train_cpm_on_minirgbd_8():
    # To test the training with no noise.
    print('Starting training cpm on the minirgbd dataset (8)')
    determinism() # Try different seed
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-NONOISE-MIKKEL-SSL-8.pt'),
                        max_epochs=50, noise=False)

def train_cpm_on_minirgbd_9():
    print('Starting training cpm on the minirgbd dataset (9)')
    determinism(123) # Try different seed
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    train_synthetic_cpm(camera_params, visible=False, sigma=7, lr=0.00005, pretrained='SSL',
                        filename=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-9.pt'),
                        max_epochs=50, noise=True, flip=True)

def train_cpm_on_mikkel_1():
    determinism()
    train_mikkel_cpm(sigma=7, batch_size=16, lr=0.00005 * 1/10, pretrained='SSL',
                     filename=os.path.join('trainednetworks', 'CPM7-REAL-MIKKEL-SSL-1.pt'),
                     max_epochs=1)


if __name__ == "__main__":
    #TODO: Plot confidence vs error.
    train_cpm_on_minirgbd_4()
    #train_cpm_on_mikkel_1()
    # python -c "from train_cpm import *; train_cpm_on_minirgbd()_1" --framerate 0
    # python -c "from train_cpm import *; train_cpm_on_mikkel()"