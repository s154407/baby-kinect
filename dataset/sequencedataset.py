import bisect

import torch
import torch.utils.data
# A better design than using inheritence would be
#  to make them composable in some way. Like TensorDataset, SubsetDataset
#  etc. from the Pytorch api. That way would be more pythonic.

class SequenceDataset(torch.utils.data.Dataset):
    def __init__(self, dataset, seq_len=5):
        self._dataset = dataset
        self._seq_len = seq_len

    def __len__(self):
        # The number of sequences.
        # E.g. if we have a seq_len of 5, and 5 frames, then we have 1 sequence.
        return len(self._dataset) - self._seq_len + 1

    def __getitem__(self, item):
        return self._dataset[item:item+self._seq_len]

    def get_sample_id(self, idx):
        return idx

class DownsampledSequenceDataset(SequenceDataset):
    def __init__(self, dataset, seq_len=5, downsample_factor=1):
        super().__init__(dataset, seq_len)
        self._downsample_factor = downsample_factor
        self._seq_len = downsample_factor * (self._seq_len - 1) + 1 # = seq_len if skip=0

    def __getitem__(self, item):
        return self._dataset[item:item+self._seq_len:self._downsample_factor]



class SkippedDownsampledSequenceDataset(DownsampledSequenceDataset):
    #    ^ *Laughs in Java

    def __init__(self, dataset, seq_len=5, downsample_factor=1, skip_factor=1):
        super().__init__(dataset, seq_len, downsample_factor)
        # Since making seq_len and downsample_factor increases the apparent size of
        #  the dataset tremendously, adding a skip factor, so we skip over some frames
        #  can reduce it to a manageable size.
        self._skip_factor = skip_factor

    def __len__(self):
        return super().__len__()//self._skip_factor

    def __getitem__(self, item):
        return super().__getitem__(item*self._skip_factor)

    def get_sample_id(self, idx):
        return super().get_sample_id(idx * self._skip_factor)