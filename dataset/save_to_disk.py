import h5py
from pathlib import Path

if __name__ == "__main__":
    # Get list of all mat files
    # For each mat file 
    # Open
    # Load downsampled images
    # save to new file on fisk.

    # load_path='/Volumes/exFAT'
    load_path='H:'
    load_path = Path(load_path)

    files = sorted(load_path.glob('*.mat'))

    sequence_names = [file.name for file in files]

    i = 0
    for sequence_path in files:
        mat_file = h5py.File(sequence_path.resolve(), 'r')    
        depth = mat_file['downsampled_depth']
        if 'rotated_pos' in mat_file.keys():
            joints3d = mat_file['rotated_pos']
        else:
            joints3d = mat_file['filtered_pos']
        
        with h5py.File("mikkeldata/"+sequence_names[i], "w") as f:
            depth_dset = f.create_dataset("downsampled_depth", data=depth)
            joints3d_dset = f.create_dataset("filtered_pos", data=joints3d)

        i = i + 1



            