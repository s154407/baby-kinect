import bisect
import os

import numpy as np
import torch

from dataset import augmentation
from dataset.sequencedataset import SequenceDataset, DownsampledSequenceDataset, SkippedDownsampledSequenceDataset
from model.SML import SML
from utils import timing

class MINIRGBDSequence(torch.utils.data.Dataset):
    def __init__(self, parampath, beta, plane):
        self._parampath = parampath
        self.beta = beta
        self.plane = plane
        N = len(os.listdir(parampath)) // 2 # Number of frames.
        self._need_read = np.ones(N, np.bool_)
        # We read one frame to get the parameter shapes.
        self._pose, self._trans = map(MINIRGBDDataset.readfile,
                                      (os.path.join(parampath, '00000_pose.txt'),
                                       os.path.join(parampath, '00000_trans.txt')))
        self._pose = np.zeros((N,) + self._pose.shape, self._pose.dtype)
        self._trans = np.zeros((N,) + self._trans.shape, self._trans.dtype)

    def __len__(self):
        return len(self._trans)

    def __getitem__(self, item):
        # As a normal torch Dataset, one can use getitem to get a sample.
        # Additionally, this supports slicing in the first dimension for the
        #  pose parameters, as they very over time.
        # The files are loaded once in a lazy fashion.
        need_read = self._need_read[item]
        if not any(need_read):
            payload = self._pose[item], self._trans[item], self.beta, self.plane
            return tuple(map(np.copy, payload))
        else:
            for i, j in enumerate(range(len(self))[item]):
                if need_read[i]:
                    num = '%05d' % j
                    self._pose[j] = MINIRGBDDataset.readfile(os.path.join(self._parampath, num + '_pose.txt'))
                    self._trans[j] =  MINIRGBDDataset.readfile(os.path.join(self._parampath, num + '_trans.txt'))
                    need_read[i] = False
            return self.__getitem__(item) # Retry


class MINIRGBDDataset(torch.utils.data.ConcatDataset):
    @staticmethod
    def readfile(file):
        #return np.loadtxt(file) # 7 seconds
        #return pd.read_csv(file, header=None).values # 38 seconds
        return np.fromfile(file, dtype=np.float64, sep=" ") # 3.5 seconds

    @staticmethod
    def get_sequences(path):
        sequences = dict()
        # Find all data files, given as (path, XXXXX):(str,str), where XXXXX is the number of the image, e.g. XXXXX_pose.png
        with os.scandir(path) as it:
            for entry in it:
                # Get directories /XX/
                if entry.is_dir() and entry.name.isdigit():
                    beta = MINIRGBDDataset.readfile(os.path.join(entry.path, 'smil_shape_betas.txt'))
                    plane = np.loadtxt(os.path.join(entry.path, 'bg_plane.txt'), usecols=range(1, 5))
                    sequences[entry.name] = MINIRGBDSequence(os.path.join(entry, 'smil_params'), beta, plane)
        return sequences

    @timing
    def __init__(self, seq_len=1, downsample_factor=1, skip_factor=1, path='MINI-RGBD_web',
                 startseq=0, endseq=-1, random_betas=False, random_rotation=False):
        # given startseq and endseq one can subset the minirgbd to use e.g. in train/test split.
        self._seq_len = seq_len
        self._betas = dict()
        self._lazy = dict()
        self._random_betas = random_betas
        self._random_rotation = random_rotation

        self._sequences = MINIRGBDDataset.get_sequences(path)

        self._sequence_names = sorted(self._sequences.keys(),
                                      key=lambda key_value: int(key_value[0]))  # Sort by folder-name as int.
        self._sequence_names = self._sequence_names[startseq:endseq] # Subset of sequences

        # Let ConcatDataset handle the details :)
        super().__init__([SkippedDownsampledSequenceDataset(self._sequences[name], seq_len, downsample_factor, skip_factor) for name in self._sequence_names])

    @property
    def betas(self):
        return [sequence.beta for sequence in self._sequences.values()]

    @property
    def seq_len(self):
        return self._seq_len

    def get_sample_id(self, idx):
        # See __getitem__ of ConcatDataset
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]

        name = self._sequence_names[dataset_idx]
        id = self.datasets[dataset_idx].get_sample_id(sample_idx)
        return name, id

    def __getitem__(self, item):
        pose, trans, beta, plane = super().__getitem__(item)

        if self._random_rotation:
            # The first three elements of pose is global rotation.
            pose[:, :3], trans = augmentation.augmented_rotation(pose, trans, plane)

        return pose, trans, beta if not self._random_betas else None, plane