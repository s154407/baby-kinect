import bisect

import torch
import numpy as np
from argparse import Namespace
from torch.utils.data import ConcatDataset

from dataset.cachetransformer.cache import Cache
from dataset.cachetransformer.cpmcache import CPMCache
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from dataset.cachetransformer.joints2dcache import Joints2DCache
from dataset.mikkel import mikkel_dataset
from model.cpm import CPM
from render.cameraparameters import CameraParameters
from utils import timing


def identity(cache):
    return cache

class BasicCachedDataset(ConcatDataset):
    # This is a more simple implementation of the cacheSMLdataset.
    #  made using the experience we got from our first implementation attempt.

    def __init__(self, datasets, cache, cache_size, transforms, post_transform=identity):
        super().__init__(datasets)

        self._cache_ptr = None # None means cache is not ready.
        self._cache_size = cache_size
        self._cache = cache
        self._indices = list(range(len(self)))  # Data indices. For shuffling.
        self._transforms = transforms

        for t in self._transforms:
            t.init_cache(self._cache)
        self._post_transform = post_transform

    def shuffle(self):
        self._cache_ptr = None # Signal cache needs to be refreshed.
        self._indices = torch.randperm(len(self)).tolist()

    @timing
    def _refresh_cache(self):
        samples = []
        for idx in range(self._cache_ptr, min(self._cache_ptr + self._cache_size, len(self))):
            samples.append(super().__getitem__(self._indices[idx]))
        nsamples = len(samples)

        cache = self._cache[:nsamples]
        self.put_samples_in_cache(cache, samples)

        # Now samples are the batched sets of samplings from the dataset, e.g. sequences.
        for t in self._transforms:
            t.transform_cache(cache)

    def put_samples_in_cache(self, samples):
        raise NotImplementedError()

    def get_sample_id(self, idx):
        idx = self._indices[idx]
        # See __getitem__ of ConcatDataset.
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]
        return self.datasets[dataset_idx].get_sample_id(sample_idx)

    def __getitem__(self, idx):
        if not -len(self) <= idx < len(self):
            raise IndexError('dataset index out of range')
        if idx < 0:
            idx += len(self)

        if self._cache_ptr is None or not 0 <= idx - self._cache_ptr < self._cache_size:
            # CACHE MISS
            # If cache is not set up, or requested sample is not in cache.
            self._cache_ptr = idx - idx % self._cache_size
            self._refresh_cache()
            return self.__getitem__(idx) # Try again.
        else:
            # CACHE HIT
            cache_idx = idx % self._cache_size
            result = self._cache[cache_idx]
            # Apply post transform. It can e.g. grab input and target and return them as tuple.
            return self._post_transform(result)

class CachedMikkelDataset(BasicCachedDataset):
    def __init__(self, datasets, sigma, background_heatmap, cpm, post_transform, cache_size=1000):
        self._seq_len = datasets[0].seq_len
        for dataset in datasets:
            if dataset.seq_len != self._seq_len:
                raise ValueError("Dataset sequence lengths do not match.")

        cache_size = min(cache_size, sum(map(len, datasets))) # No reason for cache to be larger than dataset.
        cache_size =  max(cache_size // self._seq_len, 1) # Cache contains whole sequences (at minimum 1)

        example_depth, example_joints3d = datasets[0][0]
        seq_len, width, height = example_depth.shape
        _, num_joints, _ = example_joints3d.shape
        cache = Cache()
        cache['depth'] = np.zeros((cache_size, seq_len, 1, width, height), dtype=example_depth.dtype)
        cache['joints3d'] = np.zeros((cache_size, seq_len, num_joints, 4), dtype=example_joints3d.dtype)
        cache['joints3d'][..., 3] = 1 # Homogeneous coordinates

        camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')

        # Transforms should be given as a topological ordering of the graph induced by the
        #  dependencies of the transformers.
        far = np.iinfo(example_depth.dtype).max/1000 # Because the depth is given in mm. (To undo the normalization and turn to m.
        self._renderer = Namespace(far=far) # Bad code to retrofit this with how we do it in the cpm callback.

        # FIXME: Far may be too large for project/unproject?
        # NOTE: HeatMapperCache should be second element in this list, because we use bad code in cpm.py that refers it.
        transforms = [Joints2DCache(camera_params, far=far),
                      HeatMapperCache(width, height, stride=CPM.stride,
                                      sigma=sigma, background_heatmap=background_heatmap)]
        if cpm is not None:
            transforms.append(CPMCache(cpm, far=far, batch_size=40//seq_len))
        super().__init__(datasets, cache, cache_size, transforms=transforms, post_transform=post_transform)

    def put_samples_in_cache(self, cache, samples):
        # Stack the sequences, so they can be processed in a batched manner.
        depth_samples, joints3d_samples = tuple(map(np.stack, zip(*samples)))
        depth_samples = depth_samples[:, :, np.newaxis]#.transpose(0, 1, 2, 3, 4)

        cache['depth'][...] = depth_samples
        cache['joints3d'][..., :3] = joints3d_samples


def cached_mikkel_dataset(post_transform, file_path = 'H:', cache_size=1000, cpm=None,
                          sigma=7, background_heatmap=False,
                          seq_len=1, downsample_factor=1, skip_factor=1):

    mikkel_train, mikkel_test, mikkel_valid = mikkel_dataset(file_path, downsample_factor, seq_len, skip_factor)

    def create_mikkel_cacheddataset(dataset):
        return CachedMikkelDataset([dataset], sigma=sigma, cpm=cpm, cache_size=cache_size, post_transform=post_transform,
                                   background_heatmap=background_heatmap)

    mikkel_train, mikkel_test, mikkel_valid = map(create_mikkel_cacheddataset, (mikkel_train, mikkel_test, mikkel_valid))
    return mikkel_train, mikkel_test, mikkel_valid

if __name__ == "__main__":
    mikkel_train, mikkel_test, _ = cached_mikkel_dataset()

    #mikkel_train.shuffle()
    mikkel_train.get_sample_id(0)

    pass