import bisect

import numpy as np
import torch

from dataset import augmentation
from dataset.cachetransformer.cache import Cache
from model.SML import SML
from render.renderer import Renderer
from utils import to_numpy, timing
import torch.utils.data
import torch.nn.functional as F


class CachedSMLDataset(torch.utils.data.ConcatDataset):
    def __init__(self, renderer: Renderer, model : SML, datasets, cache_size = 900,
                 render_batch_size = 1024, cube_start=0.7, cube_end=1.5,
                 augment_position=False, noise=True, augment_flip=False,
                 transforms=[], post_transform=lambda cache: cache):
        if not renderer.near <= cube_start < cube_end <= renderer.far:
            # ^ Python has chained comparisons!
            raise ValueError('The cube z bounds must be between the renderer near and far planes!')
        self.cube_start, self.cube_end = cube_start, cube_end

        self._augment_position = augment_position
        self._augment_flip = augment_flip
        self._noise = noise # Simulate noise
        self._model = model
        self._post_transform = post_transform
        # Make sure dataset is given as a list and their seq_len is the same.
        self._seq_len = datasets[0].seq_len
        self._betas = []
        for dataset in datasets:
            self._betas += dataset.betas
            if dataset.seq_len != self._seq_len:
                raise ValueError("Dataset sequence lengths do not match.")
        self._betas = np.stack(self._betas)

        cache_size = min(cache_size, sum(map(len, datasets))) # No reason for cache to be larger than datasets.
        #print(sum(map(len, datasets)), cache_size, cache_size//self._seq_len, self._seq_len)
        self._cache_size = max(cache_size // self._seq_len, 1) # Cache contains whole sequences (at minimum 1)
        self._render_batch_size = min(cache_size, render_batch_size)

        # Given dataset of MiniRGBD and/or SURREAL.
        # The dataset should return vertices.
        super().__init__(datasets)

        self._renderer = renderer

        self._indices = list(range(len(self))) # Data indices. For shuffling.

        self._cache_ptr = None
        self._cache = Cache()
        self._cache['depth'] = np.zeros((self._cache_size, self._seq_len, renderer.channels, renderer.width, renderer.height), renderer.dtype)
        # The width and height should be swapped to use the standard format ^
        self._cache['joints3d'] = np.zeros((self._cache_size, self._seq_len, model.num_joints, 4), np.float32)
        self._cache['joints2d'] = np.zeros((self._cache_size, self._seq_len, model.num_joints, 4), np.float32)
        self._transforms = transforms
        for t in self._transforms:
            t.init_cache(self._cache)
        # transform_cache = ... to allow for e.g. CPM outputs to be cached.
        # We first want to get the screen-space z of our desired cube z bounds.
        screen_z = renderer.project_points(np.array(
            [[0, 0, cube_start, 1],
             [0, 0, cube_end,   1]]))[:, 2]

        # We sample some points on sides of the frustum.
        screen_cube = np.array(
            [[0, 0, screen_z[0], 1],  # cube_start corner 1
             [1, 1, screen_z[0], 1],  # cube_start corner 4
             [1, 1, screen_z[1], 1]], np.float64) # cube_end corner 4
        screen_cube[:, :2] *= self._renderer.width, self._renderer.height
        # Get their position in 3d-space.
        far_edges =  np.array(
            [[0, 0, 1, 1],  # far corner 1
             [0, 1, 1, 1],  # far corner 2
             [1, 0, 1, 1],  # far corner 3
             [1, 1, 1, 1]], np.float64) # cube_end corner 4
        far_edges[:, :2] *= self._renderer.width, self._renderer.height
        self._far_edges = renderer.unproject_points(far_edges) # The far edges of the frustum. (Used to render the planes)

        #self._shuffle() # FIXME  remove this and next lines.
        #for i in range(self._render_batch_size):
        #    self._indices[i] = 32923 + i * self._seq_len

    def shuffle(self):
        self._cache_ptr = None # Signal cache needs to be refreshed.
        self._indices = torch.randperm(len(self)).tolist()

    def get_sample_id(self, idx):
        idx = self._indices[idx]
        # See __getitem__ of ConcatDataset
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]
        return self.datasets[dataset_idx].get_sample_id(sample_idx)
    
    def _refresh_cache(self):
        poses, translations, betas, planes = [], [], [], []
        for idx in range(self._cache_ptr, min(self._cache_ptr + self._cache_size, len(self))):
            pose, trans, beta, plane = super().__getitem__(self._indices[idx])
            poses.append(pose)
            translations.append(trans)
            betas.append(beta)
            planes.append(plane)
        nsequences = len(poses)
        nframes = nsequences * self._seq_len
        # If the beta is not supplied by the dataset select it at random.
        # We have to use sum(b is None for b in betas) instead of betas.count(None)
        # This is because .count checks for value and not identity.
        num_missing_betas = sum(b is None for b in betas)
        if num_missing_betas > 0:
            randbetas = self._betas[torch.randint(len(self._betas), (num_missing_betas,))]
            randbetas = randbetas.reshape(num_missing_betas, -1) # Fix error when num_missing_betas == 1.
            randbetas = iter(randbetas)
            betas = [beta if beta is not None else next(randbetas) for beta in betas]

        ex = next(self._model.buffers()) # TODO get device and dtype elsewhere.
        transfer = lambda t: torch.as_tensor(t, dtype=ex.dtype, device=ex.device)
        # Need function composition or piping in python!
        poses, translations, betas, planes = map(transfer, map(np.stack, (poses, translations, betas, planes)))

        # Only one beta is given for a whole sequence. We expand this to the whole sequence.
        betas = betas.unsqueeze(1).expand(-1, self._seq_len, -1)

        seqshape = lambda x: x.reshape(-1, self._seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])
        # Augment translation and/or rotation around here.
        #[render(v[i * n:(i + 1) * n])
        # for i in range((len(v) + n - 1) // n)]
        betas, poses, translations = map(flatshape, (betas, poses, translations))
        n = 1000 # To not run out of memory, since there is a large matrix multiplication bottleneck in the algorithm.
        vJtr = [self._model(betas[i * n:(i + 1) * n],
                            poses[i * n:(i + 1) * n],
                            translations[i * n:(i + 1) * n])
                for i in range((nframes + n - 1) // n)]
        vJtr = zip(*vJtr) # List of tuples to tuple of lists
        v, Jtr = map(seqshape, map(lambda t: torch.cat(t, 0), vJtr))

        if self._augment_position:
            if self._augment_flip:
                augmentation.augment_flip(v, Jtr, planes)
            # We ended up just passing all vertices. Since joints are inside the mesh, this is enough
            # to ensure that all relevant features stay completely contained in augmentation area.
            offsets = augmentation.augmented_positions(v, self._renderer, self.cube_start, self.cube_end)
            # Apply offsets
            v[..., :3] += offsets
            Jtr[..., :3] += offsets
            # Since we offset the vertices, we must also move the planes accordingly.
            axies = planes[..., :3]
            axies /= torch.norm(axies, dim=-1, keepdim=True)  # Normalize
            d = offsets.reshape(-1, 1, 3) @ axies.reshape(-1, 3, 1)  # Dot of plane normal vectors and the offsets.
            # d[i]-torch.dot(axies[i].reshape(3), offsets[i].reshape(3)) = 0
            # The dot product gives the projection onto the axis.
            planes[:, 3] -= d.squeeze(2).squeeze(1)  # Minus because -d i stored.

        # Now calculate the vertices that will make out the planes.
        far_edges = transfer(self._far_edges)
        # Their x and y positions should match the far edges of the frustum.

        # We solve for their z-positions: a*x+b*y+z*c=d <-> z=(d-a*x+b*y)/c
        z = torch.tensordot(planes[:, :2], far_edges[:, :2], ([1], [1])) # a*x+b*y
        z = (-planes[:, 3].unsqueeze(-1) - z)/planes[:, 2].unsqueeze(-1) # planes[:, -1] = -d

        if not torch.all(z <= self._renderer.far):
            raise AssertionError('A plane clips with the far plane.')
        if not torch.all(self._renderer.near <= z):
            raise AssertionError('A plane clips with the near plane.')

        far_edges = far_edges.unsqueeze(0).expand(len(z), -1, -1).clone() # This doesn't work without clone. (Worst bug)
        # Now z.shape = [N, 4] and far_edges.shape = [N, 4, 4].
        far_edges[:, :, 2] = z
        # Now that we have the far edges of the planes for each sequence we want to copy.
        # them to each frame in the sequences, concatenating them to the vertex set.
        far_edges = far_edges.unsqueeze(1).expand(*v.shape[:2], -1, -1) # [N, self._seq_len, 4, 4]
        v = torch.cat([v, far_edges], 2) # [N, self._seq_len, 6890+4, 4]

        render = lambda verts: np.copy( self._renderer.render(verts) )
        v = flatshape(v)
        # Now render the vertices in batches
        n = self._render_batch_size
        renditions = [render(v[i * n:(i + 1) * n])
                      for i in range((nframes + n - 1) // n )]
        renditions = np.concatenate(renditions)
        renditions = seqshape(renditions)
        #for i in range(1):
        #    plt.imshow(renditions[i].astype(np.float) / ((1 << 16) - 1))
        #    plt.show()
        #im = Image.fromarray(renditions[0,...,0])
        #im.save('test.tif')

        Jtr2d = self._renderer.project_points(Jtr) # Already numpy object.
        #Jtr, Jtr2d = to_numpy(Jtr[..., :3]), to_numpy(Jtr2d[..., :3])
        # Grab the arrays from the cache and modify them in-place.
        cache = self._cache[:nsequences] # Get a window into the cache.
        cache['depth'][...] = renditions.transpose(0, 1, 4, 3, 2)
        if self._noise:
            # In the MINIRGBD paper they use noise of +- 0.5 cm.
            # Since we downscale the images, this reduces the noise, making it about
            # half (and also not uniformly distributed, but we ignore this).
            #noiselevel = 0.5 # 1 in Minirgbd paper.
            # Other noise models: https://www.academia.edu/30053789/Characterizations_of_Noise_in_Kinect_Depth_Images_A_Review
            # Add simulated sensor noise of +- 0.5 cm (Standard deviation) to depth image.
            depth = torch.as_tensor(cache['depth']) # CPU tensors share memory with numpy arrays.
            #noise = torch.randn_like(depth) * 0.5 / 100
            noise = (torch.rand_like(depth) - 0.5)/100 # +- 0.5 cm noise
            depth += noise / self._renderer.far # Divide by far because depth is divided by far in shader.
            # depth.clamp_(0, far) # Clamp for completeness sake (A sensor wouldn't give numbers outside its range)
            F.relu(depth, inplace=True)  # Never negative depths!
        cache['joints3d'][...] = to_numpy(Jtr)
        cache['joints2d'][...] = to_numpy(Jtr2d)
        for t in self._transforms:
            t.transform_cache(cache)

        #torch.cuda.empty_cache()

    def get_fields(self):
        return list(self._cache.keys())

    def __getitem__(self, idx):
        if not -len(self) <= idx < len(self):
            raise IndexError('dataset index out of range')
        if idx < 0:
            idx += len(self)

        if self._cache_ptr is None or not 0 <= idx - self._cache_ptr < self._cache_size:
            # CACHE MISS
            # If cache is not set up, or requested sample is not in cache.
            self._cache_ptr = idx - idx % self._cache_size
            self._refresh_cache()
            return self.__getitem__(idx) # Try again.
        else:
            # CACHE HIT
            cache_idx = idx % self._cache_size
            result = self._cache[cache_idx]
            # We can post-transform here. This will be done by the dataloader multithreaded on cpu.
            # E.g. create heatmaps, calculate 2d joints
            #result = tuple(result[key] for key in self._return_fields)

            #image = image[..., :1].float() / 255  # Grab only first channel.
            # TODO simply return cached objects. Don't transform them?
            #  Or instead we should have dtype argument, so we can transform correctly!
            #heatmap = self._heatmap_cache[cache_idx]
            # result_heatmap = hm.generate_heatmaps(self._joints_cache, background_confmap=True)
            return self._post_transform(result)