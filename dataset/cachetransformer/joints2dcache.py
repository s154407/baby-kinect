from dataset.cachetransformer.cache import CacheTransformer
import numpy as np

from render.cameraparameters import CameraParameters


class Joints2DCache(CacheTransformer):
    # Relating to CameraParameters, used in cachedMIKKELdataset
    name = 'joints2d'
    requires = 'joints3d'

    def __init__(self, camera_params : CameraParameters, near = 0.5, far=1):
        # For the Mikkel Dataset the values of near and far does not matter, as we only
        # need the screen-space X and Y coordinates, not Z.
        self._projection = camera_params.perspective(near, far)
        self._viewport = camera_params.viewport()

    def init_cache(self, cache):
        prototype = cache[self.requires]
        cache_size, seq_len, num_joints = prototype.shape[:3]
        cache[self.name] = np.zeros((cache_size, seq_len, num_joints, 4), prototype.dtype)

    def transform_cache(self, cache):
        projected = CameraParameters.project_points(projection=self._projection,
                                                    viewport=self._viewport,
                                                    points=cache[self.requires])
        cache[self.name][...] = projected