import numpy as np

from model.heatmapper import HeatMapper
from utils import to_numpy
from dataset.cachetransformer.cache import CacheTransformer

class HeatMapperCache(HeatMapper, CacheTransformer):
    # Relating to Heatmapper.
    # I am not sure inheritence makes sense here. The heatmapper should probably be a field.
    name = 'heatmaps'

    def init_cache(self, cache):
        batch_size, seq_len, num_joints = cache['joints3d'].shape[:3]
        dtype = to_numpy(self.evalpoints.new_zeros(0)).dtype
        cache[self.name] = np.zeros((batch_size, seq_len,
                         num_joints + self._background_heatmap,
                         self._width, self._height), dtype=dtype)

    def transform_cache(self, cache):
        cache[self.name][...] = to_numpy(self.forward(self.evalpoints.new_tensor(cache['joints2d'][..., :2])))