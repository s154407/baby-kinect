class Cache(dict):
    def __getitem__(self, item):
        if isinstance(item, str):
            return super().__getitem__(item)
        # Refrain from in-place operations on the returned results since they still point to the cache.
        return Cache({key: value[item] for key, value in self.items()})

class CacheTransformer(object):
    # Cachetransformers
    # They are given as a list to the PoseDataset
    # This list should contain a topological ordering of the DAG
    #  that they induce by which dependencies they have in the cache.
    # A more complete implementation would allow for intermediate cache elements
    #  that would not be stored in the cache, but could be accessed by later transformers.
    # E.g. the SMIL model creates vertices, they are needed by the renderer and joints3d
    #  but vertices do not need to be saved.

    @property
    def name(self):
        # Should return string or tuple of strings.
        raise NotImplementedError()

    def init_cache(self, cache):
        # Should initialize the np arrays in cache.
        raise NotImplementedError()

    def transform_cache(self, cache):
        # Samples is the data that was extracted from the dataset.
        # Should place values in the cache.
        raise NotImplementedError()

def post_transform_2dto3d(pd, cache_subset):
    return cache_subset['cpmfeatures'], cache_subset['joints3d']
