import numpy as np
import torch

from dataset.cachetransformer.cache import CacheTransformer
from utils import timing, to_numpy


class CPMCache(CacheTransformer):
    # Relating to CPM
    name = 'heatmap_pred', 'cpm_features'

    def __init__(self, cpm, far, batch_size=300):
        self._batch_size = batch_size
        self._cpm = cpm
        self._far = far

    def init_cache(self, cache):
        batch_size, seq_len = cache['depth'].shape[:2]
        width, height = map(lambda x: x//self._cpm.stride, cache['depth'].shape[-2:])

        cache[self.name[0]] = np.zeros((batch_size, seq_len, self._cpm._num_heatmaps,
                         width, height), dtype=np.float32)

        # We have 128 features from conv4_7.
        cache[self.name[1]] = np.zeros((batch_size, seq_len, 128,
                         width, height), dtype=np.float32)

    #@timing
    def transform_cache(self, cache, callcnt=0):
        # TODO: Rewrite this to be simpler, and check for bugs. (We had to hack a solution bc. time pressure)
        ex = next(self._cpm.parameters())

        normalizer = 1
        depth = cache['depth']
        if np.issubdtype(depth.dtype, np.integer):
            normalizer = np.iinfo(depth.dtype).max
        depth = torch.as_tensor(depth.astype(np.float32) / normalizer,
                                dtype=ex.dtype, device=ex.device) * self._far

        seq_len = depth.shape[1]
        seqshape = lambda x: x.reshape(-1, seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])

        with torch.no_grad():
            self._cpm.eval()
            def unsqz(heatmap_features):
                return heatmap_features[0][0], heatmap_features[1][0]
           # process = lambda depthsubset: tuple(map(to_numpy, unsqz(self._cpm(depthsubset.to(ex.device, non_blocking=True)))))
            process = lambda depthsubset: unsqz(self._cpm(depthsubset))
            n = self._batch_size
            depth = flatshape(depth)
            results = [process(depth[i * n:(i + 1) * n].unsqueeze(0))
                          for i in range((len(depth) + n - 1) // n)]
            heatmaps, features = map(torch.cat, zip(*results)) # List of tuples to two lists.
            heatmaps, features = map(to_numpy, (heatmaps, features)) # Slow

        cache[self.name[0]][...] = seqshape(heatmaps)
        cache[self.name[1]][...] = seqshape(features)

    def post_transform_cpm_extension_features(self, cache_subset):
        input = torch.as_tensor(cache_subset[CPMJoints2dCache.name])
        target = np.concatenate([ cache_subset['joints2d'][..., :2],
                                  cache_subset['joints3d'][..., 2, np.newaxis] ], -1)
        target = torch.as_tensor(target)
        return input, target

    # def post_transform_cpm_extension_keypoints(self, heatmapper):
    #     heatmapper = heatmapper
    #     device = heatmapper.evalpoints.device
    #
    #     def fun(cache_subset):
    #         normalizer = 1
    #         depth = cache_subset['depth']
    #         if np.issubdtype(depth.dtype, np.integer):
    #             normalizer = np.iinfo(depth.dtype).max
    #         depth = torch.as_tensor(depth.astype(np.float32) / normalizer) * self._far
    #
    #         heatmap = torch.as_tensor(cache_subset[self.name[0]], device=device)
    #
    #         # We could also give neighbouring values for some unreliable keypoints
    #         #  (Test which with statistics). This way it can better self-correct.
    #         joints2dpred, confidence = heatmapper.get_prediction(heatmap)
    #         joints3d_z = heatmapper.get_prediction_value(depth, joints2dpred)
    #
    #         input = torch.cat([joints2dpred.to('cpu'), joints3d_z, confidence[..., None].to('cpu')], -1)
    #         target = np.concatenate([cache_subset['joints2d'][..., :2],
    #                                  cache_subset['joints3d'][..., 2, np.newaxis]], -1)
    #         target = torch.as_tensor(target)
    #         return input, target
    #     return fun


    def post_transform_cpm_extension_keypoints(self, heatmapper, target2d=True):
        heatmapper = heatmapper
        device = heatmapper.evalpoints.device

        def fun(cache_subset):
            normalizer = 1
            depth = cache_subset['depth']
            if np.issubdtype(depth.dtype, np.integer):
                normalizer = np.iinfo(depth.dtype).max
            depth = torch.as_tensor(depth.astype(np.float32) / normalizer) * self._far

            heatmap = torch.as_tensor(cache_subset[self.name[0]], device=device)
            # We could also give neighbouring values for some unreliable keypoints
            #  (Test which with statistics). This way it can better self-correct.
            joints2dpred, confidence = heatmapper.get_prediction(heatmap)
            joints3d_z = heatmapper.get_prediction_value(depth, joints2dpred)

            input = torch.cat([joints2dpred.to('cpu'), joints3d_z, confidence[..., None].to('cpu')], -1)
            if target2d:
                target = np.concatenate([cache_subset['joints2d'][..., :2],
                                     cache_subset['joints3d'][..., 2, np.newaxis]], -1)
            else:
                target = cache_subset['joints3d'][..., :3]

            target = torch.as_tensor(target, dtype=input.dtype)
            return input, target
        return fun


class CPMJoints2dCache(CacheTransformer):
    def __init__(self, heatmapper, batch_size=100):
        self._batch_size = batch_size
        self._heatmapper = heatmapper

    name = 'cpm_joints2d_pred'

    def init_cache(self, cache):
        batch_size, seq_len, num_joints = cache['joints2d'].shape[:3]
        cache[self.name] = np.zeros((batch_size, seq_len, num_joints, 4))

    def transform_cache(self, cache):
        device = self._heatmapper.evalpoints.device
        heatmap = torch.as_tensor(cache[CPMCache.name[0]])

        depth = torch.as_tensor(cache['depth'])
        seq_len = depth.shape[1]
        seqshape = lambda x: x.reshape(-1, seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])

        # We could also give neighbouring values for some unreliable keypoints
        #  (Test which with statistics). This way it can better self-correct.
        def transform(heatmap, depth):
            #depth = flatshape(depth.to(device, non_blocking=True))
            #heatmap = flatshape(heatmap.to(device, non_blocking=True))
            heatmap = heatmap.to(device, non_blocking=True)
            depth = depth.to(device, non_blocking=True)
            joints2dpred, confidence = self._heatmapper.get_prediction(heatmap)
            joints3d_z = self._heatmapper.get_prediction_value(depth, joints2dpred)
            return to_numpy(torch.cat([joints2dpred, joints3d_z, confidence[..., None]], -1))
        depth = flatshape(depth)
        heatmap = flatshape(heatmap)
        n = self._batch_size
        results = [transform(heatmap[i * n:(i + 1) * n], depth[i * n:(i + 1) * n])
                   for i in range((len(depth) + n - 1) // n)]
        cache[self.name][...] = seqshape(np.concatenate(results))