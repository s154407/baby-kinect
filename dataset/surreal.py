import bisect

import numpy as np
import torch

from model.SML import SML, inv_rodrigues
from utils import to_numpy, npz_headers

# We ended up not using the SURREAL dataset.

class SurrealDataset(object):
    # inspired by pytorch ConcatDataset
    trans_prefix = 'trans_'
    pose_prefix = 'pose_'
    ung_prefix = 'ung_'

    @staticmethod
    def cumsum(sequence):
        r, s = [], 0
        for e in sequence:
            l = e
            r.append(l + s)
            s += l
        return r

    def get_skip_length(self, length):
        return length // self._frame_skip
    def get_frame_length(self, skip_length):
        return skip_length * self._frame_skip

    def __init__(self,
                 seq_len=5,
                 path='C:/Users/sebse/git/surreal/datageneration/smpl_data/smpl_data.npz',
                 remove_ungendered_duplicates = True,
                 remove_monsters=True,
                 scale_factor = 1,
                 frame_skip = 1):
        self._seq_len = seq_len
        self._scale_factor = scale_factor
        self._frame_skip = frame_skip

        headers = npz_headers(path, lambda name: name.startswith(SurrealDataset.trans_prefix))
        # Note how we reduce the shape[0] (specific sequence length) by seq_len.
        # We also remove sequences that are not long enough to be used in training.

        shapes = {name[len(SurrealDataset.trans_prefix):]: self.get_skip_length(shape[0])
                  for name, shape, _ in headers if self.get_skip_length(shape[0])>=seq_len}

        if remove_ungendered_duplicates:
            byebye = []
            for name, _ in shapes.items():
                if name.startswith(SurrealDataset.ung_prefix):
                    if name[len(SurrealDataset.ung_prefix):] in shapes:
                        byebye.append(name) # There is a gendered version of this pose data.
            for ungname in byebye:
                del shapes[ungname]

        if remove_monsters:
            # Very bad data. 143_21 in particular... And there are still some more probably.
            monsters = ['18', '19', '143_21']
            byebye = []
            for monster in monsters:
                amonster = lambda name: name.startswith(monster) or name.startswith(SurrealDataset.ung_prefix + monster)
                byebye += filter(amonster, shapes.keys())
            for monster in byebye:
                del shapes[monster]

        def extract_numbers(name): # For sorting by sequence number.
            if name.startswith(SurrealDataset.ung_prefix):
                name = name[len(SurrealDataset.ung_prefix):]
            return tuple(int(s) for s in name.split('_'))

        self._file = np.load(path)
        self.close = self._file.close # To allow for closing the file.

        #sequences = sorted(shapes.keys(), key=extract_numbers) <- If a functional style was better supported (f-composition) it could be almost as simple as this.
        self._sequences = sorted(shapes.items(), key=lambda it: extract_numbers(it[0]))
        # Explanation for the below:
        #  E.g. if seq_len is 1 and length is 1, the resulting sequence must have length 1
        #       if seq_len is 5 and length is 5, the resulting sequence must have length 1
        #       if seq_len in 5 and length is 6, the resulting sequence must have length 2
        self.cumulative_sizes = SurrealDataset.cumsum([length-seq_len+1 for _, length in self._sequences])

    def get_sequence(self, sequence, subsequence):
        subsequence = np.s_[self.get_frame_length(subsequence):
                            self.get_frame_length(subsequence+self.seq_len):
                            self._frame_skip]
        name, _ = sequence
        pose = self._file[SurrealDataset.pose_prefix + name]
        trans = self._file[SurrealDataset.trans_prefix + name]
        pose, trans = np.array(pose[subsequence], copy=True), np.array(trans[subsequence], copy=True)

        # Make the initial frame face the camera.
        rotation = torch.as_tensor(pose[:, :3].reshape(-1, 1, 3))
        rotation = SML.rodrigues(rotation, torch.eye(3, dtype=rotation.dtype))
        rotation = rotation[0].t() @ rotation # Convert to relative rotations from first frame.

        lookatcameraplane = rotation.new_tensor([[1, 0, 0], [0, -1, 0], [0, 0, -1]])
        rotation = rotation @ lookatcameraplane
        pose[:, :3] = inv_rodrigues(rotation)
        # inv_rodrigues fails with the 180 rotation... the axis of rotation is completely ambiguous
        pose[0, :3] = [np.pi, 0, 0]  # = inv_rodrigues(torch.as_tensor(lookatcameraplane)[None])[0] in a perfect world

        # Now we need to modify the translation accordingly.
        # Since we rotated the coordinate system on all frames:
        transdiff = trans-trans[0]
        transdiff = (transdiff[:, None] @ to_numpy(rotation[0]))[:, 0]
        # Then apply a scale factor to the frame-relative translation.
        # This needs to be done because e.g. an adult step, jump, etc. is larger than an infants.
        trans = trans[0] + transdiff * self._scale_factor
        return pose, trans, None, None

    def __len__(self):
        return self.cumulative_sizes[-1]

    def __getitem__(self, idx):
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]
        return self.get_sequence(self._sequences[dataset_idx], sample_idx)

    def getinfo(self, idx):
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]
        return self._sequences[dataset_idx], self.get_frame_length(sample_idx)

    @property
    def seq_len(self):
        return self._seq_len

    @property
    def betas(self):
        return []

class PoseDataset(object):
    def __len__(self):
        raise NotImplementedError()

    def __getitem__(self, idx):
        # Should return pose, translation, betas and the plane for a subsequence of length seq_len.
        raise NotImplementedError()

    @property
    def seq_len(self):
        raise NotImplementedError()

    @property
    def betas(self):
        raise NotImplementedError()