import os

import h5py
from pathlib import Path

import bisect
import torch
from torch import utils
from torch.nn import functional as F
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt

from torch.utils.data.sampler import SubsetRandomSampler


from PIL import Image
import cv2

from dataset.cachetransformer.cache import Cache
from dataset.sequencedataset import SkippedDownsampledSequenceDataset
from dataset.surreal import SurrealDataset

from model.cpm import CPM

import numpy as np

from utils import to_numpy

class MikkelSequence(utils.data.Dataset):
    num_joints = 19

    def __init__(self,h5_file):
        self._depth = h5_file['filtered_matrix']
        self._joints3d = h5_file['filtered_pos']
        self._timestamps = h5_file['timestamps']
        self._filename = h5_file.filename
        if 'rotated_pos' in h5_file.keys():
            self._joints3d = h5_file['rotated_pos']
        if 'downsampled_depth' not in h5_file.keys():
            downsampled_depth = np.array(self._depth)
            downsampled_depth = torch.as_tensor(downsampled_depth.astype('float32'))
            downsampled_depth = F.interpolate(downsampled_depth[: , None], scale_factor=1/2, mode='area')[:,0].numpy().astype('uint16')
            h5_file.create_dataset("downsampled_depth", data=downsampled_depth)
            h5_file.close()
            print("Downsample created.") 
        else:
            self._depth = h5_file['downsampled_depth']

        


    def __getitem__(self, index):
        depth = np.copy(self._depth[index])
        joints3d = np.copy(self._joints3d[index]).reshape(-1, self.num_joints, 3)
        if depth.shape[-2:] == (240, 320):
            # Nora mode. Image is in portrait mode.
            depth = np.rot90(depth, -1, axes=(-2, -1))
        else:
            # Liva_flip mode. Is flipped.
            depth = np.flip(depth, axis=-1)
        return depth, joints3d

    def __len__(self):
        return len(self._depth)


class MikkelDataset(utils.data.dataset.ConcatDataset):
    def __init__(self, seq_len=5, downsample_factor=1, skip_factor=1,
                 startseq=0, endseq=-1, file_path='/Volumes/exFAT'):
        self._path = file_path
        self.seq_len = seq_len

        path = Path(file_path)
        assert(path.is_dir())
        files = sorted(path.glob('*.mat'))
        
        # Shuffle the files in a deterministic, pseudarandom way.
        # Just so we can split it in training, test and validation sets, not having
        #  to worry about any bias in the file name order.
        files = np.random.RandomState(seed=42).permutation(files)
        files = files[startseq:endseq]
        self.sequence_names = [file.name for file in files]

        if len(files) < 1:
            # FIXME: Ensure there is the right number of files!
            raise RuntimeError('No hdf5 datasets found')
        # k = 0
        # for seq_path in files:
        #     print(seq_path)
        #     print(k)
        #     k = k + 1
        #     MikkelSeqence(h5py.File(seq_path.resolve()), seq_len)
        sequences = [MikkelSequence(h5py.File(sequence_path.resolve(), 'r+')) for sequence_path in files]
        # TODO maybe here check for missing frames and split sequences ? Or in matlab?
        super().__init__([SkippedDownsampledSequenceDataset(seq, seq_len, downsample_factor, skip_factor) for seq in sequences])

    def get_sample_id(self, idx):
        # See __getitem__ of ConcatDataset
        dataset_idx = bisect.bisect_right(self.cumulative_sizes, idx)
        if dataset_idx == 0:
            sample_idx = idx
        else:
            sample_idx = idx - self.cumulative_sizes[dataset_idx - 1]

        name = self.sequence_names[dataset_idx]
        id = self.datasets[dataset_idx].get_sample_id(sample_idx)
        return name, id

def stats():
    data = MikkelDataset(seq_len=1, file_path='H:')
    alljoints3d = []
    minjointsz = 100
    maxjointsz = -100
    for depth, joints3d in data:
        minjointsz = min(minjointsz, joints3d[..., 2].min())
        maxjointsz = max(maxjointsz, joints3d[..., 2].max())
        print(minjointsz, maxjointsz)
        pass

def mikkel_dataset(file_path = 'H:', seq_len=1, downsample_factor=1, skip_factor=1):
    from dataset.mikkel import MikkelDataset
    mikkel_train = MikkelDataset(seq_len=seq_len, downsample_factor=downsample_factor,
                                 skip_factor=skip_factor, file_path=file_path,
                                 startseq=0, endseq=100)
    mikkel_test = MikkelDataset(seq_len=seq_len, downsample_factor=downsample_factor,
                                skip_factor=skip_factor, file_path=file_path,
                                startseq=100, endseq=100+15)
    mikkel_valid = MikkelDataset(seq_len=seq_len, downsample_factor=downsample_factor,
                                 skip_factor=skip_factor, file_path=file_path,
                                 startseq=100+15, endseq=-1)

    def hash_dataset(dataset):
        return hash(tuple(dataset.sequence_names))
    print('Creating Mikkel Datasets. Hashes of the sequence names used in the splits:',
          *map(hash_dataset, (mikkel_train, mikkel_test, mikkel_valid)))

    print('Train, Test and Validation datasets sizes (in frames):',
          *list(map(len, (mikkel_train, mikkel_test, mikkel_valid))))
    print('Train, Test and Validation datasets sizes (in sequences):',
          *list(map(lambda mikkel: len(mikkel.sequence_names), (mikkel_train, mikkel_test, mikkel_valid))))

    return mikkel_train, mikkel_test, mikkel_valid

if __name__ == "__main__":
    #stats()
    from render.cameraparameters import CameraParameters

    md = MikkelDataset(seq_len=1)


    #joints2d = cp.project_points(proj, view, np.concatenate([joints, np.ones((joints.shape[:-1], 1))], -1))[..., :2]
    set = [0] + md.cumulative_sizes[:-1]
    #set = set[:1] + set[7:8]

    for debug_idx, i in enumerate(set):
        i = i + 2000
        name, frame = md.get_sample_id(i)

        # for baby in babies:
        #     if baby 
        # if md.sequence_names[i] not in babies:
        #     continue
        depthimg, joints3d = md[i]
        depthimg, joints3d = depthimg[0], joints3d[0]  # Seq_len is 1, remove the singleton dim.

        if False:
            cp = CameraParameters(480, 480, preset='MIKKEL')
            proj = cp.perspective(near=0.1, far=5)
            view = cp.viewport()
        else:
            cp = CameraParameters(640 / 2, 480 / 2, preset='MIKKEL')
            proj = cp.perspective(near=0.1, far=5)
            view = cp.viewport()

        # depthimg = np.rot90(depthimg, 1)

        joints3d = np.concatenate([joints3d, np.ones((*joints3d.shape[:-1], 1))], -1)  # Homogeneous coordinates
        joints2d = cp.project_points(proj, view, joints3d)  # 2D homogeneous coordinates

        ax = plt.subplot()
        ax.imshow(depthimg.T, cmap='gray')
        name, frame = md.get_sample_id(i)
        ax.set_title(name + ' frame ' + str(frame) + ' (' + str(i) + ')')
        # plt.show()
        # ax.imshow(np.ones(depthimg.T.shape))
        from matplotlib.patches import Circle
        for j in range(len(joints2d)):
            j2d = joints2d[j, :2]
            # j2d[0], j2d[1] = -j2d[1], j2d[0]
            ax.add_patch(Circle(j2d, 0.3, color='g'))
            ax.text(*j2d, str(j), fontsize=6)
        plt.show()
    print('?')
    exit()
    cpm = CPM(num_heatmaps=24)
    cpm.load_state_dict(torch.load('./trainednetworks/CPM7-MIKKEL-SSL-BEST.pt', map_location='cpu')['model_state_dict'])

    example = 900
    ex_input, ex_target = MikkelDataset(seq_len=1, file_path='H:')[example]
    output_heatmap, _ = cpm(torch.as_tensor(ex_input.astype(np.float32))[None, None])
    output_heatmap = to_numpy(output_heatmap[0,0])

    print(ex_target)

    columns = 5
    for i in range(output_heatmap.shape[-3] // columns):
        for j in range(columns):
            idx = i * columns + j
            ax = plt.subplot(output_heatmap.shape[-3] // columns, columns, idx + 1)
            # plt.imshow(pred_heatmap[idx].T, vmin=0, vmax=1)
            plt.imshow(output_heatmap[idx].T)
            ax.axes.get_xaxis().set_ticks([])
            ax.axes.get_yaxis().set_ticks([])
            ax.set_xlabel(str(idx))
    plt.show()

    print(output_heatmap.shape)
    exit()
    # Train CPM on Mikkels dataset
    
    torch.manual_seed(7)
    resolution = (240, 320)
    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    m_seq = MikkelDataset()
    batch_size = 16

    train_idx = list(range(m_seq.cumulative_sizes[-10]))
    test_idx = list(range(m_seq.cumulative_sizes[-10],len(m_seq)))

    train_set = torch.utils.data.Subset(m_seq,train_idx)
    test_set = torch.utils.data.Subset(m_seq,test_idx)

    # From https://gist.github.com/kevinzakka/d33bf8d6c7f06a9d8c76d97a7879f5cb
    if False: #if shuffle
        np.random.seed(random_seed)
        np.random.shuffle(train_idx)
        np.random.shuffle(test_idx)

    # Skal der genereres heatmaps?

    # Lav et datasæt hvor den laver om til 2d
    # Brug cache transformer osv til at projektere til 2d og generere heatmaps. Senere også brug til at generere cpm outputs

    from render.renderer import Renderer

    # joints2d = Renderer.project_points(m_seq.datasets[0]._joints3d[1,:,:])

    from trainer import Trainer
    trainer = Trainer(train_set, test_set,
                      # [b, s, c, w, h]
                      loss_fn=CPM.loss
                      )

    cpm = CPM(num_heatmaps=m_seq.datasets[0]._joints3d[0,:,0].shape[0])

    #cpm.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'], strict=False)
    cpm = cpm.to(device, dtype, non_blocking=True)

    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    batch_size = 4
    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = torch.optim.Adam(cpm.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(cpm, batch_size,
                                     optimizer, scheduler,
                                     max_epochs=250, patience=10,
                                     max_batch_size=batch_size)
        Trainer.plot_training(*train_result[:2], offset=5)

# Get image from all videos
# '    k = 0
#     for dataset in m_seq.datasets:
#         filename = dataset._joints3d.file.filename
#         print(filename)
#         img = dataset._depth[1000,:,:]
#         img_scaled = cv2.normalize(img, dst=None, alpha=0, beta=65535, norm_type=cv2.NORM_MINMAX)
#         cv2.imwrite('test_img/{}.png'.format(k), img_scaled)
#         k = k +1'