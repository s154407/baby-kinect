import torch

from render.cameraparameters import CameraParameters
from render.renderer import Renderer
import numpy as np
from utils import timing
from model.SML import SML, inv_rodrigues # Unused???


def augment_flip(v, Jtr, planes):

    # From jointlist.txt in MINIRGBD:
    chiral_joints_left  = [2, 5, 8, 11, 14, 17, 19, 21, 23]
    chiral_joints_right = [3, 6, 9, 12, 15, 18, 20, 22, 24]

    minus1 = lambda lst: [x-1 for x in lst]
    chiral_joints_left, chiral_joints_right = map(torch.as_tensor, map(minus1, (chiral_joints_left, chiral_joints_right)))


    n_sequences = v.shape[0]
    # The "lucky" sequences, that get flipped. It is simply a binary mask.
    lucky = torch.randint(size=(n_sequences,), high=2, dtype=torch.uint8)
    v[lucky, ..., 0] *= -1
    planes[lucky, ..., 0] *= -1

    Jtr_augmented = Jtr.clone()
    Jtr_augmented[..., 0] *= -1
    # When we flip the image we must flip the annoations of any chiral joints.
    # The 3D position in space
    Jtr_augmented[..., chiral_joints_left, :], Jtr_augmented[..., chiral_joints_right, :] = \
        Jtr_augmented[..., chiral_joints_right, :], Jtr_augmented[..., chiral_joints_left, :]

    Jtr[lucky] = Jtr_augmented[lucky]



def augmented_rotation(pose, trans, plane):
    # We will rotate the model around the axis of the plane it sits on
    # This ensures that it will not clip through it no matter the angle.
    axis = plane[:3]  # The normal of the plane.
    axis /= np.linalg.norm(axis)  # Normalize.
    angle = torch.zeros((1,), dtype=torch.double).uniform_(2 * np.pi).item()
    # The rest is similar to SML.rodrigues.
    # See also https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
    m = np.array([
        [0, -axis[2], axis[1]],
        [axis[2], 0, -axis[0]],
        [-axis[1], axis[0], 0]])
    cos = np.cos(angle)
    R = np.eye(3) * cos + (1 - cos) * np.outer(axis, axis) + np.sin(angle) * m
    # Now we need to modify the translation accordingly.
    # Since we rotated the coordinate system on all frames:
    transdiff = trans - trans[0]
    transdiff = (R @ transdiff[..., None])[..., 0]
    new_trans = trans[0] + transdiff

    rotation = torch.as_tensor(pose[:, :3].reshape(-1, 1, 3), dtype=torch.double)
    rotation = SML.rodrigues(rotation, torch.eye(3, dtype=torch.double))
    rotation = torch.as_tensor(R) @ rotation

    # The plane normal should probably be rotated, like we do.
    # Make sure then, the plane is still the same distance from global joint.
    return inv_rodrigues(rotation), new_trans

def in_augmentation_area(points, width, height, augmentation_screen_z_min, augmentation_screen_z_max):
    # TODO: If we didnt apply a full projection, but only multiplied by perspective matrix we didn't
    #  have to use width and height args. Then the values would be in NDZ space, i.e. between -1 and 1 if inside frustum.
    good_augmentation = 0 <= points[..., 0]
    good_augmentation &= 0 <= points[..., 1]
    good_augmentation &= augmentation_screen_z_min <= points[..., 2]  # None to keep the dimension
    good_augmentation &= points[..., 0] <= width
    good_augmentation &= points[..., 1] <= height
    good_augmentation &= points[..., 2] <= augmentation_screen_z_max
    return good_augmentation

def augmented_positions(Jtr, renderer : Renderer, cube_start, cube_end):
    # This function takes a set of points, Jtr. It doesn't have to be joints
    # E.g. if it is all the vertices, it ensures that all vertices are in view of
    #  the camera. It then returns the random offsets that allow these points
    # to still be inside the frustum.

    # This algorithm has a keeps trying with a probability to success
    #  E.g. if it succeseeds 50% of the time, the runtime is n*log(n), like merge sort.
    #  It is of course probabilistic, so this will be the expected runtime.
    #  The base of the logarithm then depends on the probability of success,
    #   which agian depends on the shape of the frsustum. The closer to a cube/3d-rectangular shape
    #   the better.
    # With an emperical fit we found the iterations to succeed around 21% of the time, and
    #  the algortithm halting after about 30 iterations, with a long tail.

    # First we get the max xyz position over the joints over the sequence.
    minJtr = Jtr[..., :3].min(-3, keepdim=True)[0].min(-2, keepdim=True)[0] # [B, S, J, XYZ1] -> [B, 1, 1, XYZ]
    maxJtr = Jtr[..., :3].max(-3, keepdim=True)[0].max(-2, keepdim=True)[0]
    offsets = -maxJtr # Initially, slide the model completely off screen, so now all positions are negative.
    span = maxJtr-minJtr


    # Get important corners of the frustum. (Specifically the subset that is the augmentation area)
    augmentation_screen_z = renderer.project_z(offsets.new_tensor([cube_start, cube_end])) # Min and max screen-z
    # First we generate their screen-space positions
    corners = offsets.new_tensor([0, 1])
    # TODO: We could e.g. give the augmentation area a 10% margin, so the CPM
    #  doesn't have to learn to deal with the case where a joint is exactly at the end of screen.
    xv, yv, zv, wv = torch.meshgrid([corners*renderer.width,
                                     corners*renderer.height,
                                     augmentation_screen_z,
                                     offsets.new_ones(1)])
    corners = torch.cat([xv, yv, zv, wv], -1).reshape(-1, 4)
    # Then we project them to 3D space
    corners = renderer.unproject_points(corners)[..., :3]

    # Now we get a 3D bounding box around the frustum.
    mincorners = corners.min(-2)[0]
    maxcorners = corners.max(-2)[0]
    # We will sample random points uniformly inside this cube.
    uniform = torch.distributions.Uniform(mincorners, maxcorners + span.max(0)[0].squeeze())
    # Then we want to check if offsetting the joints by these positions allow them to
    #  remain completely inside the frustum. If not, we try again. With this simple method
    #  we keep a uniform probability density in 3D space, while ensuring the position-augmented
    #  joints are completely inside the frustum still.
    # Since we moved all the points back we must add something to the maxcorners, so it extends a little
    #  further outside the frustum

    need_augmentation = torch.ones(Jtr.shape[0], dtype=torch.uint8, device=Jtr.device)
    while torch.any(need_augmentation):
        augmented_offsets = offsets[need_augmentation].clone() # Clone to be safe. (We have been burnt by this in the past)
        augmented_offsets += uniform.sample( (augmented_offsets.shape[0],1,1) ) # Sample from the uniform, one for each sequence.
        augmented_Jtr = Jtr[need_augmentation].clone()
        augmented_Jtr[..., :3] += augmented_offsets

        # Now that we have a set of augmented joints, one for each sequence that need augmentation
        #  we will project them to screen-space, making it easier to see if they're contained in the frustum.
        augmented_Jtr_screen = renderer.project_points(augmented_Jtr)
        good_augmentation = in_augmentation_area(augmented_Jtr_screen,
                                                 renderer.width, renderer.height, *augmentation_screen_z)
        # good augmentation has shape [B, S, J]. Now we reduce it to just [B]:
        good_augmentation = good_augmentation.all(-1) # Check if augmentation is good on all joints
        good_augmentation = good_augmentation.all(-1) # Check if augmentation is good on all frames in sequence.
        # The algorithm becomes unreadble from here on. Sorry.
        # Get the indices that needed augmentation, that we found a good augmentation for:
        good_indices = need_augmentation.nonzero().squeeze(-1)[good_augmentation] # For some reason nonzero adds a singleton dimension.
        # These indices do not need augmentation anymore
        need_augmentation[good_indices] = 0 # 0 = False
        offsets[good_indices] = augmented_offsets[good_augmentation]
        #print(need_augmentation.sum().item())

    # Verify that all points are contained in frustum
    test = Jtr.clone()
    test[..., :3] += offsets
    test = renderer.project_points(test)
    pass_test = in_augmentation_area(test, renderer.width, renderer.height, *augmentation_screen_z)
    assert torch.all(pass_test)

    return offsets

if __name__ == "__main__":
    from dataset.cachedSMLdataset import CachedSMLDataset
    from dataset.minirgbd import MINIRGBDDataset
    from model.SML import SML, inv_rodrigues
    from utils import to_numpy, timing, timing
    import cv2 # Better than matplotlib to show a montage of images.

    seq_len = 32
    skip_factor = seq_len # No overlap between sequences.
    camera_params = CameraParameters(640//2, 480//2, preset='MIKKEL')

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # TODO: Visualize the span of poses in frustum.
    model = SML(nose_vertex=True).to(device, dtype, non_blocking=True)
    renderer = Renderer(model, camera_params, visible=False, decoration=False)
    train_set = MINIRGBDDataset(seq_len, skip_factor=skip_factor,
                                startseq=0, endseq=10, random_betas=False, random_rotation=True)
    def render_images(cache):
        normalizer = 1
        depth = cache['depth']
        if np.issubdtype(depth.dtype, np.integer):
            normalizer = np.iinfo(depth.dtype).max
        depth = torch.as_tensor(depth.astype(np.float32) / normalizer)
        return depth

    pd_train = CachedSMLDataset(renderer, model, [train_set],
                                cache_size=32, render_batch_size=1,
                                augment_position=True, post_transform=render_images)

    lastimg = len(pd_train)
    for i in range(10):
        print(i)
        depthsequence = pd_train[i] # [S, C, W, H]
        for depth in depthsequence:
            cv2.imshow('Image', to_numpy(depth[0]).T)
            cv2.waitKey(1000//30)
    cv2.destroyAllWindows()
