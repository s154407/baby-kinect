import torch
import torch.nn.functional as F

import numpy as np
from matplotlib import pyplot as plt
from torch import nn

from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.minirgbd import MINIRGBDDataset
from model.SML import SML
from model.cpm import CPM
from dataset.cachetransformer.cpmcache import CPMJoints2dCache, CPMCache
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from render.cameraparameters import CameraParameters
from render.renderer import Renderer
from utils import to_numpy, determinism

class CPMRegressor(nn.Module):
    activation = nn.ReLU()
    
    def __init__(self, num_joints, num_heatmaps, project_z, unproject_p, num_img_channels=1,
                 channels=128, linear_size=1024, bidirectional=False):
        # num_joints is the number of joints in the body model. The dimension of this modules output.
        # num_heatmaps is the dimension of the output from the CPM, generally equal to num_joints, unless
        #  background is also estimated. Then it is num_joints+1.
        # num_img_channels is the number of channels in the input image to the CPM. Here: 1, since only depth.
        super().__init__()
        self.project_z = project_z
        self.unproject_p = unproject_p

        #self.special_conv_1 = nn.Conv2d(in_channels=num_heatmaps+num_img_channels+2,out_channels=channels,kernel_size=3,stride=1,padding=1)
        self.special_conv_1 = nn.Conv2d(in_channels=100,out_channels=channels,kernel_size=3,stride=1,padding=1)
        self.special_conv_2 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=3,stride=1,padding=1)
        self.fa_conv_1 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=5,stride=2,padding=1)
        self.fa_conv_2 = nn.Conv2d(in_channels=channels,out_channels=num_img_channels+2,kernel_size=5,stride=2,padding=1)
        self.avg = nn.AvgPool2d(kernel_size=5, stride=2,padding=1)
        #self.maxpool = nn.MaxPool2d(kernel_size=2,stride=2)
        # Calculate 5 * 5 from some input resolution or something.
        # TODO: The dimensions 9 and 6 are because the convs above makes the image smaller (stride 2)
        #  Maybe find a better way than hardcoding them.
        #self.fc = nn.Linear(in_features=(num_img_channels+2) * 9 * 6+num_joints*3, out_features=linear_size)
        self.fc = nn.Sequential(nn.Linear(in_features=(num_img_channels+2) * 9 * 6+num_joints*3, out_features=linear_size),
                                self.activation,
                                nn.Linear(linear_size, linear_size))

        num_directions = 2 if bidirectional else 1
        #self.lstm = nn.LSTM(input_size=linear_size, # dropout=0.1,
        #                    hidden_size=linear_size, bidirectional=bidirectional,
        #                    batch_first=True)
        self._lstm_state = None
        #self.initial_hidden = nn.Parameter(torch.zeros((num_directions,
        #                                                1,
        #                                                linear_size), dtype=torch.float))
        #self.initial_cell = nn.Parameter(torch.zeros((num_directions,
        #                                                1,
        #                                                linear_size), dtype=torch.float))
        #print(self.initial_hidden.shape)
        self.fc_final = nn.Linear(in_features=linear_size*num_directions, out_features= 3 * num_joints)
        self._num_joints = num_joints
        with torch.no_grad():
            self.fc_final.bias.reshape(-1, 3)[..., 2] += 1 # 0.85
            pass

        prefix = torch.zeros((num_joints,), dtype=torch.float)
        if num_joints == 25 or num_joints==24: # SMIL joints
            prefix[:24] = torch.as_tensor([0.0457, 0.0447, 0.0419, 0.0738, 0.0230, 0.0229, 0.0511, 0.0158, 0.0159,
                                      0.0431, 0.0119, 0.0101, 0.0438, 0.0387, 0.0402, 0.0410, 0.0268, 0.0287,
                                      0.0220, 0.0233, 0.0162, 0.0166, 0.0092, 0.0119])
            # We save these prefix values so we can use them in the space_pred.
            # This way, we get a more fair comparison of how much improvement this module gives.
        self.register_buffer('prefix_values', prefix.clone())
        self.register_buffer('ones', torch.ones((1,), dtype=torch.float))
        xrange = torch.arange(0, 40, dtype=torch.float)
        yrange = torch.arange(0, 30, dtype=torch.float)
        xv, yv = torch.meshgrid([xrange, yrange])
        self.register_buffer('evalpoints', torch.stack([xv, yv], 0))

        sizes = (40, 30)
        self.maxpool = nn.MaxPool2d(kernel_size=sizes, stride=sizes, return_indices=True)
        self.maxpool2 = nn.MaxPool2d(kernel_size=2, stride=2, return_indices=True)
        self.conv3d1 = nn.Conv3d(25, 25, groups=25, kernel_size=3,stride=1,padding=1)
        self.conv3d2 = nn.Conv3d(25, 25, groups=25, kernel_size=3,stride=1,padding=1)
        self.conv3d3 = nn.Conv3d(25, 25, groups=25, kernel_size=3,stride=1,padding=1)


    def forward(self, input):
        heatmap, cpm_features, depth, joints2d_skin = input
        batch_size = cpm_features.shape[0]
        seq_len = cpm_features.shape[1]
        seqshape = lambda x: x.reshape(-1, seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])

        depth = F.interpolate(flatshape(depth), scale_factor=1/CPM.stride, mode='area')
        depth_screen = self.project_z(depth)
        xy_screen = self.evalpoints.unsqueeze(0).expand(batch_size*seq_len, -1, -1, -1)
        screen = torch.cat([xy_screen, depth_screen, self.ones.expand(*depth_screen.shape)], -3)
        screen = screen.permute(0, 2, 3, 1) # Move channel up first, so it is homogeneous coords.
        screen = self.unproject_p(screen)[..., :3] # Dangerous in-place operation.
        bad_space_pred = self.unproject_p(torch.cat([joints2d_skin[..., :2],
                                                     self.project_z((joints2d_skin[..., 2]).unsqueeze(-1)),
                                                     self.ones.expand(*joints2d_skin.shape[:-1], 1)], -1))[..., :3]
        test = screen.unsqueeze(1)-flatshape(bad_space_pred)[:, :, None, None]
        test = torch.cat([test, flatshape(heatmap).unsqueeze(-1)], -1)
        test = test.permute(0, 1, -1, 2, 3)

        screen = screen.permute(0, 3, 1, 2) # Back to the normal channel format.
        #test = test.reshape(-1, *test.shape[2:])
        test = self.activation(self.conv3d1(test))
        test = self.activation(self.conv3d2(test))
        test = self.activation(self.conv3d3(test))
        #values, indices = self.maxpool(flatshape(heatmap))
        #screenvalues = torch.gather(screen.flatten(2).unsqueeze(2).expand(-1, -1, 25, -1), 3, indices.flatten(2).unsqueeze(1).expand(-1, 3, -1, -1))
        #values = torch.cat([screenvalues.squeeze(-1).shape, values[..., 0, 0].unsqueeze(1)])

        # values = torch.gather(flatshape(heatmap).flatten(2), 2, indices.flatten(2)).view(values.shape)
        # nn.MaxPool2d(5,3,return_indices=True)(flatshape(heatmap))[1].shape
        #screen = seqshape(screen)
        #x = torch.cat([screen, heatmap], -3) # XYZC.

        # TODO: Try bn on heatmaps/features.
        #values, indices = self.maxpool(flatshape(heatmap))
        #features.unsqueeze(2)[seqshape(indices).unsqueeze(3)].shape

        #cpm_features = torch.cat([heatmap, cpm_features, depth], -3) # Concat channels
        #cpm_features = flatshape(cpm_features)
        #x = joints2d_features.reshape(seq_len*batch_size, *joints2d_features.shape[2:]) # To image

        x = self.activation(self.special_conv_1(test.flatten(1,2)))
        x = self.activation(self.special_conv_2(x))
        x = self.activation(self.fa_conv_1(x))
        x = self.activation(self.fa_conv_2(x)) + self.avg(self.avg(screen))
#RuntimeError: Given groups=1, weight of size [3, 128, 5, 5], expected input[1, 3, 9, 6] to have 128 channels, but got 3 channels instead
        #x = self.maxpool(x)
        x = torch.cat([x.reshape(batch_size, seq_len, -1), bad_space_pred.reshape(batch_size, seq_len, -1)], -1)
        #x = x.reshape(batch_size, seq_len, -1) # To vector.
        x = self.activation(self.fc(x))
        #x, self._lstm_state = self.lstm(x,
        #                                (self.initial_hidden.expand(-1, batch_size, -1).contiguous(),
        #                                 self.initial_cell.expand(-1, batch_size, -1).contiguous()))
        x = x.reshape(batch_size * seq_len, -1) # To vector
        x = self.fc_final(x)
        x = x.reshape(-1, self._num_joints, 3)

        #####
        space_pred = self.unproject_p(torch.cat([joints2d_skin[..., :2],
                                                 self.project_z((joints2d_skin[..., 2] + self.prefix_values).unsqueeze(-1)),
                                                 self.ones.expand(*joints2d_skin.shape[:-1], 1)], -1))[..., :3]
        return seqshape(x), space_pred

    @staticmethod
    def loss(output, target):
        # Sum over joint position values, mean over joints and time
        return ((output[0] - target) ** 2).sum(-1).mean(-1).mean(-1)

    def callback(self, pd_train, pd_test, project_p):
        from matplotlib.patches import Circle
        from mpl_toolkits.mplot3d import Axes3D # Needed for 3d plots
        ex = True
        def cb():
            nonlocal ex
            if ex:
                pd_example = pd_train
            else:
                pd_example = pd_test
            #ex = not ex

            example = 0
            input, target_ex = pd_example[example]  # Retreive sample.
            _, _, _, input_ex = input
            skip_samples = 8 # input_ex.shape[0]//10
            skip_samples = min(max(len(input_ex)-1, 1), skip_samples)
            result = pd_example._cache[example - pd_example._cache_ptr]  # Middle batch, last element in seq.
            samples = list(range(len(result['depth']))[::skip_samples])
            self.eval()
            output_ex = self(map(lambda t: t.to(device, dtype).unsqueeze(0), input)) # FIXME: Get device proper.
            output_ex = tuple(map(lambda o: o[0], output_ex)) # Remove the batch dim

            # result = pd_example._cache[len(pd_example)//seq_len//2, seq_len-1]
            pd_train.shuffle()

            for frame in samples:
                sample = result[frame]

                normalizer = 1
                depth = sample['depth']
                if np.issubdtype(depth.dtype, np.integer):
                    normalizer = np.iinfo(depth.dtype).max
                depth = depth.astype(np.float32) / normalizer * pd_example._renderer.far
                ax = plt.subplot()
                img = ax.imshow(depth[0].T, cmap='gray')
                plt.colorbar(img).set_label('depth [m]')
                ax.set_xlabel('frame ' + str(frame))
                name, startframe = pd_example.get_sample_id(example)
                ax.set_title(name + ' starting frame ' + str(startframe))

                joints2d = to_numpy(sample['joints2d'][..., :2])
                #pred2d = to_numpy(input_ex[frame, ..., :2])
                pred2d = to_numpy(project_p(torch.cat([output_ex[1][frame], output_ex[1].new_ones(output_ex[1][frame].shape[:-1] + (1,))], -1))[..., :2])
                refined2d = to_numpy(project_p(torch.cat([output_ex[0][frame], output_ex[0].new_ones(output_ex[0][frame].shape[:-1] + (1,))], -1))[..., :2])
                for joint in range(len(joints2d)):
                    gt = joints2d[joint]
                    pred = pred2d[joint]
                    refined = refined2d[joint]
                    ax.add_patch(Circle(gt, 0.4, color='g'))
                    ax.add_patch(Circle(pred, 0.3, color='r'))
                    ax.add_patch(Circle(refined, 0.2, color='y'))
                    ax.text(*gt, str(joint), fontsize=6)
                plt.show()

                ax = plt.subplot(projection='3d')
                #plt.gca().invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                #ax.invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                ax.invert_yaxis()
                ax.invert_zaxis()

                ax.axis('equal')
                ax.set_xlabel('x (frame ' + str(frame) + ')')
                ax.set_ylabel('y')
                ax.set_zlabel('z')

                joints3d, pred3d, refined3d = map(to_numpy, (sample['joints3d'][..., :3],
                                                             output_ex[1][frame, ..., :3],
                                                              output_ex[0][frame, ..., :3]))
                print('Frame', str(frame))
                print('CPM error           | refinement error')
                print(np.concatenate([
                    np.round(((joints3d - pred3d) * 1e3)),
                    np.round(((joints3d - refined3d) * 1e3))], -1))

                # The cpm error is dominated by errors in z.
                print('root mean (joints) sum square        cpm error:',
                      np.sqrt(((joints3d-pred3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) sum square refinement error:',
                      np.sqrt(((joints3d-refined3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) square z        cpm error:',
                      np.sqrt(((joints3d - pred3d) ** 2)[..., 2].mean(-1)))
                print('root mean (joints) square z refinement error:',
                      np.sqrt(((joints3d - refined3d) ** 2)[..., 2].mean(-1)))
                print(' ')

                for joint in range(len(joints3d)):
                    gt = joints3d[joint]
                    pred = pred3d[joint]
                    refined = refined3d[joint]
                    #ax.add_patch(Circle(gt, 0.3, color='g'))
                    #ax.add_patch(Circle(pred, 0.3, color='r'))
                    #ax.add_patch(Circle(refined, 0.3, color='y'))
                    ax.scatter(gt[..., 0], gt[..., 1], gt[..., 2], s=0.5, color='g')
                    ax.scatter(pred[..., 0], pred[..., 1], pred[..., 2], s=0.5, color='r')
                    ax.scatter(refined[..., 0], refined[..., 1], refined[..., 2], s=0.5, color='y')
                    ax.text(*gt, str(joint), fontsize=6)
                    # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                    #ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
                    #ax.add_patch(Arrow(*gt, refined[0] - gt[0], refined[1] - gt[1], color='w'))
                plt.show()

        return cb

    @staticmethod
    def post_transform(hmc):
        capture = cpmc.post_transform_cpm_extension_keypoints(hmc, target2d=False)
        def fun(cache_subset):
            joints2d_skin, joints3d = capture(cache_subset)
            cpm_heatmap, cpm_features = map(cache_subset.__getitem__, CPMCache.name)
            depth = torch.tensor(cache_subset['depth'])
            input = (cpm_heatmap, cpm_features, depth, joints2d_skin)
            input = tuple(map(torch.as_tensor, input))
            # We could do the image resizing here, so it will be done on the CPU (Best practices)
            return input, joints3d[..., :3]
        return fun



if __name__ == "__main__":
    #Cache()['test']
    torch.manual_seed(7)
    determinism(speed=True)
    camera_params = CameraParameters(640//2, 480//2, preset='MIKKEL')

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # Load model.
    nose = True
    model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

    seq_len = 1 # 32
    downsample_factor = 1 # To subsample the dataset - more temporal space between inputs.
    skip_factor = 10# To reduce the size of the dataset. If we skip, there will be less overlap between sequences.

    renderer = Renderer(model, camera_params, visible=False, decoration=False)
    sigma=7
    background_heatmap = False
    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma, stride=CPM.stride, background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)

    cpm = CPM(num_heatmaps=model.num_joints + hmc._background_heatmap)
    # FIXME: Uncomment next line
    cpm.load_state_dict(torch.load('./trainednetworks/CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-BEST.pt')['model_state_dict'])
    cpm.to(device, dtype, non_blocking=True)
    cpmc = CPMCache(cpm, renderer.far, batch_size=40)
    cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

    train_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=skip_factor,
                                startseq=0, endseq=1, random_betas=False, random_rotation=False)

    pd_train = CachedSMLDataset(renderer, model, [train_set],
                             cache_size=1024//4,
                             augment_position=True, transforms=cache_transformers,
                             post_transform=CPMRegressor.post_transform(hmc))

    #test_skip_factor = skip_factor//2 if skip_factor!=1 else skip_factor
    test_skip_factor = 8
    test_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=test_skip_factor,
                               startseq=10, endseq=11, random_betas=False, random_rotation=False)
    pd_test = CachedSMLDataset(renderer, model, [test_set],
                            cache_size=1024//4,
                            augment_position=False, transforms=cache_transformers,
                            post_transform=CPMRegressor.post_transform(hmc))
    print('train set:', train_set._sequence_names, 'length (in sequences):', len(pd_train))
    print('test set', test_set._sequence_names, 'length (in sequences):', len(pd_test))

    from trainer import Trainer
    #loss = torch.nn.SmoothL1Loss(reduction='none')

    trainer = Trainer(pd_train, pd_train,
                      loss_fn=CPMRegressor.loss
                      )
    if False:
        pd_train[0]
        pd_train[900]
        pd_train[900 * 2]
        pd_train[900 * 2]
        pd_train[900 * 3]
        pd_train[900 * 4]

    regresor = CPMRegressor(model.num_joints, cpm._num_heatmaps, renderer.project_z, renderer.unproject_points)
    #regresor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
    regresor = regresor.to(device, dtype)

    callback = regresor.callback(pd_train, pd_test, renderer.project_points) #cpm.callback(pd_train, pd_test, hmc, device, dtype)

    callback()
    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    batch_size = 32 #8*2*8*2*4
    optimizer = torch.optim.Adam(regresor.parameters(), lr=1e-5)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1,
                                                           verbose=True)
    train_result = trainer.resume_training(regresor, optimizer, scheduler, callback=callback, batch_size=batch_size,
    load_optim_state_dict=False, max_epochs=50, patience=10, max_batch_size=batch_size)
    Trainer.plot_training(*train_result[:2], offset=5)
    exit()

    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = torch.optim.Adam(regresor.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(regresor, batch_size,
                                     optimizer, scheduler,
                                     max_epochs=50, patience=10,
                                     callback=callback,
                                     max_batch_size=batch_size)
        Trainer.plot_training(*train_result[:2], offset=5)