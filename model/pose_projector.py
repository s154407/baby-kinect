from torch import nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data.dataset import Dataset
import matplotlib.pyplot as plt
from model.cpm import CPM
from utils import *
import os
from model.pose_transformer import CPM_transformer


class PoseRegressor(nn.Module):
    # C - Intermediate 3D prediction is regressed into pose prediction.
    def __init__(self, num_joints, linear_size=1024):
        super().__init__()
        # For first linear we have the multipliers LR: 1, Decay: 1
        #  For its bias we have LR: 2, Decay: 0.
        self.fc1 = nn.Linear(num_joints * 3, linear_size)
        self.fc2 = nn.Linear(linear_size, num_joints * 3)

    def forward(self, joints3d):
        batch_size = joints3d.shape[0]
        x = joints3d.reshape(batch_size, -1)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x.reshape(batch_size, -1, 3)

class FCBlock(nn.Module):
    def __init__(self, in_features, out_features, dropout_prop, batch_norm):
        super().__init__()
        # inplace doesn't make a difference, as a fc connection ALWAYS copies?
        # batch normalization cancels bias, so having both leads to parameters with not effect.
        fc = nn.Linear(in_features, out_features, bias=not batch_norm)
        chain = [fc]
        if batch_norm:
            chain.append(nn.BatchNorm1d(out_features, momentum=1-0.99, eps=0.001))
        chain.append(nn.ReLU())
        if dropout_prop:
            chain.append(nn.Dropout(p=dropout_prop))

        # Chain = [Linear, ReLU, (BatchNorm1d), (Dropout)].
        # (Can also be given as an ordereddict.)
        self.chain = nn.Sequential(*chain)

    def forward(self, input):
        return self.chain(input)


class PoseProjector(nn.Module):
    # https://github.com/chanyn/3Dpose_ssl/blob/master/tensorflow/m_refine_v2.py
    # P - Projector
    def __init__(self, num_joints, linear_size=1024, dropout_prob=0, batch_norm=True):
        super().__init__()
        self.fc1 = FCBlock(num_joints * 3, linear_size, dropout_prob, batch_norm)
        self.fc2 = FCBlock(linear_size, linear_size, dropout_prob, batch_norm)
        self.fc3 = FCBlock(linear_size, linear_size, dropout_prob, batch_norm)
        self.fc4 = nn.Linear(linear_size, 2 * num_joints)
        #TODO: https://stackoverflow.com/questions/49433936/how-to-initialize-weights-in-pytorch

    def forward(self, joints3d):
        batch_size = joints3d.shape[0]

        x = joints3d.reshape(batch_size, -1)
        # FC 3K -> 1024
        x = self.fc1(x)
        residual = x

        # FC 1024 -> 1024
        x = self.fc2(x)

        # FC 1024 -> 1024
        x = self.fc3(x) + residual

        # FC 1024 -> 2K
        x = self.fc4(x) # Should this really have ReLU?

        return x.reshape(batch_size, -1, 2)



class Pose3Dto2D(nn.Module):
    # C, P
    def __init__(self, cpm_transformer, regressor, projector):
        super().__init__()
        self.cpm_transformer = cpm_transformer
        # Training diverges if we include the regressor for some reason!
        #  This is even if we put very little weight on the 3d error.
        self.regressor = regressor
        # TODO: Maybe put the two regressor nn.Linear components directly in this module
        self.projector = projector

        for param in self.cpm_transformer.parameters():
            param.requires_grad = False # Freeze CPM Transformer modules

    def train(self, mode=True):
        super().train(mode)

    def forward(self, image):
        batch_size, seq_len = image.shape[:2]
        intermedjoints3d, pose2d = self.cpm_transformer(image)
        joints3d = self.regressor(intermedjoints3d)
        #joints3d = intermedjoints3d
        joints2d = self.projector(joints3d)
        return joints3d.reshape(batch_size, seq_len, -1, 3), joints2d.reshape(batch_size, seq_len, -1, 2)
        # The refinement is not actually needed to train the network.
        if not self.training:
            epsilon = 5
            tau = 20
            # In eval mode do pose refinement.
            pose2d = CPM.get_prediction(pose2d)

            # Save state
            for param in self.regressor.fc1.parameters():
                param.requires_grad = False
            joints2d.requires_grad = True
            old_regressor = self.regressor.fc2.state_dict()
            old_projector = self.projector.state_dict()

            results = []
            mask = [] # For debug print
            for i in range(len(intermedjoints3d)):
                with torch.enable_grad():
                    joints2d_refined_prev = joints2d[i].detach()
                    optimizer = optim.Adam(filter(lambda p: p.requires_grad, self.parameters()), lr=0.1)
                    #print({name: param.requires_grad for name, param in self.named_parameters()})
                    #print(optimizer.param_groups)
                    # At most 20 iterations (TODO: Check that it is far below this! or increase learning rate)
                    for _ in range(100):
                        optimizer.zero_grad()
                        error = (joints2d[i] - pose2d[i]) ** 2
                        #print(joints2d.requires_grad, pose2d.requires_grad)
                        error = error.sum(-1).mean(-1)
                        error.backward()
                        optimizer.step()
                        joints3d_refined = self.regressor(intermedjoints3d[i, None])[0] # We are repeating work for fc1 here!
                        joints2d_refined = self.projector(joints3d[i, None])[0]
                        joints_change_mm = (joints2d_refined_prev - joints2d_refined).sum(-1).sqrt().mean(-1) * 1e3
                        joints2d_refined_prev = joints2d_refined
                        if joints_change_mm < epsilon: # FIXME: <= ? To generalize to 0
                            print("Convergence")
                            # Terminate refinement when joints have converged.
                            break
                # Discard changes if they are not within a threshold.
                joints_change_mm = (joints2d[i] - joints2d_refined).sum(-1).sqrt().mean(-1) * 1e3
                if joints_change_mm < tau:
                    results.append((joints3d_refined, joints2d_refined))
                    mask.append([1])
                else:
                    results.append((joints3d[i], joints2d[i]))
                    mask.append([0])
                # Restore the module weights.
                self.regressor.fc2.load_state_dict(old_regressor)
                self.projector.load_state_dict(old_projector)
            print(mask)

            for param in self.regressor.fc1.parameters():
                param.requires_grad = True
            joints2d.requires_grad = False

        return joints3d, joints2d

def train_projector_only(device = torch.device('cuda')):
    num_joints = 24
    P = PoseProjector(num_joints, batch_norm=True, dropout_prob=0.3)
    from dataset import PoseGenerator
    from trainer import Trainer
    import torch.optim as optim
    pg = PoseGenerator()

    train_set = torch.utils.data.TensorDataset(
        pg.joints_3d_position[..., :3],
        pg.joints_2d_position[..., :2] / 368)
    split_point = int(len(train_set) * 11 / 12)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    trainer = Trainer(train_set, val_set,
                      # loss_fn=lambda output, target: ( ((output[0] - target[..., :3].reshape(-1, num_joints, 3)) ** 2).sum(-1).sqrt() * 0 + ((output[1] - target[..., -2:].reshape(-1, num_joints, 2)) ** 2).sum(-1).sqrt() ).mean(-1))
                      loss_fn=lambda output, target: ((output - target) ** 2).sum(-1).sqrt().mean(-1))
    for lr_i in [0.01]:
        # If model is extremely large, this may run out of memory.
        optimizer = optim.Adam(filter(lambda p: p.requires_grad, P.parameters()), lr=lr_i)
        batch_size = 1024 # len(train_set)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(P, batch_size, optimizer, scheduler,
                                     max_epochs=150, patience=10, callback=lambda: None)
        Trainer.plot_training(*train_result, offset=5)

class RegressorProjector(nn.Module):
    def __init__(self, regressor, projector):
        super().__init__()
        self.regressor = regressor
        self.projector = projector
        for param in self.projector.parameters():
            param.requires_grad = False
        #print({name: param.requires_grad for name, param in regressor.named_parameters()})

    def forward(self, input):
        x = self.regressor(input)
        x = self.projector(x)
        return x

def train_regressor_only(device = torch.device('cuda')):
    num_joints = 24
    C = PoseRegressor(num_joints)
    P = PoseProjector(num_joints, batch_norm=True, dropout_prob=0.3)
    P.load_state_dict(torch.load(
        os.path.join('trainednetworks', 'projector_only.pt'),
        map_location=device)['model_state_dict'])
    rp = RegressorProjector(C, P)

    from dataset import PoseGenerator
    from trainer import Trainer
    import torch.optim as optim
    pg = PoseGenerator()

    train_set = torch.utils.data.TensorDataset(
        pg.joints_3d_position[..., :3],
        pg.joints_2d_position[..., :2] / 368)
    split_point = int(len(train_set) * 11 / 12)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    trainer = Trainer(train_set, val_set,
                      # loss_fn=lambda output, target: ( ((output[0] - target[..., :3].reshape(-1, num_joints, 3)) ** 2).sum(-1).sqrt() * 0 + ((output[1] - target[..., -2:].reshape(-1, num_joints, 2)) ** 2).sum(-1).sqrt() ).mean(-1))
                      loss_fn=lambda output, target: ((output - target) ** 2).sum(-1).sqrt().mean(-1))
    for lr_i in [0.01]:
        # If model is extremely large, this may run out of memory.
        optimizer = optim.Adam(filter(lambda p: p.requires_grad, rp.parameters()), lr=lr_i)
        batch_size = 1024  # len(train_set)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1,
                                                               verbose=True)
        # scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(rp, batch_size, optimizer, scheduler,
                                     max_epochs=150, patience=10, callback=lambda: None)
        Trainer.plot_training(train_result[:2], offset=5)

class Projector(nn.Module):
    def __init__(self, regressor, projector):
        super().__init__()
        self.regressor = regressor
        self.projector = projector

    def forward(self, input):
        pose3d = self.regressor(input)
        pose2d = self.projector(pose3d)
        return pose3d, pose2d

def train_projector(device = torch.device('cuda')):
    num_joints = 24

    cpm = CPM(num_joints)
    from model.pose_transformer import load_pretrained_pose_transformer
    transformer = load_pretrained_pose_transformer(num_joints)
    cpm_transformer = CPM_transformer(cpm, transformer)
    cpm_transformer.load_state_dict(torch.load(
        os.path.join('trainednetworks', 'posetransformerbest2.pt'),
        map_location=device))
    C = PoseRegressor(num_joints)
    P = PoseProjector(num_joints, batch_norm=False, dropout_prob=0.3)
    #pose2dto3d = Pose3Dto2D(cpm_transformer, C, P).to(device=device)
    #pose2dto3d = nn.Sequential(OrderedDict([('regressor', C), ('projector', P)])).to(device)
    pose2dto3d = Projector(C, P).to(device)
    #pose2dto3d = P.to(device)
    #pose2dto3d.load_state_dict(torch.load(
    #    os.path.join('trainednetworks', 'regressor_and_projector.pt'),
    #    map_location=device)['model_state_dict'], strict=False)

    #pose2dto3d.load_state_dict(torch.load(
    #    os.path.join('trainednetworks', 'projector_bad.pt'),
    #    map_location=device)['model_state_dict'])

    from dataset import PoseGenerator
    from trainer import Trainer
    import torch.optim as optim
    pg = PoseGenerator()
    inputs = pg.joints_3d_i_position[..., :3]
    #inputs = inputs.reshape((-1, 5) + inputs.shape[1:])

    targets_3d = torch.as_tensor(pg.joints_3d_position[..., :3])
    #targets_3d -= targets_3d[0, 0, :3][None, None] # Move to center so initial gradients are not enormous
    targets = torch.cat([targets_3d,
                         torch.as_tensor(pg.joints_2d_position[..., :2]) / 368],-1)
    #targets = torch.as_tensor(pg.joints_2d_position[..., :2]) / 368
    #targets = targets.reshape(-1, 5, *targets.shape[1:])

    #dataset = PrePreprocess(MiniRGBDDataset, './preloaddataset.pkl', pg.generated_poses.permute(0, 2, 3, 1),
    #                        pg.joints_2d_position)
    train_set = torch.utils.data.TensorDataset(
        inputs,
        targets)
    print(inputs.shape, targets.shape, 'axe')
    split_point = int(len(train_set) * 11 / 12)

    def print_result():
        input, target = inputs[example], targets[example]
        # print(target[..., -2:] - (torch.as_tensor(pg.joints_2d_position[example, :, :2]) / 368))
        # target = (torch.as_tensor(pg.joints_2d_position[example, :, :2]) / 368)
        pose2dto3d.eval()
        output3d, output2d = pose2dto3d(input[None, ...].to(device))
        output3d, output2d = to_numpy(output3d[0]), to_numpy(output2d[0])
        target = to_numpy(target)
        # print((output3d - target[..., :3]) * 1e3)
        print((output2d - target[..., -2:]) * 368)

        # _, pose2d = pose2dto3d.cpm_transformer(input[None].to(device))
        # pose2d = CPM.get_prediction(pose2d)[example_seq]
        # print(to_numpy(pose2d) - target[..., -2:])
        plt.title('Estimation result')
        ax = plt.subplot()  # plt.subplot(3, 1, 1)
        ax.imshow(to_numpy(pg.generated_poses[example, 0].t()), cmap='gray')

        from matplotlib.patches import Circle, Arrow
        for joint in range(num_joints):
            gt = target[joint, -2:] * 368
            pred = output2d[joint] * 368
            ax.add_patch(Circle(gt, 0.3, color='g'))
            ax.add_patch(Circle(pred, 0.3, color='r'))
            # ax.add_patch(Circle(pose2d[joint], 0.3, color='y'))
            # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
            ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
        plt.show()

    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    trainer = Trainer(train_set, val_set,
                      # loss_fn=lambda output, target: ( ((output[0] - target[..., :3].reshape(-1, num_joints, 3)) ** 2).sum(-1).sqrt() * 0 + ((output[1] - target[..., -2:].reshape(-1, num_joints, 2)) ** 2).sum(-1).sqrt() ).mean(-1))
                      loss_fn=lambda output, target: (((output[1] - target[..., -2:]) ** 2).sum(-1)
                                                      + ((output[0][..., :2] - target[..., :2]) ** 2).sum(-1) + 20*(output[0][..., 2] - target[..., 2])**2).mean(-1))

    example = 0
    # example_seq = 0
    # def print_result():
    #     input, target = train_set[example]
    #     pose2dto3d.eval()
    #     output3d, output2d = pose2dto3d(input[None, ...].to(device))
    #     output3d, output2d = to_numpy(output3d[0, example_seq]), to_numpy(output2d[0, example_seq])
    #     target = to_numpy(target[example_seq])
    #     print((output3d - target[..., :3]) * 1e3)
    #     print((output2d - target[..., -2:]) * 368)
    #     #_, pose2d = pose2dto3d.cpm_transformer(input[None].to(device))
    #     #pose2d = CPM.get_prediction(pose2d)[example_seq]
    #     #print(to_numpy(pose2d) - target[..., -2:])
    #     plt.title('Estimation result')
    #     ax = plt.subplot()  # plt.subplot(3, 1, 1)
    #     #ax.imshow(to_numpy(input[example_seq, 0].t()), cmap='gray')
    #
    #     from matplotlib.patches import Circle, ConnectionPatch, Arrow
    #     for joint in range(num_joints):
    #         gt = target[joint, -2:] * 368
    #         pred = output2d[joint] * 368
    #         ax.add_patch(Circle(gt, 0.3, color='g'))
    #         ax.add_patch(Circle(pred, 0.3, color='r'))
    #         #ax.add_patch(Circle(pose2d[joint], 0.3, color='y'))
    #         # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
    #         ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
    #     plt.show()

    print_result()
    for lr_i in [0.000001]:

        # If model is extremely large, this may run out of memory.
        #parameters = [{'params': pose2dto3d.regressor.parameters()},
        #              {'params': pose2dto3d.projector.parameters(), 'lr': lr_i * 2}]
        #optimizer = optim.Adam(filter(lambda p: p.requires_grad, pose3dto2d.parameters()), lr=lr_i)
        optimizer = optim.Adam(pose2dto3d.parameters(), lr=lr_i)
        #optimizer = optim.SGD(pose2dto3d.parameters(), lr=0.0001)
        batch_size = 128 # len(train_set)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(pose2dto3d, batch_size, optimizer, scheduler,
                                     max_epochs=150, patience=20, callback=print_result)
        Trainer.plot_training(*train_result[:2], offset=5)

    images = to_numpy(pg.generated_poses[val_set.indices])
    images = images.transpose((0, 3, 2, 1)).repeat(3, axis=-1)  # Channels are usually last in images.
    # visualize3Dto2D(images, P, val_set)

def visualize3Dto2D(images, net, val_dataset, joints_parent=None):
    import cv2 # It is easy to show images in cv2. Pillow works just as well.

    images = images.copy()
    canvas = images
    net.eval()
    for idx, (input_jointpos_3d, target_jointpos_2d) in enumerate(val_dataset):
        if len(images.shape) == 4:
            canvas = images[idx]

        groundtruth = to_numpy(target_jointpos_2d)
        with torch.no_grad():
            output = to_numpy(net(input_jointpos_3d.unsqueeze(0))[0])

        for groundtruth_joint, output_joint in zip(groundtruth, output):
            cv2.circle(canvas, tuple(groundtruth_joint), 1, (0, 255, 0))
            cv2.circle(canvas, tuple(output_joint), 1, (255, 0, 0))

        cv2.imshow('image', canvas)
        k = cv2.waitKey(0)
        if k == 27:  # wait for ESC key to exit
            cv2.destroyAllWindows()
            exit()


def test_projector():
    num_joints = 24
    device = torch.device('cpu')
    cpm = CPM(num_joints)
    C = PoseRegressor(num_joints)
    P = PoseProjector(num_joints, batch_norm=False, dropout_prob=0.3)
    #pose2dto3d = Pose3Dto2D(cpm_transformer, C, P).to(device=device)
    #pose2dto3d = nn.Sequential(OrderedDict([('regressor', C), ('projector', P)])).to(device)
    pose2dto3d = Projector(C, P).to(device)
    pose2dto3d.load_state_dict(torch.load(
        os.path.join('trainednetworks', 'CPtest.pt'),
        map_location=device)['model_state_dict'])
    pose2dto3d.eval()
    from dataset import PoseGenerator
    import torch.optim as optim
    pg = PoseGenerator()

    example = 2500
    unrefined = pg.joints_3d_i_position[example, ..., :3]
    target = pg.joints_3d_position[example, ..., :3]

    joints3d, joints2d = pose2dto3d(unrefined[None])
    joints3d, joints2d = joints3d[0].detach(), joints2d[0].detach()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(unrefined[..., 0], unrefined[..., 1], unrefined[..., 2], c='y', marker='x')
    ax.scatter(joints3d[..., 0], joints3d[..., 1], joints3d[..., 2], c='r', marker='o')
    ax.scatter(target[..., 0], target[..., 1], target[..., 2], c='g', marker='o')
    ax.set_xlabel('X-axis')
    ax.set_ylabel('Y-axis')
    ax.set_zlabel('Z-axis')
    plt.axis('equal')
    plt.show()

    epsilon = 5
    tau = 20
    from matplotlib.patches import Circle, Arrow

    # Save state
    for param in pose2dto3d.regressor.fc1.parameters():
        param.requires_grad = False
    for param in pose2dto3d.regressor.fc2.parameters():
        param.requires_grad = True
    for param in pose2dto3d.projector.parameters():
        param.requires_grad = True
    joints2d_refined = joints2d.clone()
    joints2d_refined.requires_grad = True
    pose2d = pg.joints_2d_position[example, :, :2] / 368
    results = []
    mask = []  # For debug print
    with torch.enable_grad():
        joints3d_refined_prev = joints3d.detach()
        optimizer = optim.Adam(filter(lambda p: p.requires_grad, pose2dto3d.parameters()), lr=0.00003)
        # print({name: param.requires_grad for name, param in self.named_parameters()})
        # print(optimizer.param_groups)
        # At most 20 iterations (TODO: Check that it is far below this! or increase learning rate)
        for _ in range(10):
            optimizer.zero_grad()
            error = (joints2d_refined - pose2d) ** 2
            # print(joints2d.requires_grad, pose2d.requires_grad)
            error = error.sum(-1).mean(-1)
            error.backward()
            optimizer.step()
            joints3d_refined = pose2dto3d.regressor(unrefined[None])[0]  # We are repeating work for fc1 here!
            joints2d_refined = pose2dto3d.projector(joints3d_refined[None])[0]
            joints_change_mm = (joints3d_refined_prev - joints3d_refined).sum(-1).sqrt().mean(-1) * 1e3
            joints3d_refined_prev = joints3d_refined.detach()
            print(error, joints2d.shape, pose2d.shape, joints_change_mm)
            ax = plt.subplot()  # plt.subplot(3, 1, 1)
            ax.imshow(pg.generated_poses[example, 0].t(), cmap='gray')
            for joint in range(num_joints):
                gt = pg.joints_2d_position[example, joint, :2]
                pred = joints2d_refined[joint].detach() * 368
                orig = joints2d[joint].detach() * 368
                ax.add_patch(Circle(gt, 0.3, color='g'))
                ax.add_patch(Circle(pred, 0.3, color='r'))
                ax.add_patch(Circle(orig, 0.3, color='y'))
                # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
            plt.show()
            if True:
                fig = plt.figure()
                ax = fig.add_subplot(111, projection='3d')

                ax.scatter(joints3d[..., 0], joints3d[..., 1], joints3d[..., 2], c='y', marker='x')
                ax.scatter(joints3d_refined[..., 0].detach(), joints3d_refined[..., 1].detach(), joints3d_refined[..., 2].detach(), c='r', marker='o')
                ax.scatter(target[..., 0], target[..., 1], target[..., 2], c='g', marker='o')
                ax.set_xlabel('X-axis')
                ax.set_ylabel('Y-axis')
                ax.set_zlabel('Z-axis')
                plt.axis('equal')
                plt.show()
            print('plot?')
            if joints_change_mm < epsilon:  # FIXME: <= ? To generalize to 0
                print("Convergence")
                # Terminate refinement when joints have converged.
            else:
                pass

    # Discard changes if they are not within a threshold.
    joints_change_mm = (joints2d - joints2d_refined).sum(-1).sqrt().mean(-1) * 1e3
    if joints_change_mm < tau:
        results.append((joints3d_refined, joints2d_refined))
        mask.append([1])
    else:
        results.append((joints3d, joints2d))
        mask.append([0])

if __name__ == "__main__":
    #train_projector_only()
    #train_regressor_only()
    #train_projector()
    test_projector()
    