import torch
import torch.nn.functional as F

import numpy as np
from matplotlib import pyplot as plt
from torch import nn

from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.minirgbd import MINIRGBDDataset
from model.SML import SML
from model.VideoPose3D.model import TemporalModel, TemporalModelOptimized1f
from model.cpm import CPM
from dataset.cachetransformer.cpmcache import CPMJoints2dCache, CPMCache
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from render.cameraparameters import CameraParameters
from render.renderer import Renderer
from utils import to_numpy, determinism, timing


class CPMRegressor(nn.Module):
    def __init__(self, num_joints, num_heatmaps, project_z, unproject_p, num_img_channels=1,
                 channels=128, linear_size=1024, bidirectional=True):
        # num_joints is the number of joints in the body model. The dimension of this modules output.
        # num_heatmaps is the dimension of the output from the CPM, generally equal to num_joints, unless
        #  background is also estimated. Then it is num_joints+1.
        # num_img_channels is the number of channels in the input image to the CPM. Here: 1, since only depth.
        super().__init__()
        self.project_z = project_z
        self.unproject_p = unproject_p
        self.traj_model = TemporalModelOptimized1f(num_joints_in=1, in_features=3, num_joints_out=1,
                                   filter_widths=[3, 3, 3], dropout=0.05)
        self.pose_model = TemporalModelOptimized1f(num_joints_in=num_joints-1, in_features=3, num_joints_out=num_joints-1,
                                   filter_widths=[3, 3, 3], dropout=0.05)

        self._num_joints = num_joints
        prefix = torch.zeros((num_joints,), dtype=torch.float)
        if num_joints == 25 or num_joints==24: # SMIL joints
            prefix[:24] = torch.as_tensor([0.0457, 0.0447, 0.0419, 0.0738, 0.0230, 0.0229, 0.0511, 0.0158, 0.0159,
                                      0.0431, 0.0119, 0.0101, 0.0438, 0.0387, 0.0402, 0.0410, 0.0268, 0.0287,
                                      0.0220, 0.0233, 0.0162, 0.0166, 0.0092, 0.0119])
            # We save these prefix values so we can use them in the space_pred.
            # This way, we get a more fair comparison of how much improvement this module gives.
        self.register_buffer('prefix_values', prefix.clone())
        self.register_buffer('ones', torch.ones((1,), dtype=torch.float))

    @timing
    def forward(self, input):
        heatmap, cpm_features, depth, joints2d_skin = input
        batch_size = cpm_features.shape[0]
        seq_len = cpm_features.shape[1]
        seqshape = lambda x: x.reshape(-1, seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])

        space_pred_bayd = self.unproject_p(torch.cat([joints2d_skin[..., :2],
                                                 self.project_z(
                                                     (joints2d_skin[..., 2]).unsqueeze(-1)),
                                                 self.ones.expand(*joints2d_skin.shape[:-1], 1)], -1))[..., :3]
        #depth = seqshape(F.interpolate(flatshape(depth), scale_factor=1/CPM.stride, mode='area'))
        #cpm_features = torch.cat([heatmap, cpm_features, depth], -3) # Concat channels
        #cpm_features = flatshape(cpm_features)
        #x = joints2d_features.reshape(seq_len*batch_size, *joints2d_features.shape[2:]) # To image

        # TODO Try just getting z from network, but giving it all point cloud three coords!
        #  Then use this corrected z to project them into space again.

        #####
        space_pred = self.unproject_p(torch.cat([joints2d_skin[..., :2],
                                                 self.project_z((joints2d_skin[..., 2] + self.prefix_values).unsqueeze(-1)),
                                                 self.ones.expand(*joints2d_skin.shape[:-1], 1)], -1))[..., :3]
        space_pred_bayd = space_pred_bayd.contiguous()
        #https://github.com/facebookresearch/VideoPose3D/blob/1551a01dc254e51d4edbdbed7c7ff2ee4d9d5e2b/run.py
        trajectory = self.traj_model(space_pred_bayd[..., :1, :])
        pose = self.pose_model(space_pred_bayd[..., 1:, :] - space_pred_bayd[..., :1, :])

        x = torch.cat([trajectory, pose], -2)
        #space_pred_bayd[..., 1:] = space_pred_bayd[..., :1]
        return x, space_pred

    @staticmethod
    def loss(output, target):
        # Sum over joint position values, mean over joints and time
        target = target.clone() # To not have any secondary effects.
        target[:, :, 1:] -= target[:, :, :1]
        target[...] = 0
        print(output[0].min().item(), output[0].max().item())
        # Sum over XYZ, mean over joints
        return ((output[0][:, 0] - target[:,13]) ** 2).sum(-1).mean(-1)

    def callback(self, pd_train, pd_test, project_p):
        from matplotlib.patches import Circle
        from mpl_toolkits.mplot3d import Axes3D # Needed for 3d plots
        ex = False
        def cb():
            nonlocal ex
            if ex:
                pd_example = pd_train
            else:
                pd_example = pd_test
            ex = not ex
            if ex is False:
                exit()
            example = 0
            input, target_ex = pd_example[example]  # Retreive sample.
            _, _, _, input_ex = input
            #skip_samples = 8 # input_ex.shape[0]//10
            #skip_samples = min(max(len(input_ex)-1, 1), skip_samples)
            result = pd_example._cache[example - pd_example._cache_ptr]  # Middle batch, last element in seq.
            #samples = list(range(len(result['depth']))[::skip_samples])
            samples = [13]
            self.eval()
            output_ex = self(map(lambda t: t.to(device, dtype).unsqueeze(0), input)) # FIXME: Get device proper.
            output_ex = tuple(map(lambda o: o[0], output_ex)) # Remove the batch dim

            # result = pd_example._cache[len(pd_example)//seq_len//2, seq_len-1]
            pd_train.shuffle()

            #output_ex[0][:, 1:] += output_ex[0][:, :1]  # Add trajectory
            for frame in samples:
                sample = result[frame]
                output_ex[0][:, 1:] += output_ex[0].new_tensor(sample['joints3d'][:1, :3])  # FIXME remove?

                normalizer = 1
                depth = sample['depth']
                if np.issubdtype(depth.dtype, np.integer):
                    normalizer = np.iinfo(depth.dtype).max
                depth = depth.astype(np.float32) / normalizer * pd_example._renderer.far
                ax = plt.subplot()
                img = ax.imshow(depth[0].T, cmap='gray')
                plt.colorbar(img).set_label('depth [m]')
                ax.set_xlabel('frame ' + str(frame))
                name, startframe = pd_example.get_sample_id(example)
                ax.set_title(name + ' starting frame ' + str(startframe))

                joints2d = to_numpy(sample['joints2d'][..., :2])
                #pred2d = to_numpy(input_ex[frame, ..., :2])
                pred2d = to_numpy(project_p(torch.cat([output_ex[1][frame], output_ex[1].new_ones(output_ex[1][frame].shape[:-1] + (1,))], -1))[..., :2])
                #refined2d = to_numpy(project_p(torch.cat([output_ex[0][frame], output_ex[0].new_ones(output_ex[0][frame].shape[:-1] + (1,))], -1))[..., :2])
                refined2d = to_numpy(project_p(torch.cat([output_ex[0][0], output_ex[0].new_ones(output_ex[0][0].shape[:-1] + (1,))], -1))[..., :2])
                for joint in range(len(joints2d)):
                    gt = joints2d[joint]
                    pred = pred2d[joint]
                    refined = refined2d[joint]
                    ax.add_patch(Circle(gt, 0.4, color='g'))
                    ax.add_patch(Circle(pred, 0.3, color='r'))
                    ax.add_patch(Circle(refined, 0.2, color='y'))
                    ax.text(*gt, str(joint), fontsize=6)
                    ax.text(*refined, str(joint), fontsize=4)
                plt.show()

                ax = plt.subplot(projection='3d')
                #plt.gca().invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                #ax.invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                ax.invert_yaxis()
                ax.invert_zaxis()

                ax.axis('equal')
                ax.set_xlabel('x (frame ' + str(frame) + ')')
                ax.set_ylabel('y')
                ax.set_zlabel('z')

                joints3d, pred3d, refined3d = map(to_numpy, (sample['joints3d'][..., :3],
                                                             output_ex[1][frame, ..., :3],
                                                              output_ex[0][0, ..., :3]))
                print('Frame', str(frame))
                print('CPM error           | refinement error')
                print(np.concatenate([
                    np.round(((joints3d - pred3d) * 1e3)),
                    np.round(((joints3d - refined3d) * 1e3))], -1))

                # The cpm error is dominated by errors in z.
                print('root mean (joints) sum square        cpm error:',
                      np.sqrt(((joints3d-pred3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) sum square refinement error:',
                      np.sqrt(((joints3d-refined3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) square z        cpm error:',
                      np.sqrt(((joints3d - pred3d) ** 2)[..., 2].mean(-1)))
                print('root mean (joints) square z refinement error:',
                      np.sqrt(((joints3d - refined3d) ** 2)[..., 2].mean(-1)))
                print(' ')

                for joint in range(len(joints3d)):
                    gt = joints3d[joint]
                    pred = pred3d[joint]
                    refined = refined3d[joint]
                    #ax.add_patch(Circle(gt, 0.3, color='g'))
                    #ax.add_patch(Circle(pred, 0.3, color='r'))
                    #ax.add_patch(Circle(refined, 0.3, color='y'))
                    ax.scatter(gt[..., 0], gt[..., 1], gt[..., 2], s=0.5, color='g')
                    ax.scatter(pred[..., 0], pred[..., 1], pred[..., 2], s=0.5, color='r')
                    ax.scatter(refined[..., 0], refined[..., 1], refined[..., 2], s=0.5, color='y')
                    ax.text(*gt, str(joint), fontsize=6)
                    # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                    #ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
                    #ax.add_patch(Arrow(*gt, refined[0] - gt[0], refined[1] - gt[1], color='w'))
                plt.show()

        return cb

    @staticmethod
    def post_transform(hmc, cpmc):
        capture = cpmc.post_transform_cpm_extension_keypoints(hmc, target2d=False)
        def fun(cache_subset):
            joints2d_skin, joints3d = capture(cache_subset)
            cpm_heatmap, cpm_features = map(cache_subset.__getitem__, CPMCache.name)
            depth = torch.tensor(cache_subset['depth'])
            input = (cpm_heatmap, cpm_features, depth, joints2d_skin)
            input = tuple(map(torch.as_tensor, input))
            # We could do the image resizing here, so it will be done on the CPU (Best practices)
            return input, joints3d[..., :3]
        return fun

def load_pretrained(regressor, path='./model/VideoPose3D/pretrained_humaneva15_detectron.bin'):
    pretrained = torch.load(path, map_location='cpu')
    pretrained = pretrained['model_pos']
    regressor_state = regresor.pose_model.state_dict()
    # Mapping from eva15 to our joints.
    mapping = {1: 1,
               2: 9,
               3: 12,
               4: 1,
               5: 10,
               6: 13,
               7: 2, # lower spine, also map to thorax?
               8: 11,
               9: 14,
               10: 2, # top spine, we map to thorax
               11: 11,
               12: 14,
               13: 2,
               14: 3,
               15: 6,
               16: 15,
               17: 3,
               18: 6,
               19: 4,
               20: 7,
               21: 5,
               22: 8,
               23: 5,
               24: 8,
               25: 15}
    mapping = {key-1:val-1 for key, val in mapping.items()} # zero indexed python

    with torch.no_grad():
        for key, value in regressor_state.items():
            if key in pretrained:
                pretrained_weight = pretrained[key]
                if pretrained_weight.shape != value.shape:
                    if pretrained_weight.shape[0] == 45:
                        value = value.reshape(-1, 3, *value.shape[1:])
                        pretrained_weight = pretrained_weight.reshape(15, 3, *pretrained_weight.shape[1:])
                        for sml_idx, eva_idx in mapping.items():
                            if sml_idx == 0:
                                # We do not track global joint (It seems like eva does?)
                                continue
                            value[sml_idx-1, ...] = pretrained_weight[eva_idx]
                    else:
                        value = value.reshape(1024, -1, 3, 3) # We have 3d input features
                        pretrained_weight = pretrained_weight.reshape(1024, -1, 2, 3) # They have 2d input features
                        for sml_idx, eva_idx in mapping.items():
                            if sml_idx == 0:
                                # We do not track global joint (It seems like eva does?)
                                continue
                            value[:, sml_idx-1, :2, ...] = pretrained_weight[:, eva_idx] # TODO Scale this maybe?
                        #print(pretrained_weight.max(), pretrained_weight.min())
                else:
                    value[...] = pretrained_weight
                if key.startswith('shrink'): # Final key. Since infants are smaller than adults, downscale
                    value[...] *= (0.0919+0.2187)/1.5
                    #print(key, pretrained_weight)
            else:
                print('key', key, 'not in the pretrained network')
    regressor.pose_model.load_state_dict(regressor_state)

def debug():
    from trainer import Trainer, Statistic
    dtype=torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # TODO Find where the CPM fails, and how often (Gives an error over 5 px or 10 mm)
    #  Then see if a temporal model/LSTM will be able to fix it. Only fall back to this model
    #   if the confidence is low or something. (Check confidence on bad predictions!)
    # 2D output correction could be a 3D heatmap.
    # First pre-fix z with the depth.
    #model = TemporalModelOptimized1f(num_joints_in=25, in_features=3, num_joints_out=25,
    #                                          filter_widths=[5], dropout=0, channels=512*2)

    class testmodel(nn.Module):
        def __init__(self):
            super().__init__()
            num_directions = 2
            #self.lstm = nn.LSTM(25 * 3, 1024, batch_first=True, dropout=0.25, bidirectional=num_directions==2)
            temporal_hidden_size = 512
            self.lstm = nn.GRU(25, temporal_hidden_size, batch_first=True,
                               bias=True, dropout=0, bidirectional=num_directions==2)
            self.linear = nn.Linear(temporal_hidden_size*num_directions, 25, bias=True)
            #self.linear1 = nn.Linear(25*3, 25*3)

            #nn.init.eye_(self.lstm.weight_ih_l0)
            #nn.init.eye_(self.lstm.weight_ih_l0_reverse)
            #nn.init.eye_(self.linear.weight)
            #nn.init.eye_(self.lstm.weight_hh_l0)

            #nn.init.eye_(self.lstm.weight_hh_l0_reverse)
            #nn.init.zeros_(self.lstm.weight_hh_l0)
            #nn.init.zeros_(self.lstm.weight_hh_l0_reverse)
            #nn.init.zeros_(self.lstm.weight_ih_l0)
            #nn.init.zeros_(self.lstm.weight_ih_l0_reverse)
            self.register_buffer('zero', torch.zeros((1,), dtype=torch.float))

        def forward(self, input):
            input_z = input[..., 2]
            #input = self.linear1(input)
            remember, _ = self.lstm(input_z)
            remember = remember[:, self.receptive_field()//2]

            addition = self.linear(remember)[..., None]
            #if not self.training:
            #    addition = torch.where(addition > 0.01, addition, self.zero)
            output = torch.cat([input[:, self.receptive_field()//2, :, :2],
                                input[:, self.receptive_field()//2, :, 2, None]+addition], -1)
            #if not self.training:
            #    if (input[:, self.receptive_field()//2, :, 2]-output):
            #        pass

            #output = self.linear1(input)[:, self.receptive_field()//2]
            #output = output.reshape(*output.shape[:-1], 25, 3, -1).mean(-1).unsqueeze(1)
            #output = output + input[:, self.receptive_field()//2]
            # F.dropout(x[:, model.receptive_field() // 2], 0.1, True, True)
            output = output.reshape(-1, 1, 25, 3)
            return output

        def receptive_field(self):
            return 27

    model = testmodel()

    x = torch.randn((1024, model.receptive_field(), 25, 3))

    x, model = map(lambda t: t.to(device, dtype), (x, model))

    if False:
        camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
        dtype = torch.float

        # Load model.
        nose = True
        model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

        seq_len = 1000  # +8*16 # 32
        downsample_factor = 1  # To subsample the dataset - more temporal space between inputs.
        skip_factor = 1  # To reduce the size of the dataset. If we skip, there will be less overlap between sequences.

        renderer = Renderer(model, camera_params, visible=True, decoration=False)
        sigma = 7
        background_heatmap = False
        hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma, stride=CPM.stride,
                              background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)

        cpm = CPM(num_heatmaps=model.num_joints + hmc._background_heatmap)
        # FIXME: Uncomment next line
        cpm.load_state_dict(torch.load('./trainednetworks/CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-BEST.pt')['model_state_dict'])
        cpm.to(device, dtype, non_blocking=True)
        cpmc = CPMCache(cpm, renderer.far, batch_size=40)
        cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

        train_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=skip_factor,
                                    startseq=0, endseq=1, random_betas=False, random_rotation=True)

        pd_train = CachedSMLDataset(renderer, model, [train_set],
                                    cache_size=1000*3,
                                    augment_position=True, transforms=cache_transformers,
                                    post_transform=CPMRegressor.post_transform(hmc, cpmc))

        examples = list(pd_train)
        torch.save(examples, 'examples.pt')
    if False:
        examples = torch.load('examples.pt')
        input_ex, target_ex = examples[0]

        regresor = CPMRegressor(model.num_joints, cpm._num_heatmaps, renderer.project_z, renderer.unproject_points)
        load_pretrained(regresor)
        # regresor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
        regresor = regresor.to(device, dtype)

    optimizer = torch.optim.Adam(model.parameters(), lr=0.001)
    #optimizer = torch.optim.SGD(model.parameters(), lr=0.1)
    #optimizer = torch.optim.RMSprop(model.parameters(), lr=0.01, centered=True)
    optim_state = optimizer.state_dict()
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.8, patience=3,
                                                           cooldown=5, threshold=0.01, min_lr=1e-6, verbose=True)
    schedul_state = scheduler.state_dict()
    train_losses = Statistic()
    test_losses = Statistic()
    for idx, epoch in enumerate(range(200)):
        x[...] = 0
        target = x[:, model.receptive_field()//2]
        target.uniform_(-20, 20)
        x[:, :model.receptive_field()//2, ...] = x[:, model.receptive_field()//2, None]
        x[:, model.receptive_field()//2:, ...] = x[:, model.receptive_field()//2, None]

        #xin = x.clone()
        target = target.clone()
        F.dropout(x[:, model.receptive_field() // 2, :, 2], 0.1, True, True)
        loss_fn = lambda predicted: ((predicted - target) ** 2).sum(-1).mean(-1) #torch.mean(torch.norm(predicted - target, dim=-1), -1) * 1e3
        model.train()
        #print('target', target[0])
        y = model(x)[:, 0]
        #print('y', y[0])
        #print(target.shape, y.shape, x.shape)
        # x[:, model.receptive_field()//2]
        #batch_loss = ((y[:, 0] - x[:, -1]) ** 2).sum(-1).mean(-1) # Sum over XYZ, mean over joints
        batch_loss = loss_fn(y)
        train_losses.add(batch_loss)
        loss = batch_loss.mean(0)
        optimizer.zero_grad()
        loss.backward()

        with torch.no_grad():
            model.eval()
            #F.dropout(x[:, model.receptive_field() // 2], 1, True, True)
            y = model(x)[:, 0]
            # batch_loss = ((y[:, 0] - x[:, model.receptive_field() // 2]) ** 2).sum(-1).mean(-1) # Sum over XYZ, mean over joints
            batch_loss = loss_fn(y)
            test_loss = batch_loss.mean(0).item()
            test_losses.add(batch_loss)
            print(idx, test_loss)

        optimizer.step()
        scheduler.step(test_loss)

        if test_loss < 300 and optim_state is not None:
            #x = torch.randn((1024*3, model.receptive_field(), 25, 3)).to(device)
            print('wap', test_loss)
            # optim_state['param_groups'][0]['lr'] = optim_state['param_groups'][0]['lr']*100
            #optimizer.load_state_dict(optim_state)
            #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.6, patience=7, threshold=0.01,
            #                                                       verbose=True)
            optim_state = None
    #print(torch.round(torch.cat([target[0], y[0], torch.abs(target[0]-y[0])], -1)*1e3)*1e-3)
    print(torch.round(torch.cat([target[0], y[0]], -1)*1e4)*1e-4)
    print(torch.abs(target[0]-y[0]).max(-1)[0])
    print('min loss:', min(test_losses.means))

    Trainer.plot_training(train_losses, test_losses, offset=30)
    exit()



if __name__ == "__main__":
    #Cache()['test']
    torch.manual_seed(7)
    #determinism(speed=True)
    debug()
    camera_params = CameraParameters(640//2, 480//2, preset='MIKKEL')

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # Load model.
    nose = True
    model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

    seq_len = 27# +8*16 # 32
    downsample_factor = 1 # To subsample the dataset - more temporal space between inputs.
    skip_factor = 1 # To reduce the size of the dataset. If we skip, there will be less overlap between sequences.

    renderer = Renderer(model, camera_params, visible=False, decoration=False)
    sigma=7
    background_heatmap = False
    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma, stride=CPM.stride, background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)

    cpm = CPM(num_heatmaps=model.num_joints + hmc._background_heatmap)
    # FIXME: Uncomment next line
    cpm.load_state_dict(torch.load('./trainednetworks/CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-BEST.pt')['model_state_dict'])
    cpm.to(device, dtype, non_blocking=True)
    cpmc = CPMCache(cpm, renderer.far, batch_size=40)
    cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

    train_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=skip_factor,
                                startseq=0, endseq=1, random_betas=False, random_rotation=True)

    pd_train = CachedSMLDataset(renderer, model, [train_set],
                             cache_size=1000,
                             augment_position=True, transforms=cache_transformers,
                             post_transform=CPMRegressor.post_transform(hmc, cpmc))

    test_skip_factor = skip_factor//2 if skip_factor!=1 else skip_factor
    test_skip_factor = 8
    test_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=test_skip_factor,
                               startseq=10, endseq=11, random_betas=False, random_rotation=False)
    pd_test = CachedSMLDataset(renderer, model, [test_set],
                            cache_size=1296,
                            augment_position=False, transforms=cache_transformers,
                            post_transform=CPMRegressor.post_transform(hmc, cpmc))
    print('train set:', train_set._sequence_names, 'length (in sequences):', len(pd_train))
    print('test set', test_set._sequence_names, 'length (in sequences):', len(pd_test))
    from trainer import Trainer, Statistic

    #loss = torch.nn.SmoothL1Loss(reduction='none')
    # TODO: Allow for much larger cache. (Whole dataset if possible) For less switching.
    trainer = Trainer(pd_train, pd_test,
                      loss_fn=CPMRegressor.loss
                      )
    if False:
        pd_train[0]
        pd_train[900]
        pd_train[900 * 2]
        pd_train[900 * 2]
        pd_train[900 * 3]
        pd_train[900 * 4]

    regresor = CPMRegressor(model.num_joints, cpm._num_heatmaps, renderer.project_z, renderer.unproject_points)
    load_pretrained(regresor)
    #regresor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
    regresor = regresor.to(device, dtype)

    callback = regresor.callback(pd_train, pd_test, renderer.project_points) #cpm.callback(pd_train, pd_test, hmc, device, dtype)

    callback()
    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf

    batch_size = 16 #8*2*8*2*4
    optimizer = torch.optim.Adam(regresor.parameters(), lr=1e-3, amsgrad=True) # TODO: Try amsgrad=True
    #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.6, patience=1, threshold=0.001,
    #                                                       verbose=True)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1, gamma=0.98)
    train_result = trainer.resume_training(regresor, optimizer, scheduler, callback=callback, batch_size=batch_size,
    load_optim_state_dict=True, max_epochs=150, patience=20, max_batch_size=batch_size)
    Trainer.plot_training(*train_result[:2], offset=5)
    exit()

    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = torch.optim.Adam(regresor.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(regresor, batch_size,
                                     optimizer, scheduler,
                                     max_epochs=50, patience=10,
                                     callback=callback,
                                     max_batch_size=batch_size)
        Trainer.plot_training(*train_result[:2], offset=5)