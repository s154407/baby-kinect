from torch import nn
import torch.nn.functional as F
from collections import OrderedDict
from torch.utils.data.dataset import Dataset
import matplotlib.pyplot as plt
from utils import *
import os


class Pose2Dto3D(nn.Module):
    # T - Transformer
    def __init__(self, num_joints, channels=128, linear_size=1024):
        super().__init__()
        self.special_conv_1 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=3,stride=1,padding=1)
        self.special_conv_2 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=3,stride=1,padding=1)
        self.fa_conv_1 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=5,stride=2,padding=1)
        self.fa_conv_2 = nn.Conv2d(in_channels=channels,out_channels=channels,kernel_size=5,stride=2,padding=1)
        #self.maxpool = nn.MaxPool2d(kernel_size=2,stride=2)
        # Calculate 5 * 5 from some input resolution or something.
        self.fc = nn.Linear(in_features=channels * 10 * 10, out_features=linear_size)
        self.lstm = nn.LSTM(input_size=linear_size, # dropout=0.1,
                            hidden_size=linear_size, bidirectional=True,
                            batch_first=True)
        self._lstm_state = None
        self.fc_final = nn.Linear(in_features=linear_size, out_features= 3 * num_joints)
        self._num_joints = num_joints

    def forward(self, joints2d_features, seq_len):
        # Remember to give the input as (seq, batch, ...)
        batch_size = joints2d_features.shape[0] // seq_len
        #x = joints2d_features.reshape(seq_len*batch_size, *joints2d_features.shape[2:]) # To image
        x = F.relu(self.special_conv_1(joints2d_features))
        x = F.relu(self.special_conv_2(x))
        x = F.relu(self.fa_conv_1(x))
        x = F.relu(self.fa_conv_2(x))
        #x = self.maxpool(x)
        x = x.reshape(batch_size * seq_len, -1) # To vector.
        x = F.relu(self.fc(x))
        x = x.reshape(batch_size, seq_len, -1)
        # Slowly iterate over each batch, clearing the hidden state between each:
        #results = []
        #for batch in x:
        #    batch = batch.unsqueeze(0)
        #    lstm_out, _ = self.lstm(batch)
        #    results.append(lstm_out)
        #x = torch.cat(results)
        #self.init_hidden(batch_size * seq_len, self.training)
        x, self._lstm_state = self.lstm(x, self._lstm_state) # Maybe register lstm state as buffer?
        #self._lstm_state = self._lstm_state[0].detach().zero_()
        #self._lstm_state = (self._lstm_state, self._lstm_state)
        #self._lstm_state[0].zero_().detach_()
        #print([state.shape for state in self._lstm_state], 'lstm')
        self._lstm_state = None
        x = x.reshape(batch_size * seq_len, -1) # To vector
        x = self.fc_final(x)
        #x = F.relu(self.fc_final(x))
        #x = self.fc_final2(x)
        x = x.reshape(-1, self._num_joints, 3)
        return x


def load_pretrained_pose_transformer(num_joints=24):
    #cpm = CPM(num_joints, channels)
    transformer = Pose2Dto3D(num_joints)
    caffemodel: OrderedDict = torch.load('./caffemodel2pytorch/finetune_whole/finetune_whole.caffemodel.pth')
    #caffemodel = {k: v for k, v in caffemodel.items()}
    with torch.no_grad():
        transformer.fc.weight[...] = caffemodel['fc.weight']
        transformer.fc.bias[...] = caffemodel['fc.bias']
        lstmparam = dict(transformer.lstm.named_parameters())
        transformer.lstm.weight_hh_l0[...] = caffemodel['lstm1.weight']
        transformer.lstm.bias_hh_l0[...] = caffemodel['lstm1.bias']
        transformer.lstm.weight_hh_l1[...] = caffemodel['lstm2.weight']
        transformer.lstm.bias_hh_l1[...] = caffemodel['lstm2.bias']
        # print(caffemodel['lstm1.weight'], caffemodel['lstm2.bias'])
        transformer.fc_final.weight *= 40 / 10 # Just to mach the scale of caffemodel['fc-final_16.weight'] somewhat.
        #transformer.fc_final.bias -= transformer.fc_final.bias.min() # Bias is generally positive bc. output must be.
        transformer.fc_final.bias *= 10
        # Warning! We need to initialize the hh layers in the LSTM too? (Do it later..)
    return transformer

class CPM_transformer(nn.Module):
    def __init__(self, cpm, transfomer, seq_len=1, cat_input=False):
        super().__init__()
        self._cpm = cpm
        self._transformer = transfomer
        self._seq_len = seq_len
        self._cat_input = cat_input

        for param in self._cpm.parameters():
            param.requires_grad = False # Freeze cpm layer.

    def forward(self, input):
        input = input.reshape(-1, *input.shape[-3:]) # Flatten sequences
        pose2d, features = self._cpm(input)
        if self._cat_input:
            miniinput = F.interpolate(input, scale_factor=1/8, mode='nearest')
            features = torch.cat([features, miniinput], 1)
        pose3d = self._transformer(features, self._seq_len)
        return pose3d, pose2d

class MiniTransformer(nn.Module):
    def __init__(self, transfomer, seq_len=1):
        super().__init__()
        self._transformer = transfomer
        self._seq_len = seq_len

    def forward(self, features):
        #features = input.reshape(-1, *input.shape[-3:]) # Flatten sequences
        #if self._cat_input:
        #    miniinput = F.interpolate(input, scale_factor=1/8, mode='nearest')
        #    features = torch.cat([features, miniinput], 1)
        pose3d = self._transformer(features.reshape(-1, *features.shape[-3:]), self._seq_len)
        return pose3d

def train_mini_transformer(device = torch.device('cuda')):
    num_joints = 24
    batch_size = 64

    # Load all the "good" samples - the samples the CPM has high confidence in.
    good = torch.load('50conf.pt')
    good = to_numpy(good)
    goodseqindices = np.zeros(good.shape, dtype=np.int64)
    goodindices = np.zeros(good.shape)
    goodcnt = 0
    cnt = 0
    for idx, i in enumerate(good):
        if i == 1:
            cnt += 1
            if cnt == 5:
                goodseqindices[goodcnt] = idx+1
                goodcnt += 1
                cnt = 0
                goodindices[(idx-4):(idx+1)] = 1
        else:
            cnt = 0
    goodindices = np.nonzero(goodindices)[0]
    #goodindices = goodindices == True
    goodseqindices = goodseqindices[:goodcnt]
    #from cpm import CPM
    #cpm = CPM(num_joints)
    #cpm.load_state_dict(torch.load(os.path.join('trainednetworks', 'cpm_final_1.pt')))
    #transformer = load_pretrained_pose_transformer(num_joints)
    transformer = Pose2Dto3D(num_joints, channels=128, linear_size=1024) # 128 + 1 for input given.

    cpm_transformer = MiniTransformer(transformer, seq_len=5).to(device=device)
    from trainer import Trainer
    import torch.optim as optim
    #pg = PoseGenerator()

    # dataset = PrePreprocess(MiniRGBDDataset, './preloaddataset.pkl', pg.generated_poses.permute(0, 2, 3, 1),
    #                        pg.joints_2d_position)

    # TODO add some kind of transform to the tensordataset
    import h5py
    cpmfeatures = h5py.File(os.path.join('creation', 'cpmoutputs', 'cpmfeatures.h5'))
    cpmfeatures = cpmfeatures.get('data')

    #images = torch.index_select(pg.generated_poses.float(), 0, torch.as_tensor(goodindices)) / 255
    print(goodindices.shape, goodseqindices.shape, cpmfeatures.shape)
    #features = torch.as_tensor(cpmfeatures[list(goodindices)])#torch.index_select(cpmfeatures, 0, torch.as_tensor(goodindices))
    #inputs = torch.cat([features, images], -1)

    joints3d = torch.load(os.path.join('creation', 'joints', 'joints3d.pt'))
    #targets = torch.index_select(joints3d, 0, torch.as_tensor(goodindices))
    targets = torch.as_tensor(joints3d[..., :3], dtype=torch.float)
    #targets = targets.reshape(-1, 5, *targets.shape[1:])

    class FeatureDataset(Dataset):
        def __getitem__(self, item):
            #print(item, targets.shape, cpmfeatures.shape, goodseqindices.shape)
            goodseqindex = goodseqindices[item]
            #print(goodseqindex)
            input = torch.as_tensor(cpmfeatures[(goodseqindex-5):goodseqindex])
            target = targets[(goodseqindex-5):goodseqindex]
            #print(input.shape, target.shape, goodseqindices.shape)
            return input, target

        def __len__(self):
            return len(goodseqindices)
    #inputs = inputs.reshape((-1, 5) + inputs.shape[1:]) # Turn to sequences of length 5.

    #train_set = torch.utils.data.TensorDataset(
    #    inputs, # TODO: Reshape to add the sequences in a dim, then permute or something.
    #    targets) # 192
    train_set = FeatureDataset()
    split_point = int(len(train_set) * 11 / 12)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    #val_set = train_set
    #val_set = train_set
    # loss = nn.MSELoss(reduction='none')
    #weights = torch.as_tensor([1/27,1/27,25/27],device=device)
    weights = torch.ones(3, dtype=torch.float, device=device)
    weights2 = torch.ones(24, dtype=torch.float, device=device)
    #weights2[11] = 100
    weights2 /= weights2.sum()
    weights2 = weights2.reshape(1, num_joints, 1)
    trainer = Trainer(train_set, val_set,
                      loss_fn=lambda output, target: (weights * ((output - target[..., :3].reshape(-1, num_joints, 3))*weights2) ** 2).sum(-1).mean(-1)) # Sum over XYZ, mean over joints)

    example_nr = 50 #val_set.indices[20]
    seq_nr = 4
    #example = (torch.as_tensor(pg.generated_poses[example_nr, None], dtype=torch.float) / 255).to(device)
    images = torch.load(os.path.join('creation', 'renders', 'images.pt'))
    joints2d = torch.load(os.path.join('creation', 'renders', 'joints2d.pt'))
    def print_result():
        nonlocal images, joints2d
        features, target = train_set[example_nr]
        cpm_transformer.eval()
        output = to_numpy(cpm_transformer(features[None, ...].to(device=device)).detach())
        output = to_numpy(output[seq_nr])
        #input = to_numpy(input[seq_nr, 0])
        target = to_numpy(target[seq_nr, ..., :3])

        print(np.round(to_numpy(target*1e3)))
        print(np.round(output*1e3))
        print(np.round((output - target)*1e3))
        index = train_set.indices[goodseqindices[example_nr]]
        images = images[(index-5):index][seq_nr, 0].t()
        joints2d = joints2d[(index-5):index][seq_nr, ..., :4]
        imgpos = np.round(joints2d) # Approx
        imgz = images.t()[list(imgpos[:, 0]), list(imgpos[:, 1])].float()/255 * 1.5
        ax = plt.subplot()
        ax.imshow(images)
        from matplotlib.patches import Circle
        for joint in range(num_joints):
            gt = joints2d[joint, :2]
            ax.add_patch(Circle(gt, 0.3, color='g'))
        plt.show()
        print(images.shape, joints2d.shape, imgpos.shape, imgz.shape, joints2d[..., 2]/joints2d[..., 3])
        print(' test')
        print(imgz)
        print(target[..., 2])
        print(to_numpy(imgz)-to_numpy(target[..., 2]))
        exit()
        plot_pose_transformer(output, target)

        #plot_pose_transformer(output[seq_nr], to_numpy(target[seq_nr, ..., :3]))

    print_result()
    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = optim.Adam(cpm_transformer._transformer.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(cpm_transformer, batch_size, optimizer, scheduler, max_epochs=100, patience=10,
                                     callback=print_result)
        Trainer.plot_training(*train_result[:2], offset=5)
    pass


def train_pose_transformer(device = torch.device('cuda')):
    num_joints = 24
    batch_size = 8

    # Load all the "good" samples - the samples the CPM has high confidence in.
    good = torch.load('50conf.pt')
    good = to_numpy(good)
    goodindices = np.zeros(good.shape)
    goodcnt = 0
    cnt = 0
    for idx, i in enumerate(good):
        if i == 1:
            cnt += 1
            if cnt == 5:
                goodcnt += 1
                cnt = 0
                goodindices[(idx-4):(idx+1)] = 1
        else:
            cnt = 0
    goodindices = np.nonzero(goodindices)[0]

    from model.cpm import CPM
    cpm = CPM(num_joints)
    cpm.load_state_dict(torch.load(os.path.join('trainednetworks', 'cpm_final_1.pt')))
    #transformer = load_pretrained_pose_transformer(num_joints)
    transformer = Pose2Dto3D(num_joints, channels=128+1)

    cpm_transformer = CPM_transformer(cpm, transformer, cat_input=True).to(device=device)
    from trainer import Trainer
    import torch.optim as optim
    pg = PoseGenerator()

    # dataset = PrePreprocess(MiniRGBDDataset, './preloaddataset.pkl', pg.generated_poses.permute(0, 2, 3, 1),
    #                        pg.joints_2d_position)

    # TODO add some kind of transform to the tensordataset
    inputs = torch.index_select(pg.generated_poses, 0, torch.as_tensor(goodindices))
    inputs = torch.as_tensor(inputs, dtype=torch.float) / 255
    inputs = inputs.reshape((-1, 5) + inputs.shape[1:])

    targets = torch.index_select(pg.joints_3d_position, 0, torch.as_tensor(goodindices))
    targets = torch.as_tensor(targets, dtype=torch.float)
    targets = targets.reshape(-1, 5, *targets.shape[1:])

    train_set = torch.utils.data.TensorDataset(
        inputs, # TODO: Reshape to add the sequences in a dim, then permute or something.
        targets) # 192

    split_point = int(len(train_set) * 11 / 12)
    train_set, val_set = torch.utils.data.random_split(train_set, (split_point, len(train_set) - split_point))
    #val_set = train_set
    # loss = nn.MSELoss(reduction='none')
    weights = torch.as_tensor([1/27,1/27,25/27],device=device)
    trainer = Trainer(train_set, val_set,
                      loss_fn=lambda output, target: (weights * (output[0] - target[..., :3].reshape(-1, num_joints, 3)) ** 2).sum(-1).mean(-1)) # Sum over XYZ, mean over joints)

    example_nr = val_set.indices[20]
    seq_nr = 4
    #example = (torch.as_tensor(pg.generated_poses[example_nr, None], dtype=torch.float) / 255).to(device)
    def print_result():
        input, target = train_set[example_nr]
        cpm_transformer.eval()
        output = to_numpy(cpm_transformer(input[None, ...].to(device=device))[0].detach())
        output = to_numpy(output[seq_nr])
        input = to_numpy(input[seq_nr, 0])
        target = to_numpy(target[seq_nr, ..., :3])

        print(np.round(to_numpy(target*1e3)))
        print(np.round(output*1e3))
        print(np.round((output - target)*1e3))
        plot_pose_transformer(output, target)
        #plot_pose_transformer(output[seq_nr], to_numpy(target[seq_nr, ..., :3]))

    print_result()
    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = optim.Adam(cpm_transformer._transformer.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(cpm_transformer, batch_size, optimizer, scheduler, max_epochs=15, patience=10,
                                     callback=print_result)
        Trainer.plot_training(*train_result, offset=5)
    pass

def plot_pose_transformer(output, target):
    # This import registers the 3D projection, but is otherwise unused.

    #num_joints = 24
    #from cpm import CPM
    #cpm = CPM(num_joints)
    #cpm.load_state_dict(torch.load(os.path.join('trainednetworks', 'cpm_final_1.pt')))
    #transformer = load_pretrained_pose_transformer(num_joints)

    #cpm_transformer = CPM_transformer(cpm, transformer)
    #cpm_transformer.load_state_dict(torch.load(os.path.join('trainednetworks', 'posetransformerbest.pt')))

    #from dataset import PoseGenerator, MiniRGBDDataset, PrePreprocess
    #pg = PoseGenerator()

    #example = 4000
    #example_in = (torch.as_tensor(pg.generated_poses[example], dtype=torch.float) / 255)[None, ...]
    #example_target = to_numpy(torch.as_tensor(pg.joints_3d_position[example], dtype=torch.float)[..., :3])
    #example_in = example_in.to(next(cpm_transformer.parameters()).device)

    #example_out = to_numpy(cpm_transformer(example_in)[0])
    #print(example_in.shape, example_target.shape, example_out.shape)
    #print((example_out-example_target)*100)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(output[..., 0], output[..., 1], output[..., 2], c='r', marker='x')
    ax.scatter(target[..., 0], target[..., 1], target[..., 2], c='g', marker='o')
    ax.set_xlabel('X-axis')
    ax.set_ylabel('Y-axis')
    ax.set_zlabel('Z-axis')
    plt.axis('equal')

    plt.show()

def transformer_statistics():
    num_joints = 24
    pg = PoseGenerator()

    errors = to_numpy(pg.joints_3d_i_position - pg.joints_3d_position[..., :3])
    #combinederrors = errors.reshape(-1, 3)

    for j in range(num_joints):
        plt.title(str(j))
        for i in range(3):
            ax = plt.subplot(1, 3, i + 1)
            # the histogram of the data
            n, bins, patches = ax.hist(errors[:, j, i]*1e3, 30, density=1)
            # fig.tight_layout()
            plt.xlim(-100, 100)

        plt.show()



if __name__ == "__main__":
    from dataset import PoseGenerator

    #pg = PoseGenerator()
    train_mini_transformer()
    #train_pose_transformer()
    #transformer_statistics()
    exit()
    num_joints = 24
    from model.cpm import CPM

    cpm = CPM(num_joints)
    # cpm.load_state_dict(torch.load(os.path.join('trainednetworks', 'cpm_final_1.pt')))
    transformer = Pose2Dto3D(num_joints, channels=128 + 1)
    #cpm_transformer = CPM_transformer(cpm, transformer, cat_input=True)
    cpm_transformer = MiniTransformer(transformer, 5)
    cpm_transformer.load_state_dict(
        torch.load(os.path.join('bestmodel.pt'))['model_state_dict'])
    # torch.load(os.path.join('trainednetworks', 'test.pt'))['model_state_dict'])


    pg = PoseGenerator()

    example = 2500
    example = 9000

    plt.imshow(pg.generated_poses[example, 0].t(), cmap='gray')
    plt.show()

    example_in = (torch.as_tensor(pg.generated_poses[example], dtype=torch.float) / 255)[None, ...]
    example_target = to_numpy(torch.as_tensor(pg.joints_3d_position[example], dtype=torch.float)[..., :3])
    example_in = example_in.to(next(cpm_transformer.parameters()).device)

    example_out = to_numpy(cpm_transformer(example_in)[0])
    plot_pose_transformer(example_out, example_target)
    pass