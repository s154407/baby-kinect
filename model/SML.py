import torch
import pickle
import numpy as np
from torch import nn
from scipy.sparse import issparse

from utils import to_numpy


def inv_rodrigues(R):
    # # https://github.com/Lotayou/SMPL/blob/master/linear_regression.py
    axis = torch.stack((
        R[:, 2, 1] - R[:, 1, 2],
        R[:, 0, 2] - R[:, 2, 0],
        R[:, 1, 0] - R[:, 0, 1],
    ), dim=1
    )

    # http://ethaneade.com/lie.pdf: What to do when theta is small.
    # Angle, beware if R is close to I
    # (theta close to 2*K*pi, imprecise arccos)
    eps = 1e-6
    axis_norm = torch.norm(axis, dim=1)
    eps_norm = eps * torch.ones_like(axis_norm)
    axis_norm = torch.where(axis_norm < eps_norm, eps_norm, axis_norm)

    trace = R[:, 0, 0] + R[:, 1, 1] + R[:, 2, 2]
    angle = torch.atan2(axis_norm, trace - 1)

    # Angle is not unique, consider fix it into [0, 2pi]

    # Normalise the axis.
    axis /= axis_norm.unsqueeze(dim=1)

    # Return the data in compressed format [ax,ay,az]
    return axis * angle.unsqueeze(dim=1)

class SML(nn.Module):
    def with_zeros(self, x):
        """
        Append a [0, 0, 0, 1] vector to a batch of [3, 4] matrices.

        Parameter:
        ---------
        x: Tensor to be appended of shape [N, 3, 4]

        Return:
        ------
        Tensor after appending of shape [N, 4, 4]

        """
        ret = torch.cat([x, self.e4.expand(x.shape[0], 1, -1)], dim=1)
        return ret

    def pack(self, x):
        """
        Append zero tensors of shape [4, 3] to a batch of [4, 1] shape tensors.

        Parameter:
        ----------
        x: A tensor of shape [batch_size, 4, 1]

        Return:
        ------
        A tensor of shape [batch_size, 4, 4] after appending.

        """
        ret = torch.cat(
            (torch.zeros((x.shape[0], x.shape[1], 4, 3), dtype=x.dtype, device=x.device), x),
            dim=3
        )
        return ret

    @staticmethod
    def rodrigues(r, eye):
        """
        Rodrigues' rotation formula that turns axis-angle tensor into rotation
        matrix in a batch-ed manner.

        Parameter:
        ----------
        r: Axis-angle rotation tensor of shape [N, 1, 3].

        Return:
        -------
        Rotation matrix of shape [N, 3, 3].
        """
        theta = torch.norm(r, dim=(1, 2), keepdim=True)
        # avoid division by zero
        torch.max(theta, theta.new_full((1,), torch.finfo(theta.dtype).tiny), out=theta)
        #The .tiny has to be uploaded to GPU, but self.regress_joints is such a big bottleneck it is not felt.
        #FIXME Prevent division by zero by just detecting it instead of adding a tiny number...
        # see: https://github.com/blzq/tf_rodrigues/blob/master/rodrigues.py
        # Also: https://en.wikipedia.org/wiki/Axis%E2%80%93angle_representation#Log_map_from_SO(3)_to_'%22%60UNIQ--postMath-0000000D-QINU%60%22'(3)

        # "Practical implementation of the Rodrigues formula should use the Taylor
        # expansions of the coecients of the second and third terms when θ is small."
        # http://ethaneade.com/lie.pdf

        r_hat = r / theta
        z_stick = torch.zeros_like(r_hat[:, 0, 0])
        m = torch.stack(
            (z_stick, -r_hat[:, 0, 2], r_hat[:, 0, 1],
             r_hat[:, 0, 2], z_stick, -r_hat[:, 0, 0],
             -r_hat[:, 0, 1], r_hat[:, 0, 0], z_stick), dim=1)
        m = m.reshape(-1, 3, 3)

        dot = torch.bmm(r_hat.transpose(1, 2), r_hat)  # Batched outer product.
        # torch.matmul or torch.stack([torch.ger(r, r) for r in r_hat.squeeze(1)] works too.
        cos = theta.cos()
        R = cos * eye + (1 - cos) * dot + theta.sin() * m
        return R

    def __init__(self, model_path='./model.pkl', sparse=True, nose_vertex=False):
        super().__init__()

        self.parent = None # Set in self.forward based on kintree_table.
        self.model_path = model_path
        with open(model_path, 'rb') as f:
            params = pickle.load(f)

            def add_param(name):
                value = params[name]
                if value.dtype == np.uint32: # uint tensor not supported.
                    value = value.astype(np.int32)
                self.register_buffer(name, torch.as_tensor(value))

            for param in ['weights', 'posedirs', 'v_template',
                          'shapedirs', 'f', 'kintree_table']:
                add_param(param)

            self.update_parent()

            # J_regressor is a sparse tensor. This is (experimentally) supported in PyTorch.
            J_regressor = params['J_regressor']
            if issparse(J_regressor):
                # If tensor is sparse (Which it is with SMPL/SMIL)
                J_regressor = J_regressor.tocoo()
                J_regressor = torch.sparse_coo_tensor([J_regressor.row, J_regressor.col],
                                                      J_regressor.data,
                                                      J_regressor.shape)
                J_regressor = J_regressor.coalesce() # Does this make a difference?
                if not sparse:
                    J_regressor = J_regressor.to_dense()
            else:
                J_regressor = torch.as_tensor(J_regressor)
            self.register_buffer('J_regressor', J_regressor)
            self.num_joints = self.J_regressor.shape[0] + nose_vertex
            self.nose_vertex = nose_vertex
            self.num_vertices = self.J_regressor.shape[1]

            # These are maybe not strictly part of the model state, but it is easier this way.
            self.register_buffer('e4', self.posedirs.new_tensor([0, 0, 0, 1]))  # Cache this. (Saves a lot of time)
            self.register_buffer('eye', torch.eye(3, dtype=self.e4.dtype, device=self.e4.device))  # And this.

    def update_parent(self):
        kintree_table = self.kintree_table
        # (Not tested):
        #if kintree_table.is_cuda:
            # Create CPU copy, since it will be faster to iterate over.
        #    kintree_table = kintree_table.cpu()

        # Make kinematic tree relations.
        id_to_col = {kintree_table[1, i].item(): i for i in range(kintree_table.shape[1])}
        self.parent = {
            i: id_to_col[kintree_table[0, i].item()]
            for i in range(1, kintree_table.shape[1])
        }
        return self.parent

    def save_obj(self, verts, obj_mesh_name):
        with open(obj_mesh_name, 'w') as fp:
            for v in verts:
                fp.write('v %f %f %f\n' % (v[0], v[1], v[2]))

            for f in self.f:  # Faces are 1-based, not 0-based in obj files
                fp.write('f %d %d %d\n' % (f[0] + 1, f[1] + 1, f[2] + 1))

    def regress_joints(self, vertices):
        """The J_regressor matrix transforms vertices to joints."""
        # Given the template + pose blend shapes.
        batch_size = vertices.shape[0]

        # We could get the result as torch.matmul(self.J_regressor, vertices) or
        #  torch.stack([self.J_regressor.mm(verts) for verts in vertices]) in case J_regressor is sparse.
        # But turns out there is a solution faster than both of the above:
        batch_vertices = vertices.transpose(0, 1).reshape(self.J_regressor.shape[1], -1)
        batch_results = self.J_regressor.mm(batch_vertices)
        batch_results = batch_results.reshape(self.J_regressor.shape[0], batch_size, -1).transpose(0, 1)
        return batch_results

    def rotate_translate(self, rotation_matrix, translation):
        transform = torch.cat((rotation_matrix, translation.unsqueeze(2)), 2)
        return self.with_zeros(transform)

    def forward(self, beta, pose, trans=None, simplify=False):
        # TODO: Swap arguments beta and pose.
        """This module takes betas and poses in a batched manner.
        A pose is 3 * K + 3 (= self.kintree_table.shape[1] * 3) parameters, where K is the number of joints.
        A beta is a vector of size self.shapedirs.shape[2], that parameterizes the body shape.
        Since this is batched, multiple betas and poses should be concatenated along zeroth dimension.
        See http://files.is.tue.mpg.de/black/papers/SMPL2015.pdf for more info.
        """
        batch_size = beta.shape[0]  # Size of zeroth dimension.

        if self.parent is None:
            # self.parent must be reconstructed if we load the model from its state dict.
            self.update_parent()

        # The body shape is decomposed with principal component analysis from many subjects,
        #  where self.v_template is the average value. Then shapedirs is a subset of the orthogonal directions, and
        #  a the betas are the values when the subject is projected onto these. v_shaped is the "restored" subject.
        v_shaped = torch.tensordot(beta, self.shapedirs, dims=([1], [2])) + self.v_template

        # We turn the rotation vectors into rotation matrices.
        R_cube = SML.rodrigues(pose.reshape(-1, 1, 3), self.eye).reshape(batch_size, -1, 3, 3)
        J = self.regress_joints(v_shaped)  # Joints in T-pose (for limb lengths)

        if not simplify:
            # Add pose blend shapes. (How joint angles morphs the surface)
            # Now calculate how joints affects the body shape.
            lrotmin = R_cube[:, 1:] - self.eye
            lrotmin = lrotmin.reshape(batch_size, -1)
            v_shaped += torch.tensordot(lrotmin, self.posedirs, dims=([1], [2]))

        # Now we have the un-posed body shape. Convert to homogeneous coordinates.
        rest_shape_h = torch.cat((v_shaped, v_shaped.new_ones(1).expand(*v_shaped.shape[:-1], 1)), 2)

        G = [self.rotate_translate(R_cube[:, 0], J[:, 0])]
        for i in range(1, self.kintree_table.shape[1]):
            G.append(
                torch.bmm(
                    G[self.parent[i]],
                    self.rotate_translate(R_cube[:, i], J[:, i] - J[:, self.parent[i]])))
        G = torch.stack(G, 1)
        Jtr = G[..., :4, 3].clone()
        G = G - self.pack(torch.matmul(G, torch.cat([J, J.new_zeros(1).expand(*J.shape[:2], 1)], dim=2).unsqueeze(-1)))

        # T = torch.tensordot(self.weights, G, dims=([1], [1]))
        # v = T.reshape(-1, 4, 4).bmm(rest_shape_h.reshape(-1, 4, 1)).reshape(batch_size, -1, 4)

        # Two next lines are a memory bottleneck.
        T = torch.tensordot(G, self.weights, dims=([1], [1])).permute(0, 3, 1, 2)

        v = torch.matmul(T, torch.reshape(rest_shape_h, (batch_size, -1, 4, 1))).reshape(batch_size, -1, 4)
        #Jtr = self.regress_joints(v) # FIXME: Use G[..., :4, 3] instead? before G-self.pack...

        if self.nose_vertex:
            # Vertex 332 is the nose. Add it to joints.
            Jtr = torch.cat( (Jtr, v[:, 332].unsqueeze(1)) , 1)

        # TODO: Add nose joint/vertex. (Any maybe eyes and ears later)
        if trans is not None:
            trans = trans.unsqueeze(1)
            v[..., :3] += trans
            Jtr[..., :3] += trans

        return v, Jtr

if __name__ == "__main__":
    import matplotlib
    #matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt
    from render.renderer import Renderer
    from render.cameraparameters import CameraParameters
    from dataset.cachedSMLdataset import CachedSMLDataset
    from dataset.minirgbd import MINIRGBDDataset
    import cv2


    device = torch.device('cpu')
    dtype = torch.float

    model = SML(nose_vertex=True).to(device, dtype)
    scale = 4
    cp = CameraParameters(width=640*scale, height=480*scale, preset='MINIRGBD')
    render = Renderer(model, cp, visible=False, decoration=False)
    minirgbd = MINIRGBDDataset(seq_len=2)

    pics = []

    # cd = CachedSMLDataset(render,model,[minirgbd], cache_size=1, render_batch_size=1, noise=False)

    pose, trans, beta, _ = minirgbd[0]
    beta = torch.tensor(beta[None]).to(device, dtype).expand(2, -1)
    pose = torch.tensor(pose).to(device, dtype)
    trans = torch.tensor(trans).to(device, dtype)
    
    #simplify true
    v, Jtr = model(beta, pose, trans, simplify=True)

    #beta er ikke nul
    pose[...] = 0
    pose[..., 16*3+2] = -np.pi/4
    pose[..., 17*3+2] = np.pi/4
    trans[:,0] = 0
    trans[:,1] = -0.26
    trans[:,2:] = 0.66 * -1

    v, Jtr = model(beta, pose, trans)
    v[..., 2] *= -1
    Jtr[..., 2] *= -1
    jointnames = "global", "leftThigh", "rightThigh", "spine", "leftCalf", "rightCalf", "spine1", "leftFoot", "rightFoot", "spine2", "leftToes", "rightToes", "neck", "leftShoulder", "rightShoulder", "head", "leftUpperArm", "rightUpperArm", "leftForeArm", "rightForeArm", "leftHand", "rightHand", "leftFingers", "rightFingers", "noseVertex", "background"
    import matplotlib.patheffects as PathEffects
    if True:
        plt.rcParams['svg.fonttype'] = 'none'
        plt.figure(figsize=(6, 3*2))
        plt.imshow(np.copy(render.render(v))[0, ..., 0], cmap='gray_r', interpolation='lanczos')# lanczos
        J2d = render.project_points(Jtr[:1])
        J2d = J2d[0, ..., :2]
        def color(name):
            if name.startswith('left'):
                return 252/255, 13/255, 27/255
            elif name.startswith('right'):
                return 27/255, 174/255, 85/255
            else:
                return 111/255, 53/255, 158/255
        print(len(jointnames), J2d.shape)

        #for idx, (joint, name) in enumerate(zip(J2d, jointnames)):
        for idx in range(len(J2d)):
            if idx == 0 or idx == 24:
                continue
            parent = model.parent[idx]
            line = to_numpy(J2d[[idx, parent]])
            plt.plot(line[...,0],line[..., 1],'-', color=color(jointnames[idx]))

        for idx in range(len(jointnames[:-1])):
            name = jointnames[idx]
            joint = J2d[idx]
            plt.scatter(*joint[:2], c=color(name), s=5)

            txt = plt.text(*(joint[:2] + joint.new_tensor((3, 3))), str(idx), fontsize=12, family='sans-serif', color='w')
            txt.set_path_effects([PathEffects.withStroke(linewidth=2, foreground='k')])
            #print(name, '= (', round(joint[0].item()), ',', round(joint[1].item()), ')')
            print('(', round(joint[0].item()), ',', round(joint[1].item()), ')')

        plt.gca().invert_yaxis()
        plt.axis('off')
        plt.xlim([170*scale, 480*scale])
        plt.tight_layout()
        import os
        plt.savefig(os.path.join('.', 'figures', 'joints.eps'), bbox_inches='tight')
        plt.show()
        #

        exit()
    pics.append(np.copy(render.render(v)))


    pics.append(np.copy(render.render(v)))

    # beta er nul
    beta[...] = 0

    v, Jtr = model(beta, pose, trans) 

    pics.append(np.copy(render.render(v)))
    i = 0
    for pic in pics:
        # plt.imshow(np.array(pic[0,:,:].squeeze()))
        img = pic[0,:,:].squeeze()
        img_scaled = cv2.normalize(img, dst=None, alpha=0, beta=65535, norm_type=cv2.NORM_MINMAX)
        cv2.imwrite('{}_depth.png'.format(i), img_scaled)
        # im = Image.fromarray(np.array(pic[0,:,:].squeeze()))
        # im = im.convert("L")
        # im = ImageOps.autocontrast(im)
        # im.save(str(i)+'_depth.png',format='png')
        i = i + 1


