import torch
from torch import nn
from torch.nn import functional as F

from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.minirgbd import MINIRGBDDataset
from model.SML import SML
from model.cpm import CPM
from dataset.cachetransformer.cpmcache import CPMCache, CPMJoints2dCache
from model.cpmregressor import CPMRegressor
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from render.renderer import Renderer


class RefinementStage(nn.Module):
    def __init__(self, num_heatmaps, num_channels, num_features):
        super().__init__()
        self.first = nn.Conv2d(num_channels+num_heatmaps+num_features, 128, kernel_size=7, stride=1, padding=3)
        middle = []
        for _ in range(4):
            middle.append(nn.Conv2d(128, 128, kernel_size=7, stride=1, padding=3))
            middle.append(nn.ReLU())
        self.middle = nn.Sequential(*middle)
        self.final1 = nn.Conv2d(128, 128, kernel_size=1, stride=1, padding=0)
        self.final2 = nn.Conv2d(128, num_heatmaps, kernel_size=1, stride=1, padding=0)

    def forward(self, image, heatmaps, features):
        x = torch.cat([image, heatmaps, features], 1)
        x = F.relu(self.first(x))
        features = self.middle(x)
        x = self.final1(F.relu(features))
        heatmaps = self.final2(F.relu(x))
        return heatmaps, features


class CPMrefinement(CPM):
    def __init__(self, num_heatmaps, num_channels=1, refinement_stages=1):
        super().__init__(num_heatmaps, num_channels)
        # AvgPool2D vs F.interpolate with the right mode: is there a difference?
        #self.avgpool = nn.AvgPool2d(kernel_size=9, stride=8)
        self.avgpool = nn.AvgPool2d(kernel_size=8, stride=8)
        self._refinement_stages = refinement_stages
        for i in range(self._refinement_stages):
            # TODO: We can use ModuleList instead I believe.
            self.__setattr__('stage_%d' % i, RefinementStage(num_heatmaps, num_channels, 128))

    def forward(self, image):
        heatmaps, features = super().forward(image)
        all_heatmaps = [heatmaps]

        if 0 < self._refinement_stages:
            image = image.reshape(-1, *image.shape[-3:])
            image = self.avgpool(image)
            # all_features = [features]
            for i in range(self._refinement_stages):
                heatmaps, features = self.__getattr__('stage_%d' % i)(image, heatmaps, features)
                all_heatmaps.append(heatmaps)

        # all_heatmaps is of shape [B, S, C, W, H]
        all_heatmaps = torch.stack(all_heatmaps)
        return all_heatmaps, features


if __name__ == "__main__":
    #Cache()['test']
    torch.manual_seed(7)
    resolution = (368, 368)
    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # Load model.
    model = SML().to(device, dtype, non_blocking=True)

    seq_len = 16
    num_verts = model.J_regressor.shape[1]
    plane_faces = model.f.new_tensor([[0, 1, 2], [2, 1, 3]]) + num_verts
    renderer = Renderer(model.f, plane_faces, *resolution, visible=False, decoration=False)

    hmc = HeatMapperCache(*resolution, sigma=7, stride=8, background_confmap=False).to(device, dtype, non_blocking=True)

    cpm = CPM(num_heatmaps=model.num_joints + hmc._background_heatmap)
    cpm.load_state_dict(torch.load('./trainednetworks/cpm_nobg.pt')['model_state_dict'], strict=False)
    cpm.to(device, dtype, non_blocking=True)
    cpmc = CPMCache(cpm, renderer.far, batch_size=40 // seq_len)
    cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

    train_set = MINIRGBDDataset(seq_len, startseq=0, endseq=10, random_betas=False, random_rotation=False)
    pd_train = CachedSMLDataset(renderer, model, [train_set],
                                cache_size=2048,
                                augment_position=True, transforms=cache_transformers,
                                post_transform=cpmc.post_transform_cpm_extension_keypoints(hmc))

    test_set = MINIRGBDDataset(seq_len, startseq=10, endseq=11, random_betas=False, random_rotation=False)
    pd_test = CachedSMLDataset(renderer, model, [test_set],
                               cache_size=1000,
                               augment_position=False, transforms=cache_transformers,
                               post_transform=cpmc.post_transform_cpm_extension_keypoints(hmc))
    print('train set:', train_set._sequence_names)
    print('test set', test_set._sequence_names)

    from trainer import Trainer
    #loss = torch.nn.SmoothL1Loss(reduction='none')

    trainer = Trainer(pd_train, pd_test,
                      loss_fn=CPMRegressor.loss
                      )
    if False:
        pd_train[0]
        pd_train[900]
        pd_train[900 * 2]
        pd_train[900 * 2]
        pd_train[900 * 3]
        pd_train[900 * 4]

    regresor = CPMRegressor(model.num_joints)
    #regresor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
    regresor = regresor.to(device, dtype)

    callback = regresor.callback(pd_train, pd_test) #cpm.callback(pd_train, pd_test, hmc, device, dtype)

    callback()
    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    batch_size = 8*2*8 #8*2*8*2*4
    optimizer = torch.optim.Adam(regresor.parameters(), lr=0.1)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1,
                                                           verbose=True)
    train_result = trainer.resume_training(regresor, optimizer, scheduler, callback=callback, batch_size=batch_size,
    load_optim_state_dict=False ,max_epochs=50, patience=10, max_batch_size=batch_size)
    Trainer.plot_training(*train_result[:2], offset=5)
    exit()

    for lr_i in [0.001]:
        # If model is extremely large, this may run out of memory.
        optimizer = torch.optim.Adam(regresor.parameters(), lr=lr_i)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1, verbose=True)
        #scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, batch_size)
        train_result = trainer.train(regresor, batch_size,
                                     optimizer, scheduler,
                                     max_epochs=50, patience=10,
                                     callback=callback,
                                     max_batch_size=batch_size)
        Trainer.plot_training(*train_result[:2], offset=5)