import numpy
import torch
from matplotlib import pyplot as plt
import numpy as np

from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from dataset.minirgbd import MINIRGBDDataset
from model.SML import SML
from model.cpm import CPM, mycmap, post_transform_cpm_synthetic, post_transform_cpm_mikkel
from render.cameraparameters import CameraParameters
from render.renderer import Renderer
from utils import to_numpy, determinism
import os

def mikkel_cpm_statistics():
    from dataset.mikkel import mikkel_dataset
    train_set, test_set, _ = mikkel_dataset(file_path='H')

    train_set, test_set, _ = mikkel_dataset(file_path=os.path.join('.', 'mikkeldata'))
    exit()


def cpm_statistics(test_set : CachedSMLDataset, cpm : CPM, cpm_path,
                   visualize = False, shuffle=True, jointnames=None):
    from dataset.cachetransformer.joints2dcache import Joints2DCache
    from model.heatmapper import HeatMapper
    from tqdm import tqdm
    from trainer import Trainer

    resume = torch.load(cpm_path)
    cpm.load_state_dict(resume['model_state_dict'])
    resume = resume['trainer_state']
    Trainer.plot_training(resume['train_losses'], resume['test_losses'], offset=3)

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    cpm = cpm.to(device, dtype, non_blocking=True)

    def post_transform(cache):
        normalizer = 1
        depth = cache['depth']
        if np.issubdtype(depth.dtype, np.integer):
            normalizer = np.iinfo(depth.dtype).max
        depth = torch.as_tensor(depth.astype(np.float32) / normalizer,
                                dtype=torch.float) * test_set._renderer.far

        return depth, torch.as_tensor(cache[Joints2DCache.name]), torch.as_tensor(cache[HeatMapperCache.name])

    test_set._post_transform = post_transform
    for transform in test_set._transforms:
        if isinstance(transform, HeatMapper):
            hmc = transform

    hmc = hmc.to(device, dtype, non_blocking=True)
    if shuffle:
        test_set.shuffle() # To give more varied visualizations.
    test_dataset = torch.utils.data.DataLoader(test_set, batch_size=30 if not visualize else 1, pin_memory=True)

    errors = []
    heatmaperrors = []
    with torch.no_grad():
        cpm.eval()
        #for idx, sample in enumerate(tqdm(test_dataset)):
        if True:
            test_set.shuffle()
            idx = 4
            idx, sample = idx, map(lambda t: t.unsqueeze(0), test_set[idx]) # Worst frame
            #idx, sample = 915, map(lambda t: t.unsqueeze(0), test_set[915]) # Worst frame
            #idx, sample = 585, map(lambda t: t.unsqueeze(0), test_set[585]) # Worst frame
            depth, joints2d, gt_heatmaps = map(lambda t: t.to(device, dtype, non_blocking=True), sample)
            pred_heatmap, _ = cpm(depth)
            pred2d, _ = hmc.get_prediction(pred_heatmap.reshape(-1, *pred_heatmap.shape[-3:]),
                                           tune_centers=True)
            pred2d = pred2d.reshape(*pred_heatmap.shape[:-3], *pred2d.shape[1:])

            # TODO: If joints2d do not match, print it in console and do this:
            if visualize:
                #test_set.shuffle() # Shuffle for more varied samples
                ax = plt.subplot()
                img = ax.imshow(to_numpy(depth[0, 0, 0]).T, cmap='gray')
                name, frame = test_set.get_sample_id(idx)
                ax.set_title('Seq. ' + name + ' frame ' + str(frame))
                plt.colorbar(img, fraction=0.046, pad=0.04).set_label('depth [m]')
                hasgt = pred2d.shape[-2] == joints2d.shape[-2]
                print(joints2d.shape, pred2d.shape)
                from matplotlib.patches import Circle, Arrow
                for joint in range(pred2d.shape[-2]):
                    if hasgt:
                        gt = to_numpy(joints2d[0, 0, joint])[..., :2]
                    pred = to_numpy(pred2d[0, 0, joint])[..., :2]
                    if hasgt:
                        ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w', width=0.5))
                        ax.text(*gt, str(joint), fontsize=6, )
                        ax.add_patch(Circle(gt, 0.3, color='g'))
                    else:
                        ax.text(*pred, str(joint), fontsize=6, )
                    ax.add_patch(Circle(pred, 0.3, color='r'))
                    # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))

                plt.savefig(os.path.join('.', 'figures', 'cpm_visualize0.eps'), bbox_inches='tight')
                plt.show(block=False)
                if False:
                    for idx in range(5):
                        ax = plt.subplot()
                        ax.set_axis_off()
                        ax.imshow(to_numpy(pred_heatmap[0, 0, idx]).T, cmap=mycmap, vmin=0, vmax=1, interpolation='none')
                        plt.savefig(os.path.join('.', 'figures', 'cpm_visualize_heatmap' + str(idx) + '.png'),
                                    bbox_inches='tight', transparent=True)
                    plt.show(block=False)
                    pass

                columns = 6
                num_subplots = pred2d.shape[-2]
                rows = num_subplots // columns + (num_subplots % columns != 0)
                # Plot of just heatmaps # TODO: add colorbars, if it can be done in a still tight format.
                for i in range(rows):
                    for j in range(columns):
                        idx = i * columns + j
                        if idx >= num_subplots:
                            break
                        ax = plt.subplot(rows, columns, idx + 1)
                        # plt.imshow(pred_heatmap[idx].T, vmin=0, vmax=1)
                        plt.imshow(to_numpy(pred_heatmap[0, 0, idx]).T)
                        ax.axes.get_xaxis().set_ticks([])
                        ax.axes.get_yaxis().set_ticks([])
                        ax.set_xlabel(str(idx))
                plt.savefig(os.path.join('.', 'figures', 'cpm_visualize1.eps'), bbox_inches='tight')
                plt.show(block=False)

                hd = True
                if hd:
                    # Plot of heatmaps overlayed onto the input depth image.
                    minidepth = to_numpy(depth[0, 0, 0]).T #to_numpy(hmc.downscale_img(depth[0], mode='nearest')[0, 0]).T
                    for i in range(rows):
                        for j in range(columns):
                            idx = i * columns + j
                            if idx >= num_subplots:
                                ax = plt.subplot(rows, columns, idx + 1)
                                plt.axis('off')
                                #plt.colorbar(im, fraction=1, panchor=(0.0, 0.0)) # This references the im below through python scope magic.
                                #plt.colorbar(im) # This references the im below through python scope magic.
                                print('BAR')
                                break

                            ax = plt.subplot(rows, columns, idx + 1)
                            ax.imshow(minidepth, cmap='gray', interpolation='none')
                            im = ax.imshow(to_numpy(torch.nn.functional.interpolate(pred_heatmap[:, :, idx], scale_factor=8,
                                                                                    mode='nearest')[0, 0]).T, cmap=mycmap, vmin=0, vmax=1,
                                           interpolation='none')
                            #if idx == num_subplots-1:
                            #    plt.colorbar(im)
                            # ax.add_patch(Circle(to_numpy(joints2d[0, 0, idx]) / cpm.stride, 0.2, color='g'))
                            #ax.add_patch(Circle(to_numpy(pred2d[0, 0, idx]), 0.2, color='y'))
                            ax.axes.get_xaxis().set_ticks([])
                            ax.axes.get_yaxis().set_ticks([])
                            jointname = str(idx) if jointnames is None else jointnames[idx]
                            ax.set_xlabel(jointname)  # , fontsize=10
                    plt.gcf().set_figwidth(8)
                    plt.gcf().set_figheight(6)
                    plt.tight_layout()
                    plt.savefig(os.path.join('.', 'figures', 'cpm_visualize2.pdf'), bbox_inches='tight')
                    plt.show(block=False)
                else:
                    # Plot of heatmaps overlayed onto the input depth image.
                    minidepth = to_numpy(hmc.downscale_img(depth[0], mode='nearest')[0, 0]).T
                    for i in range(rows):
                        for j in range(columns):
                            idx = i * columns + j
                            if idx >= num_subplots:
                                break

                            ax = plt.subplot(rows, columns, idx + 1)
                            ax.imshow(minidepth, cmap='gray', interpolation='none')
                            ax.imshow(to_numpy(pred_heatmap[0, 0, idx]).T, cmap=mycmap, vmin=0, vmax=1,
                                      interpolation='none')
                            # ax.add_patch(Circle(to_numpy(joints2d[0, 0, idx]) / cpm.stride, 0.2, color='g'))
                            ax.add_patch(Circle(to_numpy(pred2d[0, 0, idx]) / cpm.stride, 0.2, color='r'))
                            ax.axes.get_xaxis().set_ticks([])
                            ax.axes.get_yaxis().set_ticks([])
                            jointname = str(idx) if jointnames is None else jointnames[idx]
                            ax.set_xlabel(jointname)  # , fontsize=10
                    plt.gcf().set_figwidth(8)
                    plt.gcf().set_figheight(6)
                    plt.tight_layout()
                    plt.savefig(os.path.join('.', 'figures', 'cpm_visualize2.png'), bbox_inches='tight')
                    plt.show(block=False)

                exit()
            else:
                # Average error (euclidean) of joints. We take 0 in the sequence dimension as there
                #  is just one frame.
                joints2d = joints2d[..., :2]
                error = ((pred2d - joints2d) ** 2).sum(-1).sqrt().to('cpu')
                heatmaperrors.append( ( (gt_heatmaps-pred_heatmap) ** 2 ).sum([-1, -2]).to('cpu') )

                errors.append(error)
    errors = torch.cat(errors,0)
    jointerr = errors.mean([0, 1])
    print('mean error:', errors.mean(),
          'median error', errors.median(),
          'max error:', errors.max().item())
    print('mean error for specific joints:', jointerr)
    print('std error for specific joints:', errors.std(0))

    plt.bar(x=range(0, len(jointerr)), height=to_numpy(jointerr))
            #yerr=to_numpy(errors.std(0).squeeze()*1.96))
    plt.title('Joint error (2D)')
    plt.ylabel('AJPE [pixels]')
    plt.xlabel('Joint #')
    plt.savefig(os.path.join('.', 'figures', 'cpm_jointerr.eps'), bbox_inches='tight')
    plt.show()

    if True: # visualize temporal error over the sequence
        print('Max error frame', errors[:, 0, :].mean(-1).max(0))
        print('Min error frame', errors[:, 0, :].mean(-1).min(0))
        x = to_numpy(errors[:, 0, :].mean(-1))
        N = x.shape[0]
        plt.plot(x)
        plt.ylabel('AJPE [pixels]')
        plt.xlabel('Frame')
        ylimits = list(plt.ylim())
        plt.ylim([0, ylimits[1]])
        plt.savefig(os.path.join('.', 'figures', 'cpm_jointerr_time.eps'), bbox_inches='tight')
        plt.title('AJPE over time (Seq. 12)')
        plt.show()


    heatmaperrors = torch.cat(heatmaperrors,0)
    print('mean HM error:', heatmaperrors.mean(),
          'median HM error', heatmaperrors.median(),
          'max HM error:', heatmaperrors.max().item())
    jointerr = heatmaperrors.mean([0, 1])
    plt.bar(x=range(0, len(jointerr)), height=to_numpy(jointerr))
    plt.title('Heatmap loss')
    plt.xlabel('Joint #')

    plt.savefig(os.path.join('.', 'figures', 'cpm_heatmaperr.eps'), bbox_inches='tight')
    plt.show()


def cpm_statistics_minirgbd(cpm_path, camera_params=CameraParameters(640 // 2, 480 // 2, preset='MIKKEL'),
                            sigma=7, nose=True, noise=True, background_heatmap=False, visualize=False,
                            augment=False):

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

    renderer = Renderer(model, camera_params, visible=False, decoration=False)

    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma,
                          stride=CPM.stride, background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)
    cache_transformers = [hmc]

    test_set = MINIRGBDDataset(seq_len=1, startseq=10, endseq=11, random_betas=False, random_rotation=augment)
    pd_test = CachedSMLDataset(renderer, model, [test_set], noise=noise,
                               # FIXME: Should we also add noise to validation set?
                               cache_size=1000, render_batch_size=1000,
                               augment_position=augment, transforms=cache_transformers,
                               post_transform=post_transform_cpm_synthetic(renderer.far))
    print('Test set (MINIRGBD):', test_set._sequence_names)

    num_heatmaps = model.num_joints + hmc._background_heatmap

    cpm = CPM(num_heatmaps=num_heatmaps)
    jointnames = "global", "leftThigh", "rightThigh", "spine", "leftCalf", "rightCalf", "spine1", "leftFoot", "rightFoot", "spine2", "leftToes", "rightToes", "neck", "leftShoulder", "rightShoulder", "head", "leftUpperArm", "rightUpperArm", "leftForeArm", "rightForeArm", "leftHand", "rightHand", "leftFingers", "rightFingers", "noseVertex", "background"
    cpm_statistics(pd_test, cpm, cpm_path=cpm_path, visualize=visualize, jointnames=jointnames)


def cpm_statistics_mikkel(cpm_path, mikkel_dataset_path='H:', jointnames=None, # FIXME GET MIKKEL JOINT NAMES
                         sigma=7, background_heatmap=False, num_joints=19,
                          visualize=False):
    from dataset.cachedMIKKELdataset import cached_mikkel_dataset

    _, pd_test, _ = cached_mikkel_dataset(post_transform=post_transform_cpm_mikkel,
                                          sigma=sigma, background_heatmap=background_heatmap,
                                          file_path=mikkel_dataset_path)

    print('Test set: Mikkel test split')
    cpm = CPM(num_heatmaps=num_joints + background_heatmap)
    cpm_statistics(pd_test, cpm, cpm_path=cpm_path, visualize=visualize, jointnames=jointnames)


def cpm_statistics_cross(nose=True, *args, **kwargs):
    jointnames = "global", "leftThigh", "rightThigh", "spine", "leftCalf", "rightCalf", "spine1", "leftFoot", "rightFoot", "spine2", "leftToes", "rightToes", "neck", "leftShoulder", "rightShoulder", "head", "leftUpperArm", "rightUpperArm", "leftForeArm", "rightForeArm", "leftHand", "rightHand", "leftFingers", "rightFingers", "noseVertex", "background"
    cpm_statistics_mikkel(*args, **kwargs, num_joints=SML(nose_vertex=nose).num_joints,
                          visualize=True, jointnames=jointnames) # We can only visualize as we don't have Mikkel ground truth SML joints.

def cpm_visualize(sigma=7, nose=True, noise=True, background_heatmap=False):
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    augment_flip = False
    augment = False
    cpm_path = os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7.pt')

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

    renderer = Renderer(model, camera_params, visible=False, decoration=False)

    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma,
                          stride=CPM.stride, background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)
    cache_transformers = [hmc]

    dataset = MINIRGBDDataset(seq_len=1, startseq=11, endseq=12,
                              random_betas=False,
                              random_rotation=augment)
    pd_dataset = CachedSMLDataset(renderer, model, [dataset], noise=noise,
                               # FIXME: Should we also add noise to validation set?
                               cache_size=2000, render_batch_size=1000, augment_flip=augment_flip,
                               augment_position=augment, transforms=cache_transformers,
                               post_transform=post_transform_cpm_synthetic(renderer.far))
    print('Test set (MINIRGBD):', dataset._sequence_names)

    num_heatmaps = model.num_joints + hmc._background_heatmap

    cpm = CPM(num_heatmaps=num_heatmaps)
    visualize = True
    jointnames = "global", "leftThigh", "rightThigh", "spine", "leftCalf", "rightCalf", "spine1", "leftFoot", "rightFoot", "spine2", "leftToes", "rightToes", "neck", "leftShoulder", "rightShoulder", "head", "leftUpperArm", "rightUpperArm", "leftForeArm", "rightForeArm", "leftHand", "rightHand", "leftFingers", "rightFingers", "noseVertex", "background"
    cpm_statistics(pd_dataset, cpm, cpm_path=cpm_path,
                   visualize=visualize, jointnames=jointnames,
                   shuffle=False)


if __name__ == "__main__":

    # Findings: Initializing the network with VGG19 (first 10 layers) or SSL (all layers) gives much better results
    # than the default initialization strategy in pytorch. A higher sigma may make the joint estimation worse, but
    # there is no significant effect if we lower it.
    determinism()
    mikkel_cpm_statistics()

    #cpm_visualize()
    cpm_statistics_mikkel(os.path.join('trainednetworks', 'CPM7-REAL-MIKKEL-SSL-1.pt'),
                          visualize=True)
    exit()
    cpm_statistics_cross(nose=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7.pt'),
                            sigma=7)
    exit()

    exit()
    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            visualize=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7.pt'),
                            sigma=7)
    exit()
    cpm_statistics_cross(nose=True,
                         background_heatmap=False,
                         cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-1.pt'),
                         sigma=7)
    exit()
    # Results of the statistics: Only the first one seem to be any good. Very odd.
    # We confirmed this by setting tune_centers to false in the heatmapper.
    # The other models did not seem to converge very well.
    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-SSL-1.pt'),
                            sigma=7)

    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM5-SYNTHETIC(NOSE)-MIKKEL-SSL-2.pt'),
                            sigma=5)

    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM9-SYNTHETIC(NOSE)-MIKKEL-SSL-3.pt'),
                            sigma=9)

    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-4.pt'),
                            sigma=7)

    cpm_statistics_minirgbd(nose=True,
                            noise=True,
                            background_heatmap=False,
                            cpm_path=os.path.join('trainednetworks', 'CPM7-SYNTHETIC(NOSE)-MIKKEL-VGG19-6.pt'),
                            sigma=7)


    pass