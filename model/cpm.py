from torch import nn
import torch.nn.functional as F
from collections import OrderedDict

from model.SML import SML
from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.minirgbd import MINIRGBDDataset
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from render.renderer import Renderer
from torch.utils.data.dataset import Dataset
from matplotlib.colors import ListedColormap
from PIL import Image
import torchvision.transforms.functional as TF
import matplotlib.pyplot as plt
from utils import *
import os, io


def transparent_cmap():
    "Copy colormap and set alpha values"
    # https://stackoverflow.com/questions/42481203/heatmap-on-top-of-image
    N=255
    vals = np.zeros((N, 4))
    vals[:, 0] = 1 #np.linspace(90 / 256, 1, N)
    vals[(N+1)//16:, 3] = np.linspace(0, 0.9, N)[(N+1)//16:]
    #from matplotlib.colors import ListedColormap, LinearSegmentedColormap
    mycmap = ListedColormap(vals)
    #mycmap = plt.cm.Reds
    #mycmap._init()
    #mycmap._lut[:, :-1] = 0
    #print(mycmap._lut, mycmap._lut.shape)
    #exit()
    #mycmap._lut[:, 0] = 1
    #mycmap._lut[:,-1] = np.linspace(0, 0.9, N+4)
    return mycmap
mycmap = transparent_cmap() # Used in the callback visualization.

count = 0
class CPM(nn.Module):
    # R - (2D) Regressor
    def __init__(self, num_heatmaps, num_channels=1):
        super().__init__()
        self._num_heatmaps = num_heatmaps
        #TODO: Remove the sequential again?
        self.first10 = nn.Sequential(
            nn.Conv2d(num_channels, 64, kernel_size=3, stride=1, padding=1), # conv1_1
            nn.ReLU(), # relu1_1
            nn.Conv2d(64, 64, kernel_size=3, stride=1, padding=1), # conv1_2
            nn.ReLU(), # relu1_2
            nn.MaxPool2d(kernel_size=2, stride=2), # pool1_stage1
            nn.Conv2d(64, 128, kernel_size=3, stride=1, padding=1), # conv2_1
            nn.ReLU(), # relu2_1
            nn.Conv2d(128, 128, kernel_size=3, stride=1, padding=1), # conv2_2
            nn.ReLU(), # relu2_2
            nn.MaxPool2d(kernel_size=2, stride=2), # pool2_stage1
            nn.Conv2d(128, 256, kernel_size=3, stride=1, padding=1), # conv3_1
            nn.ReLU(), # relu3_1
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1), # conv3_2
            nn.ReLU(), # relu3_2
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1), # conv3_3
            nn.ReLU(), # relu3_3
            nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1), # conv3_4
            nn.ReLU(), # relu3_4
            nn.MaxPool2d(kernel_size=2, stride=2), # pool3_stage1
            nn.Conv2d(256, 512, kernel_size=3, stride=1, padding=1), # conv4_1
            nn.ReLU(), # relu4_1
            nn.Conv2d(512, 512, kernel_size=3, stride=1, padding=1), # conv4_2
            nn.ReLU() # relu4_2
        )

        self.conv4_3 = nn.Conv2d(512,256,kernel_size=3,stride=1,padding=1)
        self.conv4_4 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_5 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_6 = nn.Conv2d(256,256,kernel_size=3,stride=1,padding=1)
        self.conv4_7 = nn.Conv2d(256,128,kernel_size=3,stride=1,padding=1)
        #self.fc = nn.Linear(128, num_joints + 1)
        self.conv5_1 = nn.Conv2d(128,512,kernel_size=1,stride=1,padding=0)
        self.conv5_2 = nn.Conv2d(512,num_heatmaps, kernel_size=1,stride=1,padding=0)

    stride = 8 # Stride is 8 since we do pooling thrice, and all other operations preserve shapes.

    @staticmethod
    def loss(output, target):
        # [B, S, C, W, H], batch, sequence, joint/channel, width, height
        if False:
            global count
            count += 1
            if count >= 1000:
                # To debug training:
                print('')
                stats = lambda t: tuple(map(torch.Tensor.item, (t.min(), t.mean(), t.max())))
                print('output:', "%.6f %.6f %.6f" % stats(output[0]))
                print('target:', "%.6f %.6f %.6f" % stats(target))
                # print('target:', *stats(target))
                print(' error:', "%.6f" % ((output[0] - target) ** 2).sum().item())
                # Keep an eye on how the maximum value develops. (Outputting zeros is safest initially)
                # This is why the max and mean value in output start approaching the mean value of the target.
                # It is easy for the network to get stuck in the suboptimal situation where it just predicts around
                #  the mean value of the target.
                count %= 1000
        return ( (output[0] - target)**2 ).sum([-4, -3, -2, -1])

    def forward(self, image):
        #plt.imshow(to_numpy(image[0, 0, 0]).T, cmap='gray')
        #plt.show()
        seq_len = image.shape[1]
        seqshape = lambda x: x.reshape(-1, seq_len, *x.shape[1:])
        flatshape = lambda x: x.reshape(-1, *x.shape[2:])

        image = flatshape(image)
        x = self.first10(image)
        #if self.batchnorm is not None:
            # We place it after the ReLU. (Placing it before also does well)
            # A good explanation of our reasoning for this: https://stackoverflow.com/a/45624249
        #    x = self.batchnorm(x)
        x = F.relu(self.conv4_3(x))
        x = F.relu(self.conv4_4(x))
        x = F.relu(self.conv4_5(x))
        x = F.relu(self.conv4_6(x))
        features = self.conv4_7(x)
        x = self.conv5_1(F.relu(features))
        heatmaps = self.conv5_2(F.relu(x))

        return seqshape(heatmaps), seqshape(features)

    def callback(self, pd_train, pd_test, hmc, device, dtype):
        # TODO: Add joint names instead of numbers to the subplots.
        def cb():
            pd_train.shuffle() # IMPORTANT: Shuffle dataset!
            if True:
                pd_example = pd_train
            else:
                pd_example = pd_test
            example = 325 # 245 + 20
            pd_example[example]  # Make sure cache is ready.
            result = pd_example._cache[example - pd_example._cache_ptr, -1]  # Middle batch, last element in seq.

            normalizer = 1
            depth = result['depth']
            if np.issubdtype(depth.dtype, np.integer):
                normalizer = np.iinfo(depth.dtype).max
            depth = depth.astype(np.float32) / normalizer * pd_example._renderer.far
            ax = plt.subplot()
            img = ax.imshow(depth[0].T, cmap='gray')
            name, frame = pd_example.get_sample_id(example)
            ax.set_title(name + ' frame ' + str(frame))
            plt.colorbar(img).set_label('depth [m]')

            pred_heatmap, _ = self(torch.as_tensor(depth[None, None], device=device, dtype=dtype))
            if len(pred_heatmap.shape) == 4:
                pred_heatmap = pred_heatmap[None]
            for pred_heatmap in pred_heatmap:
                pred_heatmap = pred_heatmap[0]

                pred2d, _ = hmc.get_prediction(pred_heatmap.reshape(-1, *pred_heatmap.shape[-3:]))
                pred2d = pred2d.reshape(*depth.shape[:1], *pred2d.shape[1:])[0]

                pred2d, joints2d, pred_heatmap = map(to_numpy, (pred2d, result['joints2d'][..., :2], pred_heatmap))

                from matplotlib.patches import Circle, Arrow
                for joint in range(len(joints2d)):
                    gt = joints2d[joint]
                    pred = pred2d[joint]
                    ax.add_patch(Circle(gt, 0.3, color='g'))
                    ax.add_patch(Circle(pred, 0.3, color='r'))
                    ax.text(*gt, str(joint), fontsize=6)
                    # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                    ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))

                plt.show(block=False)

                columns = 6
                num_subplots = len(joints2d)
                rows = num_subplots // columns + (num_subplots % columns != 0)
                # Plot of just heatmaps # TODO: add colorbars, if it can be done in a still tight format.
                for i in range(rows):
                    for j in range(columns):
                        idx = i * columns + j
                        if idx >= num_subplots:
                            break
                        ax = plt.subplot(rows, columns, idx + 1)
                        #plt.imshow(pred_heatmap[idx].T, vmin=0, vmax=1)
                        plt.imshow(pred_heatmap[idx].T)
                        ax.axes.get_xaxis().set_ticks([])
                        ax.axes.get_yaxis().set_ticks([])
                        ax.set_xlabel(str(idx))
                plt.show(block=False)

                # Plot of heatmaps overlayed onto the input depth image.
                minidepth = to_numpy(hmc.downscale_img(torch.as_tensor(depth)[None])[0, 0]).T
                for i in range(rows):
                    for j in range(columns):
                        idx = i * columns + j
                        if idx >= num_subplots:
                            break
                        ax = plt.subplot(rows, columns, idx + 1)
                        ax.imshow(minidepth, cmap='gray')
                        ax.imshow(pred_heatmap[idx].T, cmap=mycmap, vmin=0, vmax=1)

                        ax.add_patch(Circle(joints2d[idx]/self.stride, 0.2, color='g'))
                        ax.axes.get_xaxis().set_ticks([])
                        ax.axes.get_yaxis().set_ticks([])
                        ax.set_xlabel(str(idx))
                plt.show(block=False)
        return cb

def post_transform_cpm(far):
    # Since most of our data is at a depth
    def fun(cache_subset):
        # pd is arg just to get _renderer.far
        normalizer = 1
        depth = cache_subset['depth']
        if np.issubdtype(depth.dtype, np.integer):
            normalizer = np.iinfo(depth.dtype).max
        depth = torch.as_tensor(depth.astype(np.float32) / normalizer) * far

        heatmap = torch.as_tensor(cache_subset[HeatMapperCache.name])
        return depth, heatmap
    return fun

def post_transform_cpm_synthetic(far):
    # Since most of our data is at a depth
    def fun(cache_subset):
        # pd is arg just to get _renderer.far
        normalizer = 1
        depth = cache_subset['depth']
        if np.issubdtype(depth.dtype, np.integer):
            normalizer = np.iinfo(depth.dtype).max
        depth = torch.as_tensor(depth.astype(np.float32) / normalizer) * far

        heatmap = torch.as_tensor(cache_subset[HeatMapperCache.name])
        return depth, heatmap
    return fun

def post_transform_cpm_mikkel(cache_subset):
    # pd is arg just to get _renderer.far
    depth = cache_subset['depth']
    depth = torch.as_tensor(depth.astype(np.float32)) / 1000
    heatmap = torch.as_tensor(cache_subset[HeatMapperCache.name])
    return depth, heatmap

class MPII(Dataset):
    def __init__(self, path):
        self._root = path

        annotation_path = os.path.join(self._root, 'annotation', 'train_h36m.txt')
        #images_path = os.path.join(self._root, 'square', '*.jpg')

        s = io.BytesIO(open(annotation_path, 'rb').read().replace(b' ', b','))
        annotations = np.genfromtxt(s, delimiter=',', dtype=np.dtype([('name', 'S30'), ('joints', (np.float, 32))]))

        self._files = annotations.getfield('S30', offset=0).astype(np.str)
        self._positions = annotations.getfield((np.float, 32), offset=np.dtype('S30').itemsize)

    def __getitem__(self, idx):
        path = os.path.join(self._root, self._files[idx])
        image = Image.open(path)
        return TF.to_tensor(image)

    def __len__(self):
        return len(self._files)

def load_vgg19(cpm):
    # Copy over vgg19 (first 10) to the first10 of cpm.
    vgg19: OrderedDict = torch.load('vgg19-dcbb9e9d.pth')  # OrderedDict, actually.
    with torch.no_grad():
        for param in cpm.first10.parameters():
            new_param = vgg19.popitem(False)[1]
            if param.shape < new_param.shape:
                # This can happen in the first Conv2D because number of channels does not match.
                # E.g. we use one channel (Depth) while VGG19 uses 3 (BGR/RGB).
                print('Warning: The number of channels in input image does not match.')
                param[...] = new_param.mean(1, keepdim=True)
            else:
                param[...] = new_param
    return cpm

def swap(nums):
    # https://stackoverflow.com/questions/45697347/swapping-pairs-in-a-list-in-an-iterative-and-recursive-way-python3
    for i in range(0, len(nums) - 1, 2):
        nums[i], nums[i + 1] = nums[i + 1], nums[i] # Multiple assigment <3
    return nums

def load_pretrained_CPM(cpm : CPM):
    CPM_caffemodel: OrderedDict = torch.load('pose_iter_320000.caffemodel.pth')
    goodkeys = sorted([key for key in CPM_caffemodel.keys() if key.startswith('conv')])
    goodparams = swap(goodkeys) # Does this not work in-place?
    goodparams = OrderedDict((key, CPM_caffemodel[key]) for key in goodkeys)
    with torch.no_grad():
        for idx, param in enumerate(cpm.parameters()):
            key, new_param = goodparams.popitem(False)
            if param.shape < new_param.shape and idx == 0:
                # This can happen in the first Conv2D because number of channels does not match.
                # E.g. we use one channel (Depth) while VGG19 uses 3 (BGR/RGB).
                print('Warning while loading: The number of channels in input image does not match.')
                param[...] = new_param.mean(1, keepdim=True)
            elif param.shape != new_param.shape and idx in (32, 33):
                # Last layer, as such this can happen when the number of output heatmaps does not match.
                n = len(param)
                #print(param.shape, new_param.shape)
                #print(key, param.max(), param.min(), new_param.max(), new_param.min())
                if idx == 32:
                    # weight
                    param *= 10
                else:
                    # bias
                    param[...] = 0
                    # The ones corresponding to joints should be initialized as zero
                    # The single background should be initialized as 1.
                #exit()
                #n = param.in_channels
                #init.xavier_uniform_(self.weight, a=math.sqrt(5))
                #if self.bias is not None:
                #    fan_in, _ = init._calculate_fan_in_and_fan_out(self.weight)
                #    bound = 1 / math.sqrt(fan_in)
                #    init.uniform_(self.bias, -bound, bound)

            else:
                #print(key, idx, new_param.shape, param.shape)
                try:
                    param[...] = new_param
                except:
                    print('error in import', idx)
                    pass

    return cpm

def train_mikkel_cpm(file_path='H:', sigma=7, background_heatmap=False, batch_size=16, **kwargs):
    from dataset.cachedMIKKELdataset import cached_mikkel_dataset
    from dataset.mikkel import MikkelSequence

    pd_train, pd_test, _ = cached_mikkel_dataset(post_transform=post_transform_cpm_mikkel,
                                                 sigma=sigma, background_heatmap=background_heatmap, file_path=file_path)

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    hmc = pd_train._transforms[1].to(device, dtype) # TODO: Find a better way to get this. (It is only used in the callback visualization)
    num_heatmaps = MikkelSequence.num_joints + background_heatmap
    cpm = CPM(num_heatmaps=num_heatmaps)
    callback = cpm.callback(pd_train, pd_test, hmc, device, dtype)
    train_cpm(pd_train, pd_test, cpm, decay_schedule=True, batch_size=batch_size, device=device, dtype=dtype, callback=callback, **kwargs)


def train_synthetic_cpm(camera_params, sigma=7, nose=True,
                        noise=True, background_heatmap=False,
                        visible=False, flip=False, **kwargs):
    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    # Load model.
    model = SML(nose_vertex=nose).to(device, dtype, non_blocking=True)

    renderer = Renderer(model, camera_params, visible=visible, decoration=False)

    seq_len = 1
    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma,
                          stride=CPM.stride, background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)
    cache_transformers = [hmc]
    train_set = MINIRGBDDataset(seq_len, startseq=0, endseq=10, random_betas=False, random_rotation=True)
    pd_train = CachedSMLDataset(renderer, model, [train_set], noise=noise,
                                cache_size=100, render_batch_size=1000,
                                augment_position=True, augment_flip=flip, transforms=cache_transformers,
                                post_transform=post_transform_cpm_synthetic(renderer.far))

    # The fact that we are not training with the original MINIRGBD parameters
    # means that we have to augment position/rotation in the test set. Just because
    # otherwise it ...
    test_set = MINIRGBDDataset(seq_len, startseq=10, endseq=11, random_betas=False, random_rotation=False)
    pd_test = CachedSMLDataset(renderer, model, [test_set], noise=noise, # FIXME: Should we also add noise to validation set?
                               cache_size=1000, render_batch_size=1000,
                               augment_position=False, transforms=cache_transformers,
                               post_transform=post_transform_cpm_synthetic(renderer.far))
    print('train set:', train_set._sequence_names)
    print('test set', test_set._sequence_names)

    num_heatmaps = model.num_joints+hmc._background_heatmap
    cpm = CPM(num_heatmaps=num_heatmaps)
    callback = cpm.callback(pd_train, pd_test, hmc, device, dtype)
    train_cpm(pd_train, pd_test, cpm, device=device, dtype=dtype, callback=callback, **kwargs)


def train_cpm(pd_train, pd_test, cpm, device, dtype, callback, lr=0.001, filename='bestcpm.pt',
              pretrained=None, max_epochs=80, decay_schedule=False, batch_size=8):
    # The pretrained parameter should either be "SSL" or "VGG19"
    # Warning: When resuming training from a model, the weights are of course overwritten
    #  this means that using pretrained weights will be discarded.

    from trainer import Trainer
    trainer = Trainer(pd_train, pd_test,
                      # [b, s, c, w, h]
                      loss_fn=cpm.loss
                      )

    if pretrained=='SSL':
        print('Loading pretrained (3D-SSL CPM) weights')
        cpm = load_pretrained_CPM(cpm)
    elif pretrained == 'VGG19':
        print('Loading pretrained (VGG19 first10) weights')
        cpm = load_vgg19(cpm)
    cpm = cpm.to(device, dtype, non_blocking=True)

    callback() # The callback is important since it shuffles the pd_train dataset
    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    # If model is extremely large, this may run out of memory.
    optimizer = torch.optim.Adam(cpm.parameters(), lr=lr)
    if not decay_schedule:
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=4, threshold=0.1,
                                                               verbose=True)
    else:
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.1)
    trainer.resume_training(cpm, optimizer, scheduler, filename=filename, callback=callback,
                            batch_size=batch_size, max_epochs=max_epochs, patience=8,
                            max_batch_size=batch_size)


