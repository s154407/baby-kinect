import os

import torch

import numpy as np
from torch import nn

#import matplotlib
#matplotlib.use("TkAgg")  # Do this before importing pyplot!
import matplotlib.pyplot as plt
#from matplotlib import pyplot as plt # How is this different than the above?


from dataset.cachedSMLdataset import CachedSMLDataset
from dataset.minirgbd import MINIRGBDDataset
from model.SML import SML
from model.cpm import CPM
from dataset.cachetransformer.cpmcache import CPMJoints2dCache, CPMCache
from dataset.cachetransformer.heatmappercache import HeatMapperCache
from render.cameraparameters import CameraParameters
from render.renderer import Renderer
from utils import to_numpy, timing, determinism
from matplotlib.patches import Circle, Arrow
from mpl_toolkits.mplot3d import Axes3D

count = 0 # Inconspicuous global.

class CPMDepthFixer(nn.Module):
    activation = nn.ReLU()
    def __init__(self, num_joints, project_z, unproject_p, linear_size=1024):
        super().__init__()
        self.project_z = project_z
        self.unproject_p = unproject_p
        self.register_buffer('ones', torch.ones((1,), dtype=torch.float))

        prefix = torch.zeros((num_joints,), dtype=torch.float)
        if num_joints == 25 or num_joints==24: # SMIL joints
            prefix[:24] = torch.as_tensor([0.0457, 0.0447, 0.0419, 0.0738, 0.0230, 0.0229, 0.0511, 0.0158, 0.0159,
                                      0.0431, 0.0119, 0.0101, 0.0438, 0.0387, 0.0402, 0.0410, 0.0268, 0.0287,
                                      0.0220, 0.0233, 0.0162, 0.0166, 0.0092, 0.0119])
            # We save these prefix values so we can use them in the space_pred.
            # This way, we get a more fair comparison of how much improvement this module gives.
            self.register_buffer('prefix_values', prefix.clone())
        else:
            #prefix += 0.0000001 # Not zero for Mikkels dataset since the depth camera will give zero depth ...
            # ... at points where the IR projector was occluded. Unprojecting 0 gives nan, and badness ensues.
            self.prefix_values = None

        self.prefix = nn.Parameter(prefix.new_full(prefix.shape, 3*1e-3))

        #num_directions = 2
        #temporal_hidden_size = linear_size//num_directions
        #self.temporal =  nn.GRU(3*num_joints, temporal_hidden_size,
        #                   bidirectional=num_directions==2, batch_first=True, dropout=0) # FIXME: Dropout
        #self.initial_hidden = nn.Parameter(torch.zeros((num_directions, 1, temporal_hidden_size)))
        self.l1 = nn.Sequential(nn.Linear(num_joints-1, linear_size),
                                self.activation,
                                nn.Linear(linear_size, linear_size), # TODO: Try more linear layers
                                self.activation,
                                nn.Dropout(0.1),
                                nn.Linear(linear_size, linear_size),
                                self.activation,
                                nn.Linear(linear_size, num_joints))
        self.sigm = nn.Sigmoid()

    def callback(self, *args, **kwargs):
        forward_cb = CPMFixer.callback(self, *args, **kwargs)
        def cb(*args, **kwargs):
            return forward_cb(*args, **kwargs)

        return cb # even worse

    def loss(self, *args, **kwargs):
        if True:
            global count
            count += 1
            if count >= 100:
                output, target = args
                # To debug training:
                print('')
                #stats = lambda t: tuple(map(torch.Tensor.item, (t.min(), t.mean(), t.max())))
                #print('output:', "%.6f %.6f %.6f" % stats(output[0]))
                #print('target:', "%.6f %.6f %.6f" % stats(target))
                # print('target:', *stats(target))
                print('prefix ', self.prefix)
                print(' error:', ((output[0] - target) ** 2).sum(-1).sqrt().mean(0))
                count %= 100

        return CPMFixer.loss(*args, **kwargs)

    #@timing
    def forward(self, input):
        # input shape [B, S, J, 4], last dim is XYZC
        # input contains joints2d_pred, joints3d_z_pred, confidence.
        if self.prefix_values is not None:
            space_pred = self.unproject_p(torch.cat([input[..., :2],
                                                     self.project_z((input[..., 2] + self.prefix_values).unsqueeze(-1)),
                                                     self.ones.expand(*input.shape[:-1], 1)], -1))[..., :3]
        space_pred_prefix = self.unproject_p(torch.cat([input[..., :2],
                                                 self.project_z((input[..., 2] + self.prefix).unsqueeze(-1)),
                                                 self.ones.expand(*input.shape[:-1], 1)], -1))[..., :3]
        if self.prefix_values is None: # To see effect of prefix
            space_pred = space_pred_prefix.clone().detach()

        if True:
            # No prefix
            space_pred = self.unproject_p(torch.cat([input[..., :2],
                                                 self.project_z((input[..., 2]).unsqueeze(-1)),
                                                 self.ones.expand(*input.shape[:-1], 1)], -1))[..., :3]
        #space_pred = space_pred_prefix.clone().detach()
        #middle = space_pred_prefix.shape[2]//2
        #remember, _ = self.lstm(input_z)
        #remember = remember[:, middle]
        #space_pred_prefix[:, middle]
        print(space_pred_prefix.shape)
        postfix = self.l1((space_pred_prefix[..., 1:, 2:] - space_pred_prefix[..., :1, 2:]).flatten(-2)).unsqueeze(-1)
        #postfix = postfix.reshape(*postfix.shape[:2], -1, 3)
        space_pred_prefix = torch.cat([space_pred_prefix[..., :2],
                                       space_pred_prefix[..., 2:] + postfix], -1)
        # Attempt to refine the space prediction.
        #x = torch.cat([space_pred_prefix, input[..., 3:]], -1) # Append C again.
        #x = x.reshape(*input.shape[:-2], -1)  # Flatten joints and their positions.
        #x = self.activation(self.l1(x))
        #x, _ = self.temporal(x, self.initial_hidden.expand(-1, input.shape[0], -1).contiguous())
        #x = self.l2(self.activation(x))
        # num_directions = 2 if self.lstm.bidirectional else 1
        # x = x.view(*input.shape[:-2], num_directions, self.lstm.hidden_size)
        #x = space_pred + space_pred_prefix

        x = space_pred_prefix

        #if (self.prefix-self.prefix_values).abs().mean().item() > 0.1:
        #    print('Prefix: ', self.prefix, self.prefix_values)
        #print(space_pred.shape, prefixed_space_pred.shape)
        return x, space_pred


class TemporalCPMFixer(nn.Module):
    activation = nn.ReLU()
    def __init__(self, num_joints, project_z, unproject_p, linear_size=1024):
        # Abusing the python language with these args.
        super().__init__()
        self.project_z = project_z
        self.unproject_p = unproject_p
        num_directions = 2
        temporal_hidden_size = linear_size//num_directions
        self.temporal =  nn.GRU(linear_size, temporal_hidden_size,
                           bidirectional=num_directions==2, batch_first=True, dropout=0.1)
        self.l1 = nn.Sequential(nn.Linear(num_joints * 4, linear_size),
                                self.activation,
                                nn.Linear(linear_size, linear_size))
        self.l2 = nn.Linear(temporal_hidden_size*num_directions, num_joints * 3)
        self.register_buffer('ones', torch.ones((1,), dtype=torch.float))
        self.initial_hidden = nn.Parameter(torch.zeros((num_directions, 1, temporal_hidden_size)))

    def callback(self, *args, **kwargs):
        return CPMFixer.callback(self, *args, **kwargs) # bad (indirect inheritence)

    def loss(self, *args, **kwargs):
        return CPMFixer.loss(*args, **kwargs)

    @timing
    def forward(self, input):
        # input shape [B, S, J, 4], last dim is XYZC
        # input contains joints2d_pred, joints3d_z_pred, confidence

        # Project the z coordinate to screen coordinates.
        x = input[..., :3] # Grab XYZ, where XY are image coords and z is space
        # We project z into image coords so we can unproject it all to space.
        x = torch.cat([x[..., :2], self.project_z(x[..., -1:])], -1) # (Can be done in-place?)
        # Now unproject the points into 3d-space.
        # To do this we must first append 1 to the vector to make it homogeneous coordinates.
        # After unprojecting we then remove the 1 from the vector to get x.
        space_pred = self.unproject_p(torch.cat([x, self.ones.expand(*x.shape[:-1], 1)], -1))[..., :3]
        x = torch.cat([space_pred, input[..., 3:]], -1) # Append C again.

        # Attempt to refine the space prediction
        x = x.reshape(*input.shape[:-2], -1) # Flatten joints and their positions
        x = self.activation(self.l1(x))
        x, _ = self.temporal(x, self.initial_hidden.expand(-1, input.shape[0], -1).contiguous())
        x = self.l2(self.activation(x))
        #num_directions = 2 if self.lstm.bidirectional else 1
        #x = x.view(*input.shape[:-2], num_directions, self.lstm.hidden_size)

        x = x.reshape(*input.shape[:-1], -1) + space_pred

        return x, space_pred # We output space_pred to make it easier to see if refinement does anything

class CPMFixer(nn.Module):
    activation = nn.ReLU()
    def __init__(self, num_joints, project_z, unproject_p, linear_size=1024):
        super().__init__()
        self.project_z = project_z
        self.unproject_p = unproject_p
        num_directions = 1
        # self.l1 = nn.Sequential(nn.Linear(num_joints * 4, linear_size),
        #                         self.activation,
        #                         nn.Linear(linear_size, linear_size),
        #                         self.activation,
        #                         nn.LSTM(linear_size, linear_size,
        #                                 batch_first=True, dropout=0.1,
        #                                 bidirectional=num_directions==2)
        #                         )
        self.l1 = nn.Sequential(nn.Linear(num_joints * 4, linear_size),
                                self.activation,
                                nn.Linear(linear_size, linear_size),
                                self.activation,
                                nn.Linear(linear_size, linear_size),
                                )
        self.l2 = nn.Linear(linear_size*num_directions, num_joints*3)
        self.register_buffer('ones', torch.ones((1,), dtype=torch.float))


    def forward(self, input):
        # input shape [B, S, J, 4], last dim is XYZC
        # input contains joints2d_pred, joints3d_z_pred, confidence

        # Project the z coordinate to screen coordinates.
        x = input[..., :3] # Grab XYZ, where XY are image coords and z is space
        # We project z into image coords so we can unproject it all to space.
        x = torch.cat([x[..., :2], self.project_z(x[..., -1:])], -1)
        # Now unproject the points into 3d-space.
        # To do this we must first append 1 to the vector to make it homogeneous coordinates.
        # After unprojecting we then remove the 1 from the vector to get x.
        space_pred = self.unproject_p(torch.cat([x, self.ones.expand(*x.shape[:-1], 1)], -1))[..., :3]
        x = torch.cat([space_pred, input[..., 3:]], -1) # Append C again.

        # Attempt to refine the space prediction
        x = x.reshape(*input.shape[:-2], -1) # Flatten joints and their positions
        #x, _ = self.l1(x)
        x = self.l1(x)
        x = self.l2(self.activation(x))
        #num_directions = 2 if self.lstm.bidirectional else 1
        #x = x.view(*input.shape[:-2], num_directions, self.lstm.hidden_size)

        x = x.reshape(*input.shape[:-1], -1) + space_pred

        return x, space_pred # We output space_pred to make it easier to see if refinement does anything

    @staticmethod
    def loss(output, target):
        # Sum over joint position values, mean over joints and then sequence.
        return ((output[0] - target) ** 2).sum(-1).mean(-1).mean(-1)

    def callback(self, pd_train, pd_test, project_p):
        def cb():
            if False:
                pd_example = pd_train
            else:
                pd_example = pd_test

            # example += pd_example._cache_size//2
            example = 0 #245 + 20
            input_ex, target_ex = pd_example[example]  # Retreive sample.
            skip_samples = input_ex.shape[0]//10
            skip_samples = max(min(len(input_ex)-1, skip_samples), 1) # Is this needed?
            result = pd_example._cache[example - pd_example._cache_ptr]  # Middle batch, last element in seq.
            samples = list(range(len(result['depth']))[::skip_samples])
            self.eval()
            output_ex = self(input_ex[None].to(device, dtype)) # FIXME: Get device proper.
            output_ex = tuple(map(lambda o: o[0], output_ex)) # Remove the batch dim
            # result = pd_example._cache[len(pd_example)//seq_len//2, seq_len-1]
            pd_train.shuffle()

            for frame in samples:
                sample = result[frame]

                normalizer = 1
                depth = sample['depth']
                if np.issubdtype(depth.dtype, np.integer):
                    normalizer = np.iinfo(depth.dtype).max
                depth = depth.astype(np.float32) / normalizer * pd_example._renderer.far
                ax = plt.subplot()
                img = ax.imshow(depth[0].T, cmap='gray')
                plt.colorbar(img).set_label('depth [m]')
                ax.set_xlabel('frame ' + str(frame))
                name, startframe = pd_example.get_sample_id(example)
                ax.set_title(name + ' starting frame ' + str(startframe))

                joints2d = to_numpy(sample['joints2d'][..., :2])
                pred2d = to_numpy(input_ex[frame, ..., :2])
                refined2d = to_numpy(project_p(torch.cat([output_ex[0][frame], output_ex[0].new_ones(output_ex[0][frame].shape[:-1] + (1,))], -1))[..., :2])
                for joint in range(len(joints2d)):
                    gt = joints2d[joint]
                    pred = pred2d[joint]
                    refined = refined2d[joint]
                    ax.add_patch(Circle(gt, 0.4, color='g'))
                    ax.add_patch(Circle(pred, 0.3, color='r'))
                    ax.add_patch(Circle(refined, 0.2, color='y'))
                    ax.text(*gt, str(joint), fontsize=6)
                plt.show()

                ax = plt.subplot(projection='3d')
                #plt.gca().invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                #ax.invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                ax.invert_yaxis()
                ax.invert_zaxis()

                ax.axis('equal')
                ax.set_title('')
                ax.set_xlabel('x (frame ' + str(frame) + ')')
                ax.set_ylabel('y')
                ax.set_zlabel('z')

                joints3d, pred3d, refined3d = map(to_numpy, (sample['joints3d'][..., :3],
                                                             output_ex[1][frame, ..., :3],
                                                              output_ex[0][frame, ..., :3]))
                print('Frame', str(frame))
                print('CPM error           | refinement error')
                print(np.concatenate([
                    np.round(((joints3d - pred3d) * 1e3)),
                    np.round(((joints3d - refined3d) * 1e3))], -1))

                # The cpm error is dominated by errors in z.
                print('root mean (joints) sum square        cpm error:',
                      np.sqrt(((joints3d-pred3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) sum square refinement error:',
                      np.sqrt(((joints3d-refined3d)**2).sum(-1).mean(-1)))
                print('root mean (joints) square z        cpm error:',
                      np.sqrt(((joints3d - pred3d) ** 2)[..., 2].mean(-1)))
                print('root mean (joints) square z refinement error:',
                      np.sqrt(((joints3d - refined3d) ** 2)[..., 2].mean(-1)))
                print(' ')

                for joint in range(len(joints3d)):
                    gt = joints3d[joint]
                    pred = pred3d[joint]
                    refined = refined3d[joint]
                    #ax.add_patch(Circle(gt, 0.3, color='g'))
                    #ax.add_patch(Circle(pred, 0.3, color='r'))
                    #ax.add_patch(Circle(refined, 0.3, color='y'))
                    ax.scatter(gt[..., 0], gt[..., 1], gt[..., 2], s=0.5, color='g')
                    ax.scatter(pred[..., 0], pred[..., 1], pred[..., 2], s=0.5, color='r')
                    ax.scatter(refined[..., 0], refined[..., 1], refined[..., 2], s=0.5, color='y')
                    ax.text(*gt, str(joint), fontsize=6)
                    # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                    #ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
                    #ax.add_patch(Arrow(*gt, refined[0] - gt[0], refined[1] - gt[1], color='w'))
                plt.show()

                for pred_heatmap in result[CPMCache.name[0]][:1]:
                    columns = 6
                    num_subplots = len(joints2d)
                    rows = num_subplots // columns + (num_subplots % columns != 0)
                    # Plot of just heatmaps # TODO: add colorbars, if it can be done in a still tight format.
                    for i in range(rows):
                        for j in range(columns):
                            idx = i * columns + j
                            if idx >= num_subplots:
                                break
                            ax = plt.subplot(rows, columns, idx + 1)
                            # plt.imshow(pred_heatmap[idx].T, vmin=0, vmax=1)
                            plt.imshow(pred_heatmap[idx].T)
                            ax.axes.get_xaxis().set_ticks([])
                            ax.axes.get_yaxis().set_ticks([])
                            ax.set_xlabel(str(idx))
                    plt.show(block=False)
                #print(result[CPMCache.name[0]].shape)

            #cpmc.post_transform_cpm_extension_keypoints(hmc)
        return cb

def cpmfixer_statistics(test_set : CachedSMLDataset, cpmfixer : CPMFixer, cpmfixer_path,
                   visualize=False, shuffle=False):
    from tqdm import tqdm
    resume = torch.load(cpmfixer_path)
    cpmfixer.load_state_dict(resume['model_state_dict'])
    resume = resume['trainer_state']
    from trainer import Trainer
    Trainer.plot_training(resume['train_losses'], resume['test_losses'])


    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    cpmfixer = cpmfixer.to(device, dtype, non_blocking=True)

    if shuffle:
        test_set.shuffle() # To give more varied visualizations.
    test_dataset = torch.utils.data.DataLoader(test_set, batch_size=30 if not visualize else 1, pin_memory=True)

    errors = []
    with torch.no_grad():
        cpmfixer.eval()
        for idx, sample in enumerate(tqdm(test_dataset)):
            if idx != 915: # 585, 915
                continue
            input, joints3d = map(lambda t: t.to(device, dtype, non_blocking=True), sample)
            refined3d, pred3d = cpmfixer(input)

            error = ( (refined3d - joints3d) ** 2 ).sum(-1).sqrt() * 1e3 # Euclidean distance to GT [B, S, J]
            errors.append(error.to('cpu'))

            if visualize:
                for frameoffset in range(input.shape[1]):
                    ax = plt.subplot(projection='3d')
                    #ax.view_init(30)
                    ax.invert_zaxis()
                    ax.axis('equal')
                    ax.set_xlabel('x')
                    ax.set_ylabel('y (frame ' + str(idx) + ')') # Since we don't use sequences
                    ax.set_zlabel('z')
                    # plt.gca().invert_xaxis() # We must invert 2 axes, or chirality is wrong.
                    plt.gca().invert_yaxis()

                    j3d = to_numpy(joints3d[0, frameoffset])
                    p3d = to_numpy(pred3d[0, frameoffset])
                    r3d = to_numpy(refined3d[0, frameoffset])
                    for joint in range(len(j3d)):
                        gt = j3d[joint]
                        pred = p3d[joint]
                        refined = r3d[joint]
                        # ax.add_patch(Circle(gt, 0.3, color='g'))
                        # ax.add_patch(Circle(pred, 0.3, color='r'))
                        # ax.add_patch(Circle(refined, 0.3, color='y'))
                        ax.plot(np.stack([refined[..., 0], gt[..., 0]], -1),
                                np.stack([refined[..., 1], gt[..., 1]], -1),
                                np.stack([refined[..., 2], gt[..., 2]], -1), '-', linewidth=0.5, color='grey')
                        ax.text(*gt, str(joint), fontsize=6)
                        a = ax.scatter(gt[..., 0], gt[..., 1], gt[..., 2], s=0.5, color='g')
                        #ax.scatter(pred[..., 0], pred[..., 1], pred[..., 2], s=0.5, color='r')
                        b = ax.scatter(refined[..., 0], refined[..., 1], refined[..., 2], s=0.5, color='y')
                        # ax.add_patch(ConnectionPatch(gt, pred, coordsA='data'))
                        # ax.add_patch(Arrow(*gt, pred[0] - gt[0], pred[1] - gt[1], color='w'))
                        # ax.add_patch(Arrow(*gt, refined[0] - gt[0], refined[1] - gt[1], color='w'))
                    #plt.savefig(os.path.join('.', 'figures', 'cpmfixer_visualize0.eps'), bbox_inches='tight')
                    ax.legend((a, b), ['Ground truth', 'Depth Fixer'])
                    plt.savefig(os.path.join('.', 'figures', 'cpmfixer_visualize0.pdf'), bbox_inches='tight')
                    plt.show()

                    #ax = plt.subplot(projection='3d')
                    ax = plt.subplot()
                    #ax.view_init(30)
                    ax.axis('equal')
                    ax.set_xlabel('y (frame ' + str(idx) + ')')
                    ax.set_ylabel('z')
                    plt.gca().invert_yaxis()
                    plt.gca().invert_xaxis()

                    j3d = to_numpy(joints3d[0, frameoffset])[..., 1:]
                    p3d = to_numpy(pred3d[0, frameoffset])[..., 1:]
                    r3d = to_numpy(refined3d[0, frameoffset])[..., 1:]
                    for joint in range(len(j3d)):
                        gt = j3d[joint]
                        pred = p3d[joint]
                        refined = r3d[joint]
                        #ax.add_patch(Arrow(*gt, pred[..., 0] - gt[..., 0], pred[..., 1] - gt[..., 1], color='k', width=0.5))
                        ax.plot(np.stack([refined[..., 0], gt[..., 0]], -1),
                                np.stack([refined[..., 1], gt[..., 1]], -1), '-', linewidth=0.5, color='grey')
                        ax.plot(np.stack([refined[..., 0], pred[..., 0]], -1),
                                np.stack([refined[..., 1], pred[..., 1]], -1), '-', linewidth=0.3, color='grey')
                        ax.text(*gt, str(joint), fontsize=8)
                        a = ax.scatter(gt[..., 0], gt[..., 1], s=0.5, color='g')
                        b = ax.scatter(pred[..., 0], pred[..., 1], s=0.5, color='r')
                        c = ax.scatter(refined[..., 0], refined[..., 1], s=0.5, color='y')
                    ax.legend((a, c, b), ['Ground truth', 'Depth Fixer', 'No depth fix'])
                    plt.grid()
                    #plt.savefig(os.path.join('.', 'figures', 'cpmfixer_visualize1.eps'), bbox_inches='tight')
                    plt.savefig(os.path.join('.', 'figures', 'cpmfixer_visualize1.pdf'), bbox_inches='tight')

                    plt.show()
                exit()

    errors = torch.cat(errors, 0)
    print('mean distance', errors.mean())
    jointerr = errors.mean(1).mean(0)
    timeerr = errors.mean(2).mean(0)
    print('mean distance over joints', jointerr)
    print('mean distance over time', timeerr)

    plt.bar(x=range(len(jointerr)), height=to_numpy(jointerr))
    plt.title('Joint error (AJPE)')
    plt.ylabel('AJPE [mm]')
    plt.xlabel('Joint #')

    plt.savefig(os.path.join('.', 'figures', 'cpmfixer_jointerr.eps'), bbox_inches='tight')
    plt.show()
    plt.title('Time error')
    plt.bar(x=range(len(timeerr)), height=to_numpy(timeerr))
    plt.show()

    if True: # visualize temporal error over the sequence
        x = to_numpy(errors[:, 0, :].mean(-1))
        N = x.shape[0]
        plt.plot(x)
        plt.ylabel('Error [mm]')
        plt.xlabel('Frame')
        ylimits = list(plt.ylim())
        plt.ylim([0, ylimits[1]])
        plt.savefig(os.path.join('.', 'figures', 'cpmfixer_jointerr_time.eps'), bbox_inches='tight')
        plt.title('AJPE over time (Seq. 12)')
        plt.show()

    pckh = errors < 2.64 * 10 # 26.4 mm is the average head segment lengths
    pckh_all = pckh.all(-1)
    pckh = pckh.to(torch.float32)
    pckh_all = pckh_all.to(torch.float32)

    jointerr = pckh.mean(1).mean(0)
    print('PCKh 1.0 ', pckh.mean(), ' all: ', pckh_all.mean())
    print('PCKh 1.0 over joints', jointerr)

    plt.bar(x=range(len(jointerr)), height=to_numpy(jointerr))
    plt.ylim([0, 1])
    ax = plt.gca()
    vals = ax.get_yticks()
    ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
    plt.title('PCKh 1.0')
    plt.xlabel('Joint #')
    plt.savefig(os.path.join('.', 'figures', 'cpmfixer_pckh1.eps'), bbox_inches='tight')
    plt.show()

    if False: # visualize temporal error over the sequence
        plt.plot(to_numpy(pckh[:, 0, :].mean(-1)))
        plt.ylabel('PCKh 1.0 (100% = all 25 joints)')
        plt.xlabel('Frame')
        plt.ylim([0, 1])
        ax = plt.gca()
        vals = ax.get_yticks()
        ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
        plt.title('PCKh 1.0 over time (Seq. 12)')
        plt.savefig(os.path.join('.', 'figures', 'cpmfixer_pckh1_time.eps'), bbox_inches='tight')
        plt.show()

    pckh = errors < 2.64 * 10 * 2 # 26.4 mm is the average head segment lengths
    pckh_all = pckh.all(-1)
    pckh = pckh.to(torch.float32)
    pckh_all = pckh_all.to(torch.float32)

    jointerr = pckh.mean(1).mean(0)
    print('PCKh 2.0 ', pckh.mean(), ' all: ', pckh_all.mean())
    print('PCKh 2.0 over joints', jointerr)

    ax = plt.bar(x=range(len(jointerr)), height=to_numpy(jointerr))
    plt.ylim([0, 1])
    ax = plt.gca()
    vals = ax.get_yticks()
    ax.set_yticklabels(['{:,.0%}'.format(x) for x in vals])
    plt.title('PCKh 2.0')
    plt.xlabel('Joint #')
    plt.savefig(os.path.join('.', 'figures', 'cpmfixer_pckh2.eps'), bbox_inches='tight')
    plt.show()


    exit()


def train_cpmfixer_minirgbd():
    determinism(speed=False) # FIXME: TRAIN WITH THIS FALSE.
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')

    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')

    # Load model.
    model = SML(nose_vertex=True).to(device, dtype, non_blocking=True)
    # TODO: Pre-projection

    seq_len = 1  # 32
    downsample_factor = 1  # To subsample the dataset - more temporal space between inputs.
    skip_factor = 1  # To reduce the size of the dataset. If we skip, there will be less overlap between sequences.

    renderer = Renderer(model, camera_params, visible=False, decoration=False)

    sigma = 7
    background_heatmap = False
    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma, stride=CPM.stride,
                          background_heatmap=background_heatmap).to(device, dtype, non_blocking=True)

    cpm = CPM(num_heatmaps=model.num_joints + hmc._background_heatmap)
    cpm.load_state_dict(torch.load('./trainednetworks/CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7.pt')['model_state_dict'])
    cpm.to(device, dtype, non_blocking=True)
    cpmc = CPMCache(cpm, renderer.far, batch_size=30)
    cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

    train_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=skip_factor,
                                startseq=0, endseq=10, random_betas=False, random_rotation=True)

    pd_train = CachedSMLDataset(renderer, model, [train_set],
                                cache_size=1024,
                                augment_position=True, transforms=cache_transformers,
                                post_transform=cpmc.post_transform_cpm_extension_keypoints(hmc, target2d=False))

    # test_skip_factor = skip_factor//2 if skip_factor!=1 else skip_factor # FIXME: Remove this
    test_skip_factor = 1
    # FIXME: Change back To test set!:
    test_set = MINIRGBDDataset(seq_len, downsample_factor=downsample_factor, skip_factor=test_skip_factor,
                               startseq=11, endseq=12, random_betas=False, random_rotation=False)
    pd_test = CachedSMLDataset(renderer, model, [test_set],
                               cache_size=1024,
                               augment_position=False, transforms=cache_transformers,
                               post_transform=cpmc.post_transform_cpm_extension_keypoints(hmc, target2d=False))
    print('train set:', train_set._sequence_names, ' Length (in sequences):', len(pd_train))
    print('test set', test_set._sequence_names, ' Length (in sequences):', len(pd_test))

    from trainer import Trainer
    # loss = torch.nn.SmoothL1Loss(reduction='none')

    # regressor = TemporalCPMFixer(model.num_joints, unproject_p=renderer.unproject_points, project_z=renderer.project_z)
    regressor = CPMDepthFixer(model.num_joints, unproject_p=renderer.unproject_points, project_z=renderer.project_z)
    # regressor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
    regressor = regressor.to(device, dtype)

    callback = regressor.callback(pd_train, pd_test,
                                 renderer.project_points)  # cpm.callback(pd_train, pd_test, hmc, device, dtype)
    cpmfixer_statistics(pd_test, regressor,
                        './trainednetworks/CPMFIXER(CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7)-5linear.pt',
                        visualize=True)
    exit()
    callback()  # Shuffles dataset!

    trainer = Trainer(pd_train, pd_test,
                      loss_fn=CPMFixer.loss  # TODO: Change to regressor.loss.
                      )

    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    batch_size = min(512, len(pd_train))  # 32 #8*2*8*2*4
    optimizer = torch.optim.Adam(regressor.parameters(), lr=0.001)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1,
                                                           verbose=True)


    train_result = trainer.resume_training(regressor, optimizer, scheduler, callback=callback, batch_size=batch_size,
                                           load_optim_state_dict=False, max_epochs=150, patience=8,
                                           max_batch_size=batch_size,
                                           filename='./trainednetworks/CPMFIXER(CPM7-SYNTHETIC(NOSE)-FLIP-MIKKEL-SSL-7)-5linear2.pt')

def train_cpmfixer_mikkel(dtype, device):
    determinism(speed=True)
    camera_params = CameraParameters(640 // 2, 480 // 2, preset='MIKKEL')
    num_joints = 19

    # Load model.

    seq_len = 1  # 32
    downsample_factor = 1  # To subsample the dataset - more temporal space between inputs.
    skip_factor = 4  # To reduce the size of the dataset. If we skip, there will be less overlap between sequences.
    print('Skip factor', skip_factor)
    sigma = 7
    background_heatmap = False
    hmc = HeatMapperCache(*camera_params.resolution(), sigma=sigma, stride=CPM.stride,
                          background_heatmap=background_heatmap).to(device, dtype, non_blocking=True).to(device, dtype)

    from dataset.cachedMIKKELdataset import cached_mikkel_dataset
    from dataset.mikkel import MikkelSequence
    cpm = CPM(num_heatmaps=MikkelSequence.num_joints + background_heatmap)
    cpm.load_state_dict(torch.load('./trainednetworks/CPM7-REAL-MIKKEL-SSL-1.pt')['model_state_dict'])
    cpm = cpm.to(device=device, dtype=dtype)
    pd_train, pd_test, _ = cached_mikkel_dataset(post_transform=None, cpm = cpm, background_heatmap=background_heatmap, sigma=sigma,
                                                 seq_len=seq_len, downsample_factor=downsample_factor,
                                                 skip_factor=skip_factor, cache_size=512 * 4)

    # Very bad code LOL (static code analysis fails completely of course):
    from argparse import Namespace
    far = pd_train._renderer.far
    near = 0.1 # Any low value will do since we are not actually rendering with this.
    viewport = camera_params.viewport()
    projection = camera_params.perspective(near, far)
    renderer = Namespace(far=pd_train._renderer.far,
                         project_points=lambda points: camera_params.project_points(projection, viewport, points),
                         unproject_points=lambda points: camera_params.unproject_points(projection, viewport, points),
                         project_z=lambda z: camera_params.project_z(projection, viewport, z))

    pd_train._transforms[-2] = pd_train._transforms[-2].to(device, dtype) # Heatmapper Cache
    pd_test._transforms[-2] = pd_test._transforms[-2].to(device, dtype)
    pd_train._post_transform = pd_train._transforms[-1].post_transform_cpm_extension_keypoints(pd_train._transforms[-2], target2d=False) # CPM cache
    pd_test._post_transform = pd_test._transforms[-1].post_transform_cpm_extension_keypoints(pd_test._transforms[-2], target2d=False)

    #cpm.load_state_dict(torch.load('./trainednetworks/CPM7-REAL-MIKKEL-SSL-1.pt')['model_state_dict'])
    #cpm.to(device, dtype, non_blocking=True)
    #cpmc = CPMCache(cpm, renderer.far, batch_size=30)
    #cache_transformers = [hmc, cpmc, CPMJoints2dCache(hmc)]

    from trainer import Trainer
    # loss = torch.nn.SmoothL1Loss(reduction='none')

    # regresor = TemporalCPMFixer(model.num_joints, unproject_p=renderer.unproject_points, project_z=renderer.project_z)
    regresor = CPMDepthFixer(num_joints, unproject_p=renderer.unproject_points, project_z=renderer.project_z)
    # regresor.load_state_dict(torch.load('bestmodel.pt')['model_state_dict'])
    regresor = regresor.to(device, dtype)

    callback = regresor.callback(pd_train, pd_test,
                                 renderer.project_points)  # cpm.callback(pd_train, pd_test, hmc, device, dtype)
    cpmfixer_statistics(pd_test, regresor, './trainednetworks/CPMFIXER(CPM7-REAL-MIKKEL-SSL-1)-5linear2.pt')
    exit()
    callback()  # Shuffles dataset!
    trainer = Trainer(pd_train, pd_test,
                      loss_fn=regresor.loss  # TODO: Change to regressor.loss.
                      )

    # FIXME: Check out https://arxiv.org/pdf/1505.00487.pdf
    batch_size = min(128, len(pd_train))  # 32 #8*2*8*2*4
    optimizer = torch.optim.Adam(regresor.parameters(), lr=0.01)
    #scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, threshold=0.1,
    #                                                       verbose=True)
    scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.8)

    train_result = trainer.resume_training(regresor, optimizer, scheduler, callback=callback, batch_size=batch_size,
                                               load_optim_state_dict=False, max_epochs=150, patience=8,
                                               max_batch_size=batch_size,
                                               filename='./trainednetworks/CPMFIXER(CPM7-REAL-MIKKEL-SSL-1)-5linear.pt')


if __name__ == "__main__":
    dtype = torch.float
    if torch.cuda.is_available():
        device = torch.device('cuda') # Accessed in the callback ...
    else:
        device = torch.device('cpu')

    #train_cpmfixer_mikkel(dtype, device)
    train_cpmfixer_minirgbd()