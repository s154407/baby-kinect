import torch
from torch import nn
import torch.nn.functional as F

from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.optim import SGD

from utils import timing


class HeatMapper(nn.Module):
    def __init__(self, width, height, sigma, stride=1, background_heatmap=True, dtype=torch.float):
        super().__init__()
        # It would be relatively simple to allow different x and y stride, like in Conv2d.
        # In fact this could be made compatible with any input/output from a Conv2D
        if not (width % stride == 0 and height % stride == 0):
            raise ValueError('Width and height must be multiple of stride.')
        self._width = width // stride
        self._height = height // stride
        self._sigma = sigma
        self._stride = stride
        self._background_heatmap = background_heatmap

        xrange = torch.arange(0, self._width, dtype=dtype) * self._stride
        yrange = torch.arange(0, self._height, dtype=dtype) * self._stride
        xv, yv = torch.meshgrid([xrange, yrange])
        # FIXME?: Oops, the convention is [C, H, W], channels, height, width. We used channels, width, height.
        # evalpoints is shape [width, height, 2] and evalpoints[x,y] = [x,y] * stride
        #self.evalpoints = torch.stack([xv, yv], -1)
        self.register_buffer('evalpoints', torch.stack([xv, yv], -1))

    def forward(self, centers):
        # Centers should be shape [B, C, XY]
        ## Normalized PDF, where maximum = 1.
        centers = centers.reshape(*centers.shape[:-1], 1, 1, 2)
        heatmap = torch.sum((centers - self.evalpoints) ** 2, -1)
        #heatmap = torch.sum((centers[:, :, None, None, :] - self._evalpoints) ** 2, -1)
        heatmap /= - (2 * self._sigma ** 2)
        heatmap.exp_()
        if self._background_heatmap:
            backgroundheat = 1 - heatmap.max(-3, keepdim=True)[0]  # expm1 can be used also.
            heatmap = torch.cat([heatmap, backgroundheat], -3)

        # (width * height, batch_size, n_keypoints) confidence to -> (batch_size, n_keypoints, width, height)
        #print(confmap.shape, centers.shape)
        #confmap = confmap.permute((1, 2, 0)).reshape(centers.shape[0], -1, self._width, self._height)
        return heatmap

    def downscale_img(self, img, mode='area'):
        return F.interpolate(img, scale_factor=1/self._stride, mode=mode)

    def get_prediction_value(self, img, centers):
        # img of shape     [B, C, W, H]
        # centers of shape [B, J, 2]
        # returns values of shape [B, J, C]

        # It samples the image at the points specified by centers.
        batch = torch.arange(img.shape[0])
        # FIXME: Check bounds: Test correctness
        centers = centers.round().to(batch.dtype)
        centers[centers<0] = 0
        centers[centers[..., 0] >= img.shape[2]] = img.shape[2] - 1
        centers[centers[..., 1] >= img.shape[3]] = img.shape[3] - 1
        values = torch.stack([img[b, :, centers[b, :, 0], centers[b, :, 1]].t() for b in range(img.shape[0])])
        return values

    def get_prediction(self, heatmaps, tune_centers=True, maxiter=20):
        # Only works for the real heatmaps and not the BG! (Since there is no joint to predict for BG)
        # TODO: Check where we actually need detach in this function.
        heatmaps = heatmaps.detach()
        # Heatmaps shape should be [B, C, X, Y]
        ground_truth_heatmaps = heatmaps
        if self._background_heatmap:
            # Last channel, the background, does not matter for the prediction of keypoints.
            heatmaps = heatmaps[:, :-1]

        # Commence ARGMAX2D
        values, maxYpos = heatmaps.max(-1)  # Get max in Y direction
        # We get max in Y direction. values is now a row containing the largest Y-values.
        values, maxXpos = values.max(-1, keepdim=True)  # Get the X position of the largest Y-value.
        maxYpos = torch.gather(maxYpos, -1, maxXpos)  # Now get the Y-position that this X had.
        # Low values (<0.1) indicates the joint prediction is wrong.
        #  If _background_confmap we can compare to the background certainty.

        values = values[..., 0]
        centers = torch.cat([maxXpos, maxYpos], -1).to(dtype=heatmaps.dtype) * self._stride

        if not tune_centers:
            # Return a simple argmax.
            # Values can be underestimated by 1/sqrt(2).
            # TODO if self._background_confmap is true compare with this.
            return centers, values

        # Now we will create heatmaps using these indices
        # Then we adjust the parameters so they match the given heatmaps best.
        pcenters = nn.Parameter(centers.clone())

        # Optimizer and scheduler could be arguments.
        optimizer = SGD([pcenters], lr=30 * 368)
        scheduler = ReduceLROnPlateau(optimizer,
                                      factor=0.1, patience=5,
                                      threshold=0.1, verbose=False)
        with torch.enable_grad():
            for i in range(maxiter):
                # There is probably a somewhat closed form expression for the iterates...
                oldpcenter = pcenters.data.clone().detach()  # detach not needed?
                optimizer.zero_grad()
                heatmaps = self.forward(pcenters)  # "Recreate" the ground_truth_heatmaps with our estimated params.
                # Refine our estimations.
                #error = ((ground_truth_heatmaps - heatmaps) ** 2).mean([-1, -2]).sum(-1).sum()
                # We mean over all image dimensions (width, height, channel) to make the learning rate
                # not have to depend on those. Then sum over batch, because each batch is independent.
                # TODO: How to "correctly" reduce? Target is the convergence should be independent of input shapes.
                # To mean or to sum over joints? Answer: sum since they're independent. (almost - there may be BG)
                error = ((ground_truth_heatmaps.detach() - heatmaps) ** 2).mean([-1, -2]).sum(-1)
                error.sum().backward()
                optimizer.step()  # modifies pcenters
                scheduler.step(error.max())  # Scheduler maybe isn't needed, but it doesn't hurt.
                #if (pcenters.data - oldpcenter).max() < 0.01:
                    # If no pcenters moved more than a 100th of a pixel.
                if (pcenters.data - oldpcenter).abs().mean() < 0.01: # More robust.
                    #print('done', i,
                    #      (pcenters.data - oldpcenter).max(), # = 0.0388
                    #      (pcenters.data - oldpcenter).abs().mean()) # = 0.0065
                    break
        pcenters = pcenters.data
        # Check if both x and y have moved at most one stride.
        # (The theoretical value is self._stride/2, we give some extra room for error)
        if True:
            goodlimit = self._stride * 2 / 3
            goodestimates = torch.all((pcenters - centers).abs() <= goodlimit, -1)
            #print((1-goodestimates).sum().item())
            # Use the refined estimates if they give subpixel change.
            centers = torch.where(goodestimates[..., None].expand(*centers.shape),
                                  pcenters,
                                  centers)  # quick fix


        return centers, values


if __name__ == "__main__":


    import matplotlib
    # matplotlib.use("TkAgg")
    import matplotlib.pyplot as plt
    from render.renderer import Renderer
    from render.cameraparameters import CameraParameters
    from dataset.cachedSMLdataset import CachedSMLDataset
    from dataset.minirgbd import MINIRGBDDataset
    import cv2
    from SML import SML
    import numpy as np


    device = torch.device('cpu')
    dtype = torch.float

    model = SML(nose_vertex=False).to(device, dtype)
    scale = 1
    cp = CameraParameters(width=640 * scale, height=480 * scale, preset='MINIRGBD')
    render = Renderer(model, cp, visible=False, decoration=False)
    minirgbd = MINIRGBDDataset(seq_len=2)

    pics = []

    # cd = CachedSMLDataset(render,model,[minirgbd], cache_size=1, render_batch_size=1, noise=False)

    pose, trans, beta, _ = minirgbd[0]
    beta = torch.tensor(beta[None]).to(device, dtype).expand(2, -1)
    pose = torch.tensor(pose).to(device, dtype)
    trans = torch.tensor(trans).to(device, dtype)

    # simplify true
    v, Jtr = model(beta, pose, trans, simplify=True)

    # beta er ikke nul
    pose[...] = 0
    trans[:, 0] = 0
    trans[:, 1] = -0.26
    trans[:, 2:] = 0.66 * -1

    v, Jtr = model(beta, pose, trans)
    v[..., 2] *= -1
    Jtr[..., 2] *= -1

    import matplotlib.pyplot as plt
    # The following shows that with a maxiter of 20, the tune_centers
    # mean that we get an error of 0.0062 pixels at most instead of 4 = stride/2.
    hm = HeatMapper(cp.width, cp.height, 7, stride=1).to(dtype)

    J2d = render.project_points(Jtr[:1])
    J2d = J2d[0, ..., :2]
    print(J2d.shape)
    heatmaps = hm(J2d)

    from cpm import transparent_cmap
    mycmap = transparent_cmap()
    from matplotlib.patches import Circle, Arrow

    minidepth = hm.downscale_img(torch.as_tensor(np.copy(render.render(v))[0, None, None, ..., 0]), mode='nearest')[0, 0].numpy()
    # Plot of heatmaps overlayed onto the input depth image.
    num_subplots = heatmaps.shape[0]
    print(J2d.shape)
    from utils import to_numpy
    rows = 5
    jointnames = "global", "leftThigh", "rightThigh", "spine", "leftCalf", "rightCalf", "spine1", "leftFoot", "rightFoot", "spine2", "leftToes", "rightToes", "neck", "leftShoulder", "rightShoulder", "head", "leftUpperArm", "rightUpperArm", "leftForeArm", "rightForeArm", "leftHand", "rightHand", "leftFingers", "rightFingers", "background"
    columns = 5
    for i in range(rows):
        for j in range(columns):
            idx = i * columns + j
            if idx >= num_subplots:
                break

            ax = plt.subplot(rows, columns, idx + 1)
            ax.imshow(minidepth, cmap='gray_r', interpolation='bilinear')
            ax.imshow(to_numpy(heatmaps[idx]).T, cmap=mycmap, vmin=0, vmax=1, interpolation='none')
            # ax.add_patch(Circle(to_numpy(joints2d[0, 0, idx]) / cpm.stride, 0.2, color='g'))
            #ax.add_patch(Circle(to_numpy(J2d[idx]), 0.2, color='r'))
            ax.axes.get_xaxis().set_ticks([])
            ax.axes.get_yaxis().set_ticks([])
            jointname = str(idx) if jointnames is None else jointnames[idx]
            ax.set_xlabel(jointname)  # , fontsize=10
            ax.invert_yaxis()

    plt.gcf().set_figwidth(8)
    plt.gcf().set_figheight(6)
    plt.tight_layout()
    import os
    plt.savefig(os.path.join('.', 'figures', 'heatmaps.png'), bbox_inches='tight')
    plt.show(block=False)

    print(heatmaps.shape)
    exit()

    xrange = torch.arange(0, 14, dtype=torch.float)
    yrange = torch.arange(0, 14, dtype=torch.float)
    xv, yv = torch.meshgrid([xrange, yrange])
    evalpoints = torch.stack([xv, yv], -1).reshape(-1, 1, 2)

    heatmap = hm(evalpoints)
    pos, val = hm.get_prediction(heatmap, True, maxiter=50)
    #print((pos-evalpoints.reshape(-1, 1, 2)).max(-1)[0])
    print((pos-evalpoints).max(-1)[0].max())

    heatmap = HeatMapper(368, 368, 7, stride=1)(evalpoints)
    print(hm.get_prediction_value(heatmap, pos))
    #print(heatmap[0, ..., 0].max(0))

    #heatmap = heatmap.permute((0, 3, 2, 1))
    #plt.imshow(heatmap[0, ..., 0])
    #plt.show()

    #plt.imshow(heatmap[0, ..., 1])
    #plt.show()
    pass