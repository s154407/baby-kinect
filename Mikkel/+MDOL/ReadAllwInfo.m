function obj = ReadAllwInfo(fid)
n = fread(fid,1,'int32');
obj = -1;
if n ~= 0
    n = n*fread(fid,1,'int32');
    type = MDOL.ReadString(fid);
    if strcmpi(type,'System.String')
        obj = cell(n,1);
        for i = 1:n
            obj{i} = MDOL.ReadString(fid);
        end
    else
        if strcmpi(type,'System.Byte')
            obj = fread(fid,n,'uint8');
        elseif strcmpi(type,'System.Boolean')
            obj = fread(fid,n,'locical');
        elseif strcmpi(type,'System.Int64')
            obj = fread(fid,n,'int64');
        elseif strcmpi(type,'System.Double')
            obj = fread(fid,n,'double');
        elseif strcmpi(type,'System.Single')
            obj = fread(fid,n,'single');
        end
    end
end
end