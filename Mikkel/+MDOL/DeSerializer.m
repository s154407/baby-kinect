classdef DeSerializer
    properties(Access = public)
        DepthTimeStamps
    end
    properties(Access = private)
        fid
        Offsets
        Lengths
        GlobalOffset
        File
    end
    methods
        function obj = DeSerializer(File)
            obj.File = File;
            obj.fid = fopen(File);
            obj.DepthTimeStamps = MDOL.ReadAllwInfo(obj.fid);
            obj.Offsets = MDOL.ReadAllwInfo(obj.fid);
            obj.Lengths = MDOL.ReadAllwInfo(obj.fid);
            obj.GlobalOffset = ftell(obj.fid);
        end
        function Close(obj)
            fclose(obj.fid);
        end
        function [Im,TimeStamp] = GetImage(obj,i,filetype)
            Depth = false;
            if nargin == 2
                [~,filename,~] = fileparts(obj.File);
                switch filename
                    case {'Color','DepthColor'}
                        filetype = 'jpg';
                    case 'Depth'
                        Depth = true;
                        filetype = 'png';
                    otherwise
                        error('Filetype could not be predicted');
                end
            end
            [data,TimeStamp] = obj.GetData(i);
            tmp = fopen([tempdir '/deleteme.' filetype],'w');
            fwrite(tmp,data,'uint8');
            fclose(tmp);
            Im = imread([tempdir '/deleteme.' filetype]);
%             if Depth
%                Im = obj.RGB2Depth(Im);
%             end
        end
        function [data,TimeStamp] = GetData(obj,i)
            fseek(obj.fid,obj.GlobalOffset + obj.Offsets(i),-1);
            TimeStamp = obj.DepthTimeStamps(i);
            data = uint8(fread(obj.fid,obj.Lengths(i),'uint8'));
        end
    end
    methods (Access = private)
        function delete(obj)
            obj.Close();
        end
        function D = RGB2Depth(~,RGB)
            RGB = double(RGB);
            D = uint16(RGB(:,:,2)*256+RGB(:,:,3));
        end
    end
end