function Str = ReadString(fid)
n = fread(fid,1,'uint8');
Str = fread(fid,n,'*char')';
end