from utils import find_mini_rgbd
from minirgbd import MicroRGBD
from model.SML import SML
from render.renderer import Renderer

import torch
import numpy as np

import os

# Be sure that SML outputs R_cube


if __name__ == "__main__":        
    device = torch.device('cpu')
    dtype = torch.float
    poses = find_mini_rgbd(os.path.join('MINI-RGBD_web'))
    poses = [MicroRGBD(pose) for pose in poses[:2]]

    # Load model. 
    model = SML('./model.pkl').to(device, dtype)
    vbeta = torch.tensor(np.array([pose.beta for pose in poses])).to(device, dtype, non_blocking=True)
    vpose = torch.tensor(np.array([pose.pose for pose in poses])).to(device, dtype, non_blocking=True)
    vtrans = torch.tensor(np.array([pose.trans for pose in poses])).to(device, dtype, non_blocking=True)

    vpose[...] = 0

    pos_3d = np.array([pose.joints_3d for pose in poses])

    renderer = Renderer(model.f, 640, 640, visible=False)
    v, Jtr, R_cube = model(vbeta, vpose, vtrans) # If this fails modify model, so it returns R_cube
    output = renderer.render(v)

    R_cube = R_cube.numpy().transpose(3,2,1,0)

    import scipy.io
    scipy.io.savemat('mikkel_images/rotation.mat', mdict={'rot_matrices': R_cube, 'pose3d': Jtr.numpy()})    
    # rot_file = open('rotation.csv','w')
    # np.savetxt(R_cube.numpy())
    # rot_file.close()

    import PIL.Image as Image

    for idx in range(2):

        im = Image.fromarray(output[idx,...,0].numpy())
        im.save('mikkel_images/tpose'+str(idx+1)+'_depth.png',format='png')
