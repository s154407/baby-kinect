% Take folder name
% Get depth images and save in matrix
% Get labels (all pose positions) from HumanHandler and save in struct
% labels.Poses.Positions

% Make current folder /Volumes/Elements/BabyKinect/PROJECT_FOLDER

% loop through all babies
% loop through each date 
% generate matrix
% generate label


% Append KinectData/ to RH and TD 

% Be in folder ex. PHD/

relative_seq_path = 'GM/Serialized/Depth.bin';
relative_label_path = 'GM/';

save_drive = '/Volumes/exFAT 1/';

height = 640;
width = 480;

D = dir;
D = D(~ismember({D.name}, {'.', '..'}));

% PHD
% Klara_240 
% Manfred_127
% Marcus_129
% Wilson too large
% RH
% Clara_599 no annotation
% TD
% Aya_536 no annotation
% Elliot_600 no annotation
% Frida_502 no annotation
% Gorm_506 no annotation
% Gudrun_588 no annotation
% Pelle
% Nohr
% Storm (1 af dem)
% Julius
for k = 1:numel(D)
    k
    currD = D(k).name
    
    path_to_baby = [D(k).folder '/' currD];
    
    subdirs = dir(path_to_baby);
    subdirs = subdirs(~ismember({subdirs.name}, {'.', '..'}));

    for i = 1:numel(subdirs)
        currSubDir = subdirs(i).name;
        path_to_date = [path_to_baby '/' currSubDir];
        path_to_seq = [path_to_baby '/' currSubDir '/' relative_seq_path];
        alt_path_to_seq = [path_to_baby '/' currSubDir '/GM/1/Serialized/Depth.bin'];
        
%         if exist([save_drive currD '_' currSubDir '.mat'],'file') || exist([save_drive currD '_' currSubDir '_flip.mat'],'file')
%             continue;
%         end 
% Comment for structure of Ellinor_208        
%         if exist(path_to_seq,'file')
%         elseif exist(alt_path_to_seq,'file')
%             sub_subdirs = sub_subdirs(~ismember({sub_subdirs.name}, {'.', '..'}));
%         end
        label_path = [path_to_date '/' relative_label_path];
        
        if exist(path_to_seq, 'file')
             if exist(label_path,'file')
            
                seq = MDOL.DeSerializer(fullfile(path_to_seq));

                [length,~] = size(seq.DepthTimeStamps);

                im = seq.GetImage(1);

                [dim1,dim2] = size(im);

                % matrix = zeros([height,width,length], 'uint16');
                matrix = zeros([dim1,dim2,length], 'uint16');

                for j = 1:length
                    matrix(:,:,j) = seq.GetImage(j); 
                end

                labels = HumanHandler(fullfile(label_path));

                positions = labels.Poses.Positions;
               
                if dim2 == height
                    save([save_drive currD '_' currSubDir '_flip.mat'], 'matrix','positions', '-v7.3');
                else
                    save([save_drive currD '_' currSubDir '.mat'], 'matrix','positions', '-v7.3');
                end


             else
                continue;
             end
        else 
            continue;
        end
    end

end

print('yay');

%% reformat data

save_drive = '/Volumes/exFAT/';
D = dir;
D = D(~ismember({D.name}, {'.', '..','$RECYCLE.BIN','.Trashes','.Spotlight-V100','.fseventsd' 'System Volume Information'}));
for k = 1:numel(D)
    k
    currFileName = D(k).name
    
    currFile = matfile(currFileName, 'Writable', true);
    pos = currFile.positions
    new_pos = cell2mat(struct2cell(pos));
    save(currFileName,'-append','new_pos');

    
end

