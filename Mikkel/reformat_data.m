%% reformat data again  

% To correctly align timestamps:
% Load original file
% get depth time stamp
% get label time stamp
% find which one's shortest
% check that all of the shortest is contained in the largest
% we assume that the labelling is sequential and doesn't break
% 

% Could also load generated data. Load only timestamps from old data and
% append to genereated mat-files. Then load files, do filtering on
% timestamps and then

% Algorithm to find sequences to train on
% min. 5 frames long.
% Start collecting 0, break when S(i) == 0 ?? No, see how long the gap is.
% If the gap is > 4
% Or generate list of all potential sequences. Then filter out the ones
% that are < 5.

% OR asssume that some amount of noise can help it generalize. If a small
% percentage of the training data has this issue, it probably won't inhibit
% leaning, and might promote generalization?? Dunno.

% Well anyway, when we have all the correct indices we sort out the files in the already generated dataset.

% Generate two new files with babyname_label_timestamp and
% babyname_image_timestamp



relative_seq_path = 'GM/KinectData/Serialized/Depth.bin';
relative_label_path = 'GM/KinectData/';

save_drive = '/Volumes/exFAT 2/';

height = 640;
width = 480;

D = dir;
D = D(~ismember({D.name}, {'.', '..'}));

% Asta_244 7
% Ellinor_208 11
% 

% Julies burde generes
% Markus ligs� 
% Wilson ogs�
% Zoey needs new_pos

% Vigga RH
for k = 1:numel(D)
    k
    currD = D(k).name
    
    path_to_baby = [D(k).folder '/' currD];
    
    subdirs = dir(path_to_baby);
    subdirs = subdirs(~ismember({subdirs.name}, {'.', '..'}));

    for i = 1:numel(subdirs)
        currSubDir = subdirs(i).name;
        path_to_date = [path_to_baby '/' currSubDir];
        path_to_seq = [path_to_baby '/' currSubDir '/' relative_seq_path];
        alt_path_to_seq = [path_to_baby '/' currSubDir '/GM/1/Serialized/Depth.bin'];
        
%         if exist([save_drive currD '_' currSubDir '.mat'],'file') || exist([save_drive currD '_' currSubDir '_flip.mat'],'file')
%             continue;
%         end 
% Comment for structure of Ellinor_208        
%         if exist(path_to_seq,'file')
%         elseif exist(alt_path_to_seq,'file')
%             sub_subdirs = sub_subdirs(~ismember({sub_subdirs.name}, {'.', '..'}));
%         end
        label_path = [path_to_date '/' relative_label_path];
        
        if exist(path_to_seq, 'file')
             if exist([label_path 'X_1Long_62Double.bin'],'file')
            
                seq = MDOL.DeSerializer(fullfile(path_to_seq));

                [length,~] = size(seq.DepthTimeStamps);
                
                im = seq.GetImage(1);

                [dim1,dim2] = size(im);

                labels = HumanHandler(fullfile(label_path));

                positions = labels.Poses.Positions;
                
                ts_d = seq.DepthTimeStamps;
                ts_l = labels.TimeStamps';
                limit = min(size(ts_d,1), size(ts_l,1));
                [ts_values, ts_idx] = intersect(ts_d,ts_l);
                
                [~,id_img]=ismember(ts_d,ts_l);
                [~,id_pos]=ismember(ts_l,ts_d);
                
                if dim2 == height || dim2 == 1920
                    genfile = matfile([save_drive currD '_' currSubDir '_flip.mat']);
                else
                    genfile = matfile([save_drive currD '_' currSubDir '.mat']);
                end
                
                filtered_matrix = genfile.matrix; %index with id_img
                filtered_matrix = filtered_matrix(:,:,id_img > 0); %index with id_img

                filtered_pos = genfile.new_pos;
                filtered_pos = filtered_pos(:, id_pos > 0);
                timestamps = ts_values;
                
                [~,~,sz_img] = size(filtered_matrix);
                [~,sz_pos] = size(filtered_pos);
                
                if sz_img ~= sz_pos
                    print('ERROR NOT SAME SIZE');
                    break;
                end


                % Generate index list of where timestamps reside in img and
                % label
                
                % Make logical array of same length to include or not

                if dim2 == height || dim2 == 1920
                    save([save_drive currD '_' currSubDir '_flip.mat'],'-append','filtered_matrix','filtered_pos', 'timestamps');
                else
                    save([save_drive currD '_' currSubDir '.mat'],'-append','filtered_matrix','filtered_pos', 'timestamps');
                end


             else
                continue;
             end
        else 
            continue;
        end
    end

end
