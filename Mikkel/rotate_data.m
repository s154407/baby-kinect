%%


relative_seq_path = 'GM/Serialized/Depth.bin';
relative_label_path = 'GM/';

baby_direction = readtable('/Users/nsturis/baby-kinect/mikkel/UpDirection.csv');

save_drive = '/Volumes/exFAT/';

height = 640;
width = 480;

D = dir;
D = D(~ismember({D.name}, {'.', '..'}));

for k = 1:69
    k
    currD = char(baby_direction.Var1(k))
    
    path_to_baby = ['/Volumes/Elements/BabyKinect/PHD/' currD];
 
    currSubDir = num2str(baby_direction.Var2(k))
    path_to_date = [path_to_baby '/' currSubDir];
    path_to_seq = [path_to_baby '/' currSubDir '/' relative_seq_path];
    alt_path_to_seq = [path_to_baby '/' currSubDir '/GM/1/Serialized/Depth.bin'];
    label_path = [path_to_date '/' relative_label_path];

    if exist(path_to_seq, 'file')
         if exist([label_path 'X_1Long_62Double.bin'],'file')
             if exist([save_drive currD '_' currSubDir '.mat'],'file') || exist([save_drive currD '_' currSubDir '_flip.mat'],'file')

%                     if [currD '_' currSubDir] ~= [char(baby_direction.Var1(j)) '_' num2str(baby_direction.Var2(j))]
%                         [currD '_' currSubDir]
%                         [char(baby_direction.Var1(j)) '_' num2str(baby_direction.Var2(j))]
%                         print('WRONG FILE OR FOLDER')
%                         break;
%                     end
                UP = char(baby_direction.Var3(k));
                seq = MDOL.DeSerializer(fullfile(path_to_seq));

                [length,~] = size(seq.DepthTimeStamps);

                im = seq.GetImage(1);

                [dim1,dim2] = size(im);

                human = HumanHandler(fullfile(label_path));
                angle = 0;

                switch UP

                    case 'S'

                        angle = -pi;

                    case 'E'

                        angle = -pi/2;

                    case 'W'

                        angle = pi/2;

                end


                human.Data(1:3,:) = eul2rotm([-angle 0 0])*human.Data(1:3,:);

                human.Data(9,:) = human.Data(9,:)+angle;

                human.UpdatePoses();

                positions = human.Poses.Positions;

                ts_d = seq.DepthTimeStamps;
                ts_l = human.TimeStamps';
                limit = min(size(ts_d,1), size(ts_l,1));
                [ts_values, ts_idx] = intersect(ts_d,ts_l);

                [~,id_img]=ismember(ts_d,ts_l);
                [~,id_pos]=ismember(ts_l,ts_d);

                if dim2 == height || dim2 == 1920
                    genfile = matfile([save_drive currD '_' currSubDir '_flip.mat']);
                else
                    genfile = matfile([save_drive currD '_' currSubDir '.mat']);
                end

                rotated_pos = cell2mat(struct2cell(positions));
                rotated_pos = rotated_pos(:, id_pos > 0);

                % Generate index list of where timestamps reside in img and
                % label

                % Make logical array of same length to include or not

                if dim2 == height || dim2 == 1920
                    save([save_drive currD '_' currSubDir '_flip.mat'],'-append','rotated_pos');
                else
                    save([save_drive currD '_' currSubDir '.mat'],'-append','rotated_pos');
                end

             else
                 continue;
             end;
         else
            continue;
         end
    else 
        continue;
    end

end