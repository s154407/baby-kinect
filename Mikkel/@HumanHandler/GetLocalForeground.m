function QNew = GetLocalForeground(obj,f)
Start = cell(12,1);
Start{1} = obj.Poses.Positions(f).BodyCenter;
Start{2} = obj.Poses.Positions(f).HeadCenter;
Start{3} = obj.Poses.Positions(f).LUArmStart;
Start{4} = obj.Poses.Positions(f).LLArmStart;
Start{5} = obj.Poses.Positions(f).RUArmStart;
Start{6} = obj.Poses.Positions(f).RLArmStart;
Start{7} = obj.Poses.Positions(f).LULegStart;
Start{8} = obj.Poses.Positions(f).LLLegStart;
Start{9} = obj.Poses.Positions(f).LFootStart;
Start{10} = obj.Poses.Positions(f).RULegStart;
Start{11} = obj.Poses.Positions(f).RLLegStart;
Start{12} = obj.Poses.Positions(f).RFootStart;

Basis = cell(12,1);
Basis{1} = obj.Poses.Bases(f).BodyBasis;
Basis{2} = obj.Poses.Bases(f).HeadBasis;
Basis{3} = obj.Poses.Bases(f).LUArmBasis;
Basis{4} = obj.Poses.Bases(f).LLArmBasis;
Basis{5} = obj.Poses.Bases(f).RUArmBasis;
Basis{6} = obj.Poses.Bases(f).RLArmBasis;
Basis{7} = obj.Poses.Bases(f).LULegBasis;
Basis{8} = obj.Poses.Bases(f).LLLegBasis;
Basis{9} = obj.Poses.Bases(f).LFootBasis;
Basis{10} = obj.Poses.Bases(f).RULegBasis;
Basis{11} = obj.Poses.Bases(f).RLLegBasis;
Basis{12} = obj.Poses.Bases(f).RFootBasis;

Q = obj.GetForeground(f);
QNew = zeros(size(Q));
Type = obj.GetID(f);
if size(Q,2) == length(Type)
    for t = 1:12
        q = Q(:,Type==t);
        qnew = Basis{t}\(q-repmat(Start{t},1,size(q,2)));
        QNew(:,Type==t) = qnew;
    end
end
end