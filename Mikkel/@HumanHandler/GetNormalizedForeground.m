function [QNew,NewStart] = GetNormalizedForeground(obj,f)
Start = cell(12,1);
Start{1} = obj.Poses.Positions(f).BodyCenter;
Start{2} = obj.Poses.Positions(f).HeadCenter;
Start{3} = obj.Poses.Positions(f).LUArmStart;
Start{4} = obj.Poses.Positions(f).LLArmStart;
Start{5} = obj.Poses.Positions(f).RUArmStart;
Start{6} = obj.Poses.Positions(f).RLArmStart;
Start{7} = obj.Poses.Positions(f).LULegStart;
Start{8} = obj.Poses.Positions(f).LLLegStart;
Start{9} = obj.Poses.Positions(f).LFootStart;
Start{10} = obj.Poses.Positions(f).RULegStart;
Start{11} = obj.Poses.Positions(f).RLLegStart;
Start{12} = obj.Poses.Positions(f).RFootStart;

Basis = cell(12,1);
Basis{1} = obj.Poses.Bases(f).BodyBasis*diag(obj.Poses.Variables(f).BodySize);
Basis{2} = obj.Poses.Bases(f).HeadBasis*diag(obj.Poses.Variables(f).HeadSize([1 1 1]));
Basis{3} = obj.Poses.Bases(f).LUArmBasis*diag(obj.Poses.Variables(f).UArmSize([2 1 2]));
Basis{4} = obj.Poses.Bases(f).LLArmBasis*diag(obj.Poses.Variables(f).LArmSize([2 1 2]));
Basis{5} = obj.Poses.Bases(f).RUArmBasis*diag(obj.Poses.Variables(f).UArmSize([2 1 2]));
Basis{6} = obj.Poses.Bases(f).RLArmBasis*diag(obj.Poses.Variables(f).LArmSize([2 1 2]));
Basis{7} = obj.Poses.Bases(f).LULegBasis*diag(obj.Poses.Variables(f).ULegSize([2 1 2]));
Basis{8} = obj.Poses.Bases(f).LLLegBasis*diag(obj.Poses.Variables(f).LLegSize([2 1 2]));
Basis{9} = obj.Poses.Bases(f).LFootBasis*diag(obj.Poses.Variables(f).FootSize([2 1 2]));
Basis{10} = obj.Poses.Bases(f).RULegBasis*diag(obj.Poses.Variables(f).ULegSize([2 1 2]));
Basis{11} = obj.Poses.Bases(f).RLLegBasis*diag(obj.Poses.Variables(f).LLegSize([2 1 2]));
Basis{12} = obj.Poses.Bases(f).RFootBasis*diag(obj.Poses.Variables(f).FootSize([2 1 2]));

NewStart = cell(12,1);
NewStart{1} = [0,0,1]';
Variables = HumanHandler.GetNormalizedVariables();

NewBasis = cell(12,1);
NewBasis{1} = eye(3)*diag(Variables.BodySize);
NewBasis{2} = eye(3)*diag(Variables.HeadSize([1 1 1]));
NewBasis{3} = [0 1 0
    1 0 0
    0 0 1]*diag(Variables.UArmSize([2 1 2]));
NewBasis{4} = [0 1 0
    1 0 0
    0 0 1]*diag(Variables.LArmSize([2 1 2]));
NewBasis{5} = [0 -1 0
    1 0 0
    0 0 1]*diag(Variables.UArmSize([2 1 2]));
NewBasis{6} = [0 -1 0
    1 0 0
    0 0 1]*diag(Variables.LArmSize([2 1 2]));
NewBasis{7} = [1 0 0
    0 -1 0
    0 0 1]*diag(Variables.ULegSize([2 1 2]));
NewBasis{8} = [1 0 0
    0 -1 0
    0 0 1]*diag(Variables.LLegSize([2 1 2]));
NewBasis{9} = [1 0 0
    0 -1 0
    0 0 1]*diag(Variables.FootSize([2 1 2]));
NewBasis{10} = [-1 0 0
    0 -1 0
    0 0 1]*diag(Variables.ULegSize([2 1 2]));
NewBasis{11} = [-1 0 0
    0 -1 0
    0 0 1]*diag(Variables.LLegSize([2 1 2]));
NewBasis{12} = [-1 0 0
    0 -1 0
    0 0 1]*diag(Variables.FootSize([2 1 2]));

Q = obj.GetForeground(f);
QNew = zeros(size(Q));
Type = obj.GetID(f);
if size(Q,2) == length(Type)
    for t = 1:12
        q = Q(:,Type==t);
        qnew = NewBasis{t}*(Basis{t}\(q-repmat(Start{t},1,size(q,2))))+repmat(NewStart{t},1,size(q,2));
        QNew(:,Type==t) = qnew;
        switch t
            case 1
                NewStart{2} = NewBasis{t}*(Basis{t}\(Start{2}-Start{t}))+NewStart{t};
                NewStart{3} = NewBasis{t}*(Basis{t}\(Start{3}-Start{t}))+NewStart{t};
                NewStart{5} = NewBasis{t}*(Basis{t}\(Start{5}-Start{t}))+NewStart{t};
                NewStart{7} = NewBasis{t}*(Basis{t}\(Start{7}-Start{t}))+NewStart{t};
                NewStart{10} = NewBasis{t}*(Basis{t}\(Start{10}-Start{t}))+NewStart{t};
            case 3
                NewStart{4} = NewBasis{t}*(Basis{t}\(Start{4}-Start{t}))+NewStart{t};
            case 5
                NewStart{6} = NewBasis{t}*(Basis{t}\(Start{6}-Start{t}))+NewStart{t};
            case 7
                NewStart{8} = NewBasis{t}*(Basis{t}\(Start{8}-Start{t}))+NewStart{t};
            case 8
                NewStart{9} = NewBasis{t}*(Basis{t}\(Start{9}-Start{t}))+NewStart{t};
            case 10
                NewStart{11} = NewBasis{t}*(Basis{t}\(Start{11}-Start{t}))+NewStart{t};
            case 11
                NewStart{12} = NewBasis{t}*(Basis{t}\(Start{12}-Start{t}))+NewStart{t};
        end
    end
end
end