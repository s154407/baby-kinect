function Labels = LoadMDOL(obj)
Labels = false(12,obj.nFrames);
if exist([obj.Path '/MDOL.txt'],'file')
    fid = fopen([obj.Path '/MDOL.txt'],'r');
    X = textscan(fid,'%s'); X = X{1};
    fclose(fid);
    for l = 1:12
        s = strsplit(X{l},';');
        s = strsplit(s{2},',');
        for f = str2double(s)
            Labels(l,obj.TimeStamps==f) = true;
        end
    end
end
end