function RGB = GetForegroundRGB(obj,f)
[R,C] = obj.GetRC(f);
RGB = nan(3,length(R));
if exist([obj.Path '/DepthColorImages/' num2str(obj.TimeStamps(f)) '.jpg'],'file')
    im = imread([obj.Path '/DepthColorImages/' num2str(obj.TimeStamps(f)) '.jpg']);
    RGB = reshape(im,[],3)';
    RGB = RGB(:,sub2ind([480,640],R,C));
end
end