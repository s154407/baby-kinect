function [Distance,T,Q] = GetDistance(obj,f)

Variables = obj.Poses.Variables(f);
Positions = obj.Poses.Positions(f);
Types = {8,7,0,0,0,0,0,0,0,0,0,0};
Params = {[Variables.BodyCenter;obj.Poses.Bases(f).BodyBasis(:);Variables.BodySize]
    [Positions.HeadCenter;Variables.HeadSize]
    [Positions.LUArmStart;Positions.LLArmStart-Positions.LUArmStart;Variables.UArmSize(1)^2;Variables.UArmSize(2)]
    [Positions.LLArmStart;Positions.LHandStart-Positions.LLArmStart;Variables.LArmSize(1)^2;Variables.LArmSize(2)]
    [Positions.RUArmStart;Positions.RLArmStart-Positions.RUArmStart;Variables.UArmSize(1)^2;Variables.UArmSize(2)]
    [Positions.RLArmStart;Positions.RHandStart-Positions.RLArmStart;Variables.LArmSize(1)^2;Variables.LArmSize(2)]
    [Positions.LULegStart;Positions.LLLegStart-Positions.LULegStart;Variables.ULegSize(1)^2;Variables.ULegSize(2)]
    [Positions.LLLegStart;Positions.LFootStart-Positions.LLLegStart;Variables.LLegSize(1)^2;Variables.LLegSize(2)]
    [Positions.LFootStart;Positions.LToesStart-Positions.LFootStart;Variables.FootSize(1)^2;Variables.FootSize(2)]
    [Positions.RULegStart;Positions.RLLegStart-Positions.RULegStart;Variables.ULegSize(1)^2;Variables.ULegSize(2)]
    [Positions.RLLegStart;Positions.RFootStart-Positions.RLLegStart;Variables.LLegSize(1)^2;Variables.LLegSize(2)]
    [Positions.RFootStart;Positions.RToesStart-Positions.RFootStart;Variables.FootSize(1)^2;Variables.FootSize(2)]};

Q = obj.GetForeground(f);
nQ = size(Q,2);
Distance = inf(1,nQ);
T = -ones(1,nQ);
for t = 1:12
    Type = Types{t};
    gpuP = Params{t};
    for id = 1:nQ
        Qidx = Q(1,id);
        Qidy = Q(2,id);
        Qidz = Q(3,id);
        
        QidxC = Qidx - gpuP(1);
        QidyC = Qidy - gpuP(2);
        QidzC = Qidz - gpuP(3);
        if (Type == 0) % Cylinder
            lambda = (gpuP(4) * QidxC + gpuP(5) * QidyC + gpuP(6) * QidzC) / gpuP(7);
            if(lambda > 1)
                lambda = 1;
            elseif(lambda < 0)
                lambda = 0;
            end
            z = QidzC - lambda * gpuP(6);
            d = abs(sqrt(((QidxC - lambda * gpuP(4))^2 + (QidyC - lambda * gpuP(5))^2 + (z)^2)) - gpuP(8));
            d = d + max(z, 0);
        elseif (Type == 7) % Sphere
            d = abs(gpuP(4) - sqrt(QidxC^2 + QidyC^2 + QidzC^2));
        elseif (Type == 8) % DistanceMap
            QidxAbs = abs(QidxC * gpuP(4) + QidyC * gpuP(5) + QidzC * gpuP(6));
            QidyNotAbs = QidxC * gpuP(7) + QidyC * gpuP(8) + QidzC * gpuP(9);
            QidzAbs = abs(QidxC * gpuP(10) + QidyC * gpuP(11) + QidzC * gpuP(12));
            
            if (QidyNotAbs < 0)
                Xi = round(QidxAbs * 40.0 / (gpuP(13) * 2));
                Yi = round(-QidyNotAbs * 40.0 / (gpuP(14) * 2));
                Zi = round(QidzAbs * 40.0 / (gpuP(15) * 2));
                if (Xi < 40 && Yi < 40 && Zi < 40)
                    d = obj.DistanceMapLower(Xi+1, Yi+1, Zi+1);
                else
                    d = sqrt(QidxAbs^2 + QidyNotAbs^2 + QidzAbs^2);
                end
            else
                Xi = round(QidxAbs * 40.0 / (gpuP(13) * 2));
                Yi = round(QidyNotAbs * 40.0 / (gpuP(14) * 2));
                Zi = round(QidzAbs * 40.0 / (gpuP(15) * 2));
                if (Xi < 40 && Yi < 40 && Zi < 40)
                    d = obj.DistanceMapUpper(Xi+1, Yi+1, Zi+1);
                else
                    d = sqrt(QidxAbs^2 + QidyNotAbs^2 + QidzAbs^2);
                end
            end
        end
        if(Distance(id) > d)
            Distance(id) = d;
            T(id) = t;
        end
    end
end
end