function UpdateDistanceMap(obj,a,b,c)
xyzPoints = zeros(40 * 40 * 40,3);
xyzUpper = RoundCornerAbsDistanceGetXYZ(a,b,c);
xyzLower = EllipsoidAbsDistanceGetXYZ(a,b,c);
n = 0;
for i = 1:40
    for j = 1:40
        for k = 1:40
            n = n+1;
            xyzPoints(n, 1) = (i-1) / 40 * a * 2;
            xyzPoints(n, 2) = (j-1) / 40 * b * 2;
            xyzPoints(n, 3) = (k-1) / 40 * c * 2;
        end
    end
end
[~,distUpper] = knnsearch(xyzUpper,xyzPoints);
[~,distLower] = knnsearch(xyzLower,xyzPoints);

obj.DistanceMapUpper = zeros(40, 40, 40);
obj.DistanceMapUpper(:) = distUpper;
obj.DistanceMapLower = zeros(40, 40, 40);
obj.DistanceMapLower(:) = distLower;
end
function XYZ = RoundCornerAbsDistanceGetXYZ(a,b,c)
N = 64;
cT = zeros(N, 1);
sT1 = zeros(N, 1);
sT2 = zeros(N, 1);
for i = 1:N
    
    cT(i) = cos((i-1) * 0.5 * pi / (N - 1));
    sT1(i) = sin((i-1) * 0.5 * pi / (N - 1));
    sT2(i) = (sin((i-1) * 0.5 * pi / (N - 1)))^0.2;
end
X = (cT*sT2')'; X = X(:);
Z = (sT1*sT2')'; Z = Z(:);
Y = (ones(N, 1)*cT')'; Y = Y(:);
XYZ = zeros(N * N, 3);
for i = 1:(N * N)
    
    XYZ(i, 1) = X(i);
    XYZ(i, 2) = Y(i);
    XYZ(i, 3) = Z(i);
end

axis1 = [a,0,0];
axis2 = [0,b,0];
axis3 = [0,0,c];
R = zeros(3, 3);
for i = 1:3
    R(1, i) = axis1(i);
    R(2, i) = axis2(i);
    R(3, i) = axis3(i);
end
XYZ = XYZ*R;
end
function XYZ = EllipsoidAbsDistanceGetXYZ(a,b,c)
N = 64;
cT = zeros(N, 1);
sT = zeros(N, 1);
for i = 1:N
    cT(i) = cos((i-1) * pi / 2 / (N - 1));
    sT(i) = sin((i-1) * pi / 2 / (N - 1));
end
X = (cT*sT')'; X = X(:);
Y = (sT*sT')'; Y = Y(:);
Z = (ones(N, 1)*cT')'; Z = Z(:);
XYZ = zeros(N * N, 3);
for i = 1:(N*N)
    XYZ(i, 1) = X(i);
    XYZ(i, 2) = Y(i);
    XYZ(i, 3) = Z(i);
end
axis1 = [a,0,0];
axis2 = [0,b,0];
axis3 = [0,0,c];
R = zeros(3, 3);
for i = 1:3
    R(1, i) = axis1(i);
    R(2, i) = axis2(i);
    R(3, i) = axis3(i);
end
XYZ = XYZ*R;
end