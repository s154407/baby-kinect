function [R,C] = GetRC(obj,f)
if exist([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.rc'],'file')
    fid = fopen([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.rc'],'r');
    RC = fread(fid,[2,inf],'uint16');
    fclose(fid);
    R = RC(1,:)+1;
    C = RC(2,:)+1;
end
end