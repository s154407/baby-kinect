function [Foreground,R,C] = GetForeground(obj,f)
if(obj.CurrentForeground_f==f)
    Foreground = obj.CurrentForeground;
    R = obj.CurrentR;
    C = obj.CurrentC;
    return;
end
if exist([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.bin'],'file')
    fid = fopen([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.bin'],'r');
    Foreground = fread(fid,[3,inf],'single');
    fclose(fid);
    obj.CurrentForeground = Foreground;
    obj.CurrentForeground_f = f;
    if nargout > 1 && exist([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.rc'],'file')
        fid = fopen([obj.Path '/Foreground/' num2str(obj.TimeStamps(f)) '.rc'],'r');
        RC = fread(fid,[2,inf],'int16');
        fclose(fid);
        R = RC(1,:);
        C = RC(2,:);
        
        obj.CurrentR = R;
        obj.CurrentR_f = f;
        obj.CurrentC = C;
        obj.CurrentC_f = f;
    end
end
end