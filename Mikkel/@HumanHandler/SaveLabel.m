function SaveLabel(obj,Label)
if exist([obj.Path '/Video.avi'],'file')
    choice = 1;
    if exist([obj.Path '/' Label.Name '.lbl'],'file')
        choice = menu('Overwrite File?','Yes','No');
    end
    if(choice == 1)
        FileName = [obj.Path '/Video.avi'];
        bFileName = uint8(FileName);
        bFileName(2,:) = 0;
        fid = fopen([obj.Path '/' Label.Name '.lbl'],'w');
        fwrite(fid,length(bFileName)*2,'int32');
        fwrite(fid,bFileName,'uint8');
        fwrite(fid,length(Label.ID),'int32');
        fwrite(fid,Label.ID,'int32');
        fclose(fid);
    end
end
end