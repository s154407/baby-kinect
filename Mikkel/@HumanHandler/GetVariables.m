function Data = GetVariables(obj,varargin)
Data = [];
for i = 1:length(varargin)
    if isfield(obj.Poses.Variables,varargin{1})
        Data = [Data;obj.Poses.Variables.(varargin{i})];
    end
end
end