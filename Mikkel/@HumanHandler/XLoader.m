function X = XLoader(obj,XFile)
if(isempty(XFile))
    X = zeros(1,obj.nVariables);
else
    fid = fopen(XFile);
    X = textscan(fid,'%f'); X = X{1};
    fclose(fid);
end

X(end+1) = -1;
X(end+1) = -1;
end