function AddMDOL(obj)
MDOL = obj.LoadMDOL();

for l = 1:12
    if sum(MDOL(l,:))>0
        obj.AddLabel(@(x) MDOL(l,:)*1,['MDOL_' num2str(l)]);
    end
end
end