function [SegmentID,Type] = SegmentLimb(obj,f,N)
if nargin == 2
    N = 10;
end
Positions = obj.Poses.Positions(f);
Start{1} = Positions.Crotch;
End{1} = Positions.Chest;
Start{2} = Positions.HeadJoint;
End{2} = Positions.HeadCenter*2-Start{2};

Start{3} = Positions.LUArmStart;
End{3} = Positions.LLArmStart;
Start{4} = Positions.LLArmStart;
End{4} = Positions.LHandStart;
Start{5} = Positions.RUArmStart;
End{5} = Positions.RLArmStart;
Start{6} = Positions.RLArmStart;
End{6} = Positions.RHandStart;

Start{7} = Positions.LULegStart;
End{7} = Positions.LLLegStart;
Start{8} = Positions.LLLegStart;
End{8} = Positions.LFootStart;
Start{9} = Positions.LFootStart;
End{9} = Positions.LToesStart;
Start{10} = Positions.RULegStart;
End{10} = Positions.RLLegStart;
Start{11} = Positions.RLLegStart;
End{11} = Positions.RFootStart;
Start{12} = Positions.RFootStart;
End{12} = Positions.RToesStart;

Type = obj.GetID(f);
Q = obj.GetForeground(f);
SegmentID = zeros(1,length(Type));
for t = 1:12
    Qt = Q(:,Type==t);
    
    Start_t = Start{t};
    End_t = End{t};
    Dir = End_t-Start_t;
    l = Dir'*(Qt-repmat(Start_t,1,size(Qt,2)))/norm(Dir)^2;
    [~,SegmentID(Type==t)] = histc(l,[-inf linspace(0,1,N-1) inf]);
end
end