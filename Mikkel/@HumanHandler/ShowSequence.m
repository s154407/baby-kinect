function ShowSequence(obj,ImageOn,ZoomRange)
if nargin == 1
    ImageOn = false;
    ZoomRange = 0;
elseif nargin == 2
    ZoomRange = 0;
end

figure
subplot(121)
hPoseAxis = gca;
%cameratoolbar
%cameratoolbar('SetCoordSys','Y')

subplot(122)
hAngleAxis = gca;
h = line(1:obj.nFrames,obj.GetVariables('BodyCenter')');
set(h(1),'LineStyle','-.','Color',[1 0 0],'LineWidth',2);
set(h(2),'LineStyle','--','Color',[0 1 0],'LineWidth',2);
set(h(3),'LineStyle',':','Color',[0 0 1],'LineWidth',2);
hold on
h(4) = line([0 0],[-pi pi],'Color','r','LineWidth',2);
set(gca,'UserData',h);
ylim([-pi pi])
xlim([1 obj.nFrames]);

ShowFrame(obj,1,hPoseAxis,hAngleAxis,ImageOn,ZoomRange);
sldTime = uicontrol('Style','slider','Min',1,'Max',obj.nFrames,...
    'SliderStep',[1 5]./max(5,(obj.nFrames-1)),'Value',1,'UserData',1);
tmrPlayer = timer('ExecutionMode','fixedSpacing',...
    'TasksToExecute',inf,...
    'Period',0.03,...
    'TimerFcn',@(hObject,eventdata) SetTime(sldTime));
cmbAngle = uicontrol('Style', 'popup',...
    'String', obj.strAngles(:,1),...
    'Callback', @(hObject,eventdata) ChangeData(obj,get(hObject,'Value')));
if ~isempty(obj.Labels)
    cmbLabel  = uicontrol('Style', 'popup',...
        'String', {'None',obj.Labels.Name},...
        'Callback', @(hObject,eventdata) ChangeLabel(obj,get(hObject,'Value')));
else
    cmbLabel  = uicontrol('Style', 'popup',...
        'String', 'Disabled','Enable','inactive',...
        'Callback', @(hObject,eventdata) ChangeLabel(obj,get(hObject,'Value')));
end
chkPlay = uicontrol('Style', 'CheckBox',...
    'String', 'Play',...
    'BackgroundColor',[0.8 0.8 0.8],...
    'Callback', @(hObject,eventdata) StartStop(tmrPlayer));
set(gcf,'DeleteFcn',@(hObject,eventdata) stop(tmrPlayer));
set(gcf,'resizefcn',@(hObject,eventdata) Resize(get(hObject,'Position'),sldTime,cmbAngle,cmbLabel,chkPlay));
Resize(get(gcf,'Position'),sldTime,cmbAngle,cmbLabel,chkPlay)
set(gcf,'toolbar','figure');
addlistener(sldTime,'Value','PostSet',@(hObject,eventdata) ShowFrame(obj,round(get(sldTime,'Value')),hPoseAxis,hAngleAxis,ImageOn,ZoomRange));
end

function SetTime(Time)
dir = get(Time,'UserData');
time = get(Time,'Value')+dir;
if(time > get(Time,'Max') || time < 1)
    set(Time,'UserData',-dir);
else
    set(Time,'Value',time);
end
end

function ShowFrame(obj,f,hPoseAxis,hAngleAxis,ImageOn,ZoomRange)
if nargin == 4
    ImageOn = false;
    ZoomRange = 0;
elseif nargin == 5
    ZoomRange = 0;
end
if ImageOn
    if ~ischar(ImageOn)
        ImageOn = 'DepthColorImages';
    end
    if exist([obj.Path '/' ImageOn '/' num2str(obj.TimeStamps(f)) '.jpg'],'file')
        subplot(121)
        imshow(imread([obj.Path '/' ImageOn '/' num2str(obj.TimeStamps(f)) '.jpg']));
    end
else
    obj.ShowPose(f,hPoseAxis);
end
h = get(hAngleAxis,'UserData');
set(h(4),'XData',[f f]);
if ZoomRange ~= 0
    subplot(122)
    xlim([f-ZoomRange f+ZoomRange]);
end
set(get(hPoseAxis,'Title'),'string',num2str(obj.TimeStamps(f)));
end

function StartStop(Player)
if strcmpi(Player.Running,'off')
    start(Player);
else
    stop(Player);
end
end

function Resize(position,time,angle,label,play)
set(time,'Position',[20 20 position(3)-20*2 20]);
set(angle,'Position',[20 30 100 50]);
set(label,'Position',[20 60 100 50]);
set(play,'Position',[150 60 100 20]);
end

function ChangeData(obj,angleID)
data = obj.GetVariables(obj.strAngles{angleID,1});
subplot(122)
h = get(gca,'UserData');
for i = 1:size(data,1)
    set(h(i),'YData',data(i,:));
end
for j = (i+1):3
    set(h(j),'YData',data(i,:)*0);
end
end

function ChangeLabel(obj,labelid)
if(labelid == 1)
    h = get(gca,'UserData');
    if(length(h) == 5)
        set(h(5),'Vertices',[]);
        set(h(5),'Faces',[]);
        set(h(5),'FaceVertexCData',[]);
    end
    set(gca,'UserData',h);
else
    subplot(122);
    Label = obj.Labels(labelid-1);
    X = [1;1];
    l = Label.ID(1);
    L = l;
    for i = 2:length(Label.ID)
        if(Label.ID(i) ~= l)
            X(:,end+1) = [i-1;i-1];
            l = Label.ID(i);
            L(end+1) = l;
        end
    end
    X(:,end+1) = [i-1;i-1];
    Y = repmat([pi;-pi],1,size(X,2));
    patchinfo.Vertices = [X(:) Y(:)];
    patchinfo.Faces = [reshape((1:(size(patchinfo.Vertices,1)-2))',2,[])' fliplr(reshape((1:(size(patchinfo.Vertices,1)-2))',2,[])'+2)];
    cm = flipud(gray(max(L)+1));
    patchinfo.FaceColor = 'flat';
    patchinfo.FaceVertexCData = cm(L+1,:);
    patchinfo.EdgeColor = 'none';
    subplot(122)
    h = get(gca,'UserData');
    if(length(h) == 4)
        h(5) = patch(patchinfo);
    else
        set(h(5),'Vertices',patchinfo.Vertices);
        set(h(5),'Faces',patchinfo.Faces);
        set(h(5),'FaceVertexCData',patchinfo.FaceVertexCData);
    end
    set(gca,'UserData',h);
end
end