function Match = GetMatch(obj,f)
% Foreground1 = Human.GetForeground(f);
% Foreground2 = Human.GetForeground(f+1);
% Match = Human.GetMatch(f+1);
% flow = zeros(3,size(Foreground2,2));
% for i = 1:length(Match)
%     if Match(i) ~= -1
%         flow(:,i) = Foreground2(:,i)-Foreground1(:,Match(i)+1);
%     end
% end
Match = [];
if(obj.CurrentMatch_f==f)
    Match = obj.CurrentMatch;
    return;
end
if exist([obj.Path '/Match/' num2str(obj.TimeStamps(f)) '.bin'],'file')
    fid = fopen([obj.Path '/Match/' num2str(obj.TimeStamps(f)) '.bin'],'r');
    Match = fread(fid,[1,inf],'int32');
    fclose(fid);
    obj.CurrentMatch = Match;
    obj.CurrentMatch_f = f;
end
end