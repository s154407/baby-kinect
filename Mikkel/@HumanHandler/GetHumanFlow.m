function varargout = GetHumanFlow(obj,f,fOld,local)
if(nargin == 2)
    local = true;
    fOld = f-1;
elseif(nargin == 3)
    local = true;
end
frameid = [fOld,f];
if all(frameid>=1) && all(frameid<=obj.nFrames)
    Start = cell(12,2);
    Basis = cell(12,2);
    ParentBasis = cell(12,2);
    XYZ = cell(1,2);
    ID = cell(1,2);
    for i = 1:2
        f = frameid(i);
        ID{i} = obj.GetID(f);
        XYZ{i} = obj.GetForeground(f);
        Start{1,i} = obj.Poses.Positions(f).BodyCenter;
        Start{2,i} = obj.Poses.Positions(f).HeadCenter;
        Start{3,i} = obj.Poses.Positions(f).LUArmStart;
        Start{4,i} =obj.Poses.Positions(f).LLArmStart;
        Start{5,i} = obj.Poses.Positions(f).RUArmStart;
        Start{6,i} = obj.Poses.Positions(f).RLArmStart;
        Start{7,i} = obj.Poses.Positions(f).LULegStart;
        Start{8,i} = obj.Poses.Positions(f).LLLegStart;
        Start{9,i} = obj.Poses.Positions(f).LFootStart;
        Start{10,i} = obj.Poses.Positions(f).RULegStart;
        Start{11,i} = obj.Poses.Positions(f).RLLegStart;
        Start{12,i} = obj.Poses.Positions(f).RFootStart;
        
        Basis{1,i} = obj.Poses.Bases(f).BodyBasis;
        Basis{2,i} = obj.Poses.Bases(f).HeadBasis;
        Basis{3,i} = obj.Poses.Bases(f).LUArmBasis;
        Basis{4,i} = obj.Poses.Bases(f).LLArmBasis;
        Basis{5,i} = obj.Poses.Bases(f).RUArmBasis;
        Basis{6,i} = obj.Poses.Bases(f).RLArmBasis;
        Basis{7,i} = obj.Poses.Bases(f).LULegBasis;
        Basis{8,i} = obj.Poses.Bases(f).LLLegBasis;
        Basis{9,i} = obj.Poses.Bases(f).LFootBasis;
        Basis{10,i} = obj.Poses.Bases(f).RULegBasis;
        Basis{11,i} = obj.Poses.Bases(f).RLLegBasis;
        Basis{12,i} = obj.Poses.Bases(f).RFootBasis;
        
        ParentBasis{1,i} = eye(3);
        ParentBasis{2,i} = obj.Poses.Bases(f).BodyBasis;
        ParentBasis{3,i} = obj.Poses.Bases(f).BodyBasis;
        ParentBasis{4,i} = obj.Poses.Bases(f).LUArmBasis;
        ParentBasis{5,i} = obj.Poses.Bases(f).BodyBasis;
        ParentBasis{6,i} = obj.Poses.Bases(f).RUArmBasis;
        ParentBasis{7,i} = obj.Poses.Bases(f).BodyBasis;
        ParentBasis{8,i} = obj.Poses.Bases(f).LULegBasis;
        ParentBasis{9,i} = obj.Poses.Bases(f).LLLegBasis;
        ParentBasis{10,i} = obj.Poses.Bases(f).BodyBasis;
        ParentBasis{11,i} = obj.Poses.Bases(f).RULegBasis;
        ParentBasis{12,i} = obj.Poses.Bases(f).RLLegBasis;
    end
    
    F = zeros(3,size(XYZ{2},2));
    for j = 1:12
        QOld = XYZ{1}(:,ID{1}==j); QOldT = Basis{j,1}'*(QOld-repmat(Start{j,1},1,size(QOld,2)));
        Q = XYZ{2}(:,ID{2}==j); QT = Basis{j,2}'*(Q-repmat(Start{j,2},1,size(Q,2)));
        
        if ~isempty(QOld) && ~isempty(Q)
            
            %[~, ~, ~, ~, match] = icp(QOldT,QT);
            match2Old = knnsearch(QOldT',QT');
            matchFromOld = knnsearch(QT',QOldT');
            
            OK = matchFromOld(match2Old)==(1:length(match2Old))';
            
            if local
                QOld_Local = ParentBasis{j,1}'*(QOld-repmat(Start{j,1},1,size(QOld,2)));
                Q_Local = ParentBasis{j,2}'*(Q-repmat(Start{j,2},1,size(Q,2)));
                f = Q_Local-QOld_Local(:,match2Old);
            else
                f = Q-QOld(:,match2Old);
            end
            %f(:,~OK) = 0;
            F(:,ID{2}==j) = f;
        end
    end
    varargout{1} = F;
    varargout{2} = XYZ{2};
else
    varargout{1} = [];
    varargout{2} = [];
end