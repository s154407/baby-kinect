function DataLoader(obj,DataPath)
DataPath(end+1) = '/';
[obj.Data,obj.TimeStamps,obj.Date] = obj.XBinaryReader([DataPath 'X_1Long_62Double.bin']);
obj.nFrames = length(obj.TimeStamps);
[~,id] = sort(obj.TimeStamps);
obj.Data = obj.Data(:,id);
obj.UpdatePoses();
end