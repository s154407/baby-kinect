function ID = GetID(obj,f)
if(obj.CurrentID_f==f)
    ID = obj.CurrentID;
    return;
end
if exist([obj.Path '/ID/' num2str(obj.TimeStamps(f)) '.bin'],'file')
    fid = fopen([obj.Path '/ID/' num2str(obj.TimeStamps(f)) '.bin'],'r');
    ID = fread(fid,[1,inf],'uint8')+1;
    fclose(fid);
    obj.CurrentID = ID;
    obj.CurrentID_f = f;
end
end