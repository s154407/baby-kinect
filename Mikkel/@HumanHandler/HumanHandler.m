classdef HumanHandler < handle
    % write a description of the class here.
    properties
        % define the properties of the class here, (like fields of a struct)
        strVariables;
        strPositions;
        strAngles;
        nVariables;
        SizeVariables;
        cSumVariables;
        
        nFrames;
        Data;
        Angles;
        TimeStamps;
        Date;
        Poses;
        Labels;
        Path;
        Info;
    end
    properties (Access = private)
        DistanceMapUpper;
        DistanceMapLower;
        
        CurrentForeground;
        CurrentR;
        CurrentC;
        CurrentID;
        CurrentDistance;
        CurrentFlow;
        CurrentMatch;
        
        CurrentForeground_f;
        CurrentR_f;
        CurrentC_f;
        CurrentID_f;
        CurrentDistance_f;
        CurrentFlow_f;
        CurrentMatch_f;
    end
    methods
        % methods, including the constructor are defined in this block
        function obj = HumanHandler(DataPath)
            % Load Data			
            obj.LoadVariables();
            if ischar(DataPath)
                if exist([DataPath '/X_1Long_62Double.bin'],'file')
                    Update = true;
                    binInfo = dir([DataPath '/X_1Long_62Double.bin']);
                    matInfo = dir([DataPath '/Human.mat']);
                    if ~isempty(matInfo)
                        MatContent = load([DataPath '/' matInfo.name]);
                        if strcmpi(MatContent.XDate,binInfo.date)
                            Update = false;
                            load([DataPath '/' matInfo.name]);
                        end
                    end
                    if(Update)
                        obj.LoadVariables();
                        obj.DataLoader(DataPath);
                        obj.Path = DataPath;
                        obj.UpdateDistanceMap(obj.Poses.Variables(1).BodySize(1),obj.Poses.Variables(1).BodySize(2),obj.Poses.Variables(1).BodySize(3));
                        XDate = binInfo.date;
                        %save([DataPath '/Human.mat'],'obj','XDate');
                    end
                    % Load Extra
                    obj.LoadLabels(DataPath);
                    obj.Info = obj.LoadInfo(DataPath);
                else
                    error('Could not find "X_1Long_62Double.bin"');
                end
            else
                obj.Data = DataPath;
                obj.nFrames = size(DataPath,2);
                obj.TimeStamps = 1:obj.nFrames;
                obj.Labels = struct('Name',[],'ID',[]);
                obj.UpdatePoses();
            end
        end
        
        function LoadLabels(obj,DataPath)
            
            LabelFiles = dir([DataPath '/*.lbl']);
            Labels = struct('Name',[],'ID',[]);
            
            fid = fopen([DataPath '/TimeStamps.bin']);
            if(fid ~= -1)
                TimeStamps = fread(fid,[1,inf],'int64');
                fclose(fid);
                
                for i = 1:length(LabelFiles)
                    file = [DataPath '/' LabelFiles(i).name];
                    fid = fopen(file);
                    lFileName = fread(fid,[1,1],'int32');
                    FileName = fread(fid,[lFileName,1],'*char')';
                    nFrames = fread(fid,[1,1],'int32');
                    ID = fread(fid,[nFrames,1],'int32');
                    fclose(fid);
                    IDX = knnsearch(TimeStamps(:),obj.TimeStamps(:));
                    Labels(i).ID = ID(IDX);
                    Labels(i).Name = LabelFiles(i).name(1:end-4);
                end
            end
            if ~isempty(Labels(1).ID)
                obj.Labels = Labels;
            end
        end
        
        function objT = Transform(obj,fun)
            objT = obj;
            objT.Data = fun(obj.Data);
            objT.Labels = obj.Labels;
            objT.nFrames = size(objT.Data,2);
            objT.TimeStamps = 1:objT.nFrames;
            objT.DateModified = zeros(1,objT.nFrames);
            objT.UpdatePoses();
        end
        function objT = Take(obj,id)
            objT = obj;
            objT.Data = obj.Data(:,id);
            objT.TimeStamps = obj.TimeStamps(:,id);
            objT.DateModified = obj.DateModified(:,id);
            objT.Labels = obj.Labels;
            objT.nFrames = length(objT.TimeStamps);
            objT.Poses.Variables = obj.Poses.Variables(id);
            objT.Poses.Positions = obj.Poses.Positions(id);
            objT.Angles = obj.Angles(:,id);
        end
        function AddFeature(obj,fun,name)
            Output = fun(obj.Angles);
            OutputCell = mat2cell(Output,size(Output,1),ones(1,size(Output,2)));
            [obj.Poses.Variables.(name)] = OutputCell{:};
            obj.strAngles{end+1} = name;
        end
        function Normalize(obj)
            Variables = obj.GetNormalizedVariables();
            obj.Data(1:3,:) = repmat(Variables.BodyCenter,1,obj.nFrames);
            obj.Data(4:6,:) = repmat(Variables.BodySize,1,obj.nFrames);
            obj.Data(7:9,:) = repmat(Variables.BodyAngles,1,obj.nFrames);
            obj.Data(10:12,:) = repmat(Variables.HeadLocation,1,obj.nFrames);
            obj.Data(13,:) = repmat(Variables.HeadSize,1,obj.nFrames);
            obj.Data(14:16,:) = repmat(Variables.HeadAngles,1,obj.nFrames);
            obj.Data(17:19,:) = repmat(Variables.ArmLocation,1,obj.nFrames);
            obj.Data(20:21,:) = repmat(Variables.UArmSize,1,obj.nFrames);
            obj.Data(22:24,:) = repmat(Variables.LUArmAngles,1,obj.nFrames);
            obj.Data(25:27,:) = repmat(Variables.RLArmAngles,1,obj.nFrames);
            obj.Data(28:29,:) = repmat(Variables.LArmSize,1,obj.nFrames);
            obj.Data(30:32,:) = repmat(Variables.LLArmAngles,1,obj.nFrames);
            obj.Data(33:35,:) = repmat(Variables.RLArmAngles,1,obj.nFrames);
            obj.Data(36:38,:) = repmat(Variables.LegLocation,1,obj.nFrames);
            obj.Data(39:40,:) = repmat(Variables.ULegSize,1,obj.nFrames);
            obj.Data(41:43,:) = repmat(Variables.LULegAngles,1,obj.nFrames);
            obj.Data(44:46,:) = repmat(Variables.RULegAngles,1,obj.nFrames);
            obj.Data(47:48,:) = repmat(Variables.LLegSize,1,obj.nFrames);
            obj.Data(49:51,:) = repmat(Variables.LLLegAngles,1,obj.nFrames);
            obj.Data(52:54,:) = repmat(Variables.RLLegAngles,1,obj.nFrames);
            obj.Data(55:56,:) = repmat(Variables.FootSize,1,obj.nFrames);
            obj.Data(57:59,:) = repmat(Variables.LFootAngles,1,obj.nFrames);
            obj.Data(60:62,:) = repmat(Variables.RFootAngles,1,obj.nFrames);
            obj.UpdatePoses()
        end
        function objCopy = CopyInfo(obj)
            objCopy = obj;
            objCopy.nFrames = [];
            objCopy.Data = [];
            objCopy.Angles = [];
            objCopy.TimeStamps = [];
            objCopy.Poses = [];
            objCopy.Labels = [];
            objCopy.DistanceMapUpper = [];
            objCopy.DistanceMapLower = [];
        end
    end
    methods(Static)
        function Info = LoadInfo(DataPath)
            File = [DataPath '/Info.txt'];
            if exist(File,'file')
                %fid = fopen(File);
                %Raw = textscan(fid,'%s'); Raw = Raw{1};
                %fclose(fid);
                Raw = strsplit(fileread(File),'\r\n');
                Info.Raw = Raw;
                
                for i = 1:length(Raw)
                    if ~isempty(Raw{i})
                        infoline = strsplit(Raw{i},';');
                        if ismember(infoline{1},{'Name','Date','Birth','Due'})
                            Info.(infoline{1}) = infoline{2};
                        else
                            Info.(infoline{1}) = str2double(infoline{2});
                        end
                    end
                end
                
                if ~isfield(Info,'Due') && isfield(Info,'Birth')
                    Info.Due = Info.Birth;
                end
                Info.CorrectedAge = (datenum(Info.Date,'dd-mm-yyyy')-datenum(Info.Due,'dd-mm-yyyy'))/7;
            else
                Info = nan;
            end
        end
        
        function Variables = GetNormalizedVariables()
            Variables.BodyCenter = [0;0;1];
            Variables.BodySize = [0.08;0.15;0.07];
            Variables.BodyAngles = [0;0;0];
            Variables.HeadLocation = [0;1;0];
            Variables.HeadSize = 0.07;
            Variables.HeadAngles = [0;0;0];
            Variables.ArmLocation = [0.8;0.8;0];
            Variables.UArmSize = [0.1;0.03];
            Variables.LUArmAngles = [0;0;0];
            Variables.RUArmAngles = [0;0;0];
            Variables.LArmSize = [0.12;0.02];
            Variables.LLArmAngles = [0;0;0];
            Variables.RLArmAngles = [0;0;0];
            Variables.LegLocation = [0.6;-0.6;0];
            Variables.ULegSize = [0.09;0.035];
            Variables.LULegAngles = [0;0;0];
            Variables.RULegAngles = [0;0;0];
            Variables.LLegSize = [0.1;0.025];
            Variables.LLLegAngles = [0;0;0];
            Variables.RLLegAngles = [0;0;0];
            Variables.FootSize = [0.07;0.02];
            Variables.LFootAngles = [0;0;0];
            Variables.RFootAngles = [0;0;0];
        end
    end
end