function ShowStats(obj)
figure
MeanPose = obj.Transform(@(x)mean(x,2));
MedianPose = obj.Transform(@(x)median(x,2));
subplot(221)
MeanPose.ShowPose;
title('Mean Pose');
subplot(222)
MedianPose.ShowPose;
title('Median Pose');
subplot(223)
ShowAngleStd(obj,MeanPose);
title('Std Angles');
subplot(224)
ShowPositionStd(obj,MeanPose);
title('Std Positions');
end

function ShowPositionStd(obj,MeanPose)
StdPose = obj.Transform(@(x)std(x,[],2));

Body = norm(StdPose.Poses.Positions.Chest);
Head = norm(StdPose.Poses.Positions.HeadCenter);
LUArm = norm(StdPose.Poses.Positions.LLArmStart);
RUArm = norm(StdPose.Poses.Positions.RLArmStart);
LLArm = norm(StdPose.Poses.Positions.LHandStart);
RLArm = norm(StdPose.Poses.Positions.RHandStart);
LHip = norm(StdPose.Poses.Positions.LULegStart);
RHip = norm(StdPose.Poses.Positions.RULegStart);
LULeg = norm(StdPose.Poses.Positions.LLLegStart);
RULeg = norm(StdPose.Poses.Positions.RLLegStart);
LLLeg = norm(StdPose.Poses.Positions.LFootStart);
RLLeg = norm(StdPose.Poses.Positions.RFootStart);
LFoot = norm(StdPose.Poses.Positions.LToesStart);
RFoot = norm(StdPose.Poses.Positions.RToesStart);

MAX = max([Body Head LUArm RUArm LLArm RLArm LULeg RULeg LLLeg RLLeg LFoot RFoot])/10;

hPoseAxis = MeanPose.ShowPose;

h = get(hPoseAxis,'UserData');
set(h(1),'LineWidth',Body/MAX);
set(h(2),'LineWidth',Head/MAX);
set(h(3),'LineWidth',Body/MAX);
set(h(4),'LineWidth',LUArm/MAX);
set(h(5),'LineWidth',LLArm/MAX);
set(h(6),'LineWidth',Body/MAX);
set(h(7),'LineWidth',RUArm/MAX);
set(h(8),'LineWidth',RLArm/MAX);
set(h(9),'LineWidth',Body/MAX);
set(h(10),'LineWidth',LHip/MAX);
set(h(11),'LineWidth',LULeg/MAX);
set(h(12),'LineWidth',LLLeg/MAX);
set(h(13),'LineWidth',LFoot/MAX);
set(h(14),'LineWidth',RHip/MAX);
set(h(15),'LineWidth',RULeg/MAX);
set(h(16),'LineWidth',RLLeg/MAX);
set(h(17),'LineWidth',RFoot/MAX);
end
function ShowAngleStd(obj,MeanPose)
StdPose = obj.Transform(@(x)std(x,[],2));
Body = norm(StdPose.Poses.Variables.BodyAngles);
Head = norm(StdPose.Poses.Variables.HeadAngles);
LUArm = norm(StdPose.Poses.Variables.LUArmAngles);
RUArm = norm(StdPose.Poses.Variables.RUArmAngles);
LLArm = norm(StdPose.Poses.Variables.LLArmAngles);
RLArm = norm(StdPose.Poses.Variables.RLArmAngles);
LULeg = norm(StdPose.Poses.Variables.LULegAngles);
RULeg = norm(StdPose.Poses.Variables.RULegAngles);
LLLeg = norm(StdPose.Poses.Variables.LLLegAngles);
RLLeg = norm(StdPose.Poses.Variables.RLLegAngles);
LFoot = norm(StdPose.Poses.Variables.LFootAngles);
RFoot = norm(StdPose.Poses.Variables.RFootAngles);

MAX = max([Body Head LUArm RUArm LLArm RLArm LULeg RULeg LLLeg RLLeg LFoot RFoot])/10;

hPoseAxis = MeanPose.ShowPose;

h = get(hPoseAxis,'UserData');
set(h(1),'LineWidth',Body/MAX);
set(h(2),'LineWidth',Head/MAX);
set(h(3),'LineWidth',Body/MAX);
set(h(4),'LineWidth',LUArm/MAX);
set(h(5),'LineWidth',LLArm/MAX);
set(h(6),'LineWidth',Body/MAX);
set(h(7),'LineWidth',RUArm/MAX);
set(h(8),'LineWidth',RLArm/MAX);
set(h(9),'LineWidth',Body/MAX);
set(h(10),'LineWidth',Body/MAX);
set(h(11),'LineWidth',LULeg/MAX);
set(h(12),'LineWidth',LLLeg/MAX);
set(h(13),'LineWidth',LFoot/MAX);
set(h(14),'LineWidth',Body/MAX);
set(h(15),'LineWidth',RULeg/MAX);
set(h(16),'LineWidth',RLLeg/MAX);
set(h(17),'LineWidth',RFoot/MAX);
end