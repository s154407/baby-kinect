function UpdatePoses(obj)
Positions = struct(); Positions = Positions(ones(obj.nFrames,1));
Variables = struct(); Variables = Variables(ones(obj.nFrames,1));
Bases = struct(); Bases = Bases(ones(obj.nFrames,1));
for f = 1:obj.nFrames
    Variables(f).BodyCenter = obj.Data(1:3,f);
    Variables(f).BodySize = obj.Data(4:6,f);
    Variables(f).BodyAngles = obj.Data(7:9,f);
    Variables(f).HeadLocation = obj.Data(10:12,f);
    Variables(f).HeadSize = obj.Data(13,f);
    Variables(f).HeadAngles = obj.Data(14:16,f);
    Variables(f).ArmLocation = obj.Data(17:19,f);
    Variables(f).UArmSize = obj.Data(20:21,f);
    Variables(f).LUArmAngles = obj.Data(22:24,f);
    Variables(f).RUArmAngles = obj.Data(25:27,f);
    Variables(f).LArmSize = obj.Data(28:29,f);
    Variables(f).LLArmAngles = obj.Data(30:32,f);
    Variables(f).RLArmAngles = obj.Data(33:35,f);
    Variables(f).LegLocation = obj.Data(36:38,f);
    Variables(f).ULegSize = obj.Data(39:40,f);
    Variables(f).LULegAngles = obj.Data(41:43,f);
    Variables(f).RULegAngles = obj.Data(44:46,f);
    Variables(f).LLegSize = obj.Data(47:48,f);
    Variables(f).LLLegAngles = obj.Data(49:51,f);
    Variables(f).RLLegAngles = obj.Data(52:54,f);
    Variables(f).FootSize = obj.Data(55:56,f);
    Variables(f).LFootAngles = obj.Data(57:59,f);
    Variables(f).RFootAngles = obj.Data(60:62,f);
    
    Basis = RotateBasisExp([-1 0 0;0 1 0;0 0 -1],Variables(f).BodyAngles);
    RLBasis = RotateBasisExp(Basis, [0, 0, pi ]);
    RUBasis = RotateBasisExp(Basis, [ 0, 0, pi / 2 ]);
    HeadBasis = RotateBasisExp(Basis, Variables(f).HeadAngles);

    RUArmBasis = RotateBasisExp(RUBasis, Variables(f).RUArmAngles);
    RLArmBasis = RotateBasisExp(RUArmBasis, Variables(f).RLArmAngles);
    LUArmBasis = RotateBasisExp(RUBasis, Variables(f).LUArmAngles);
    LLArmBasis = RotateBasisExp(LUArmBasis, Variables(f).LLArmAngles);

    RULegBasis = RotateBasisExp(RLBasis, Variables(f).RULegAngles);
    RLLegBasis = RotateBasisExp(RULegBasis, Variables(f).RLLegAngles);
    RFootBasis = RotateBasisExp(RLLegBasis, Variables(f).RFootAngles);
    LULegBasis = RotateBasisExp(RLBasis, Variables(f).LULegAngles);
    LLLegBasis = RotateBasisExp(LULegBasis, Variables(f).LLLegAngles);
    LFootBasis = RotateBasisExp(LLLegBasis, Variables(f).LFootAngles);
    
    Positions(f).HeadJoint = Basis(:,1)*Variables(f).BodySize(1) *Variables(f).HeadLocation(1)+Basis(:,2)*Variables(f).BodySize(2) *Variables(f).HeadLocation(2)+Basis(:,3)*Variables(f).BodySize(3) *Variables(f).HeadLocation(3)+Variables(f).BodyCenter;
    Positions(f).HeadCenter = HeadBasis(:,2)*(Variables(f).HeadSize(1))+(Positions(f).HeadJoint);
    
    Positions(f).RUArmStart = Basis(:,1)*-Variables(f).BodySize(1) *Variables(f).ArmLocation(1)+Basis(:,2)*Variables(f).BodySize(2) *Variables(f).ArmLocation(2)+Basis(:,3)*Variables(f).BodySize(3) *Variables(f).ArmLocation(3)+Variables(f).BodyCenter;
    Positions(f).RLArmStart = RUArmBasis(:,2)*(Variables(f).UArmSize(1))+(Positions(f).RUArmStart);
    Positions(f).RHandStart = RLArmBasis(:,2)*(Variables(f).LArmSize(1))+(Positions(f).RLArmStart);
    Positions(f).LUArmStart = Basis(:,1)*Variables(f).BodySize(1) *Variables(f).ArmLocation(1)+Basis(:,2)*Variables(f).BodySize(2) *Variables(f).ArmLocation(2)+Basis(:,3)*Variables(f).BodySize(3) *Variables(f).ArmLocation(3)+Variables(f).BodyCenter;
    Positions(f).LLArmStart = RUBasis*(RUBasis'*LUArmBasis(:,2)*(Variables(f).UArmSize(1)).*[1;-1;1])+Positions(f).LUArmStart;
    Positions(f).LHandStart = RUBasis*(RUBasis'*LLArmBasis(:,2)*(Variables(f).LArmSize(1)).*[1;-1;1])+Positions(f).LLArmStart;
    
    Positions(f).RULegStart = Basis(:,1)*-Variables(f).BodySize(1) *Variables(f).LegLocation(1)+Basis(:,2)*Variables(f).BodySize(2) *Variables(f).LegLocation(2)+Basis(:,3)*Variables(f).BodySize(3) *Variables(f).LegLocation(3)+Variables(f).BodyCenter;
    Positions(f).RLLegStart = RULegBasis(:,2)*(Variables(f).ULegSize(1))+(Positions(f).RULegStart);
    Positions(f).RFootStart = RLLegBasis(:,2)*(Variables(f).LLegSize(1))+(Positions(f).RLLegStart);
    Positions(f).RToesStart = RFootBasis(:,2)*(Variables(f).FootSize(1))+(Positions(f).RFootStart);
    Positions(f).LULegStart = Basis(:,1)*Variables(f).BodySize(1) *Variables(f).LegLocation(1)+Basis(:,2)*Variables(f).BodySize(2) *Variables(f).LegLocation(2)+Basis(:,3)*Variables(f).BodySize(3) * Variables(f).LegLocation(3)+Variables(f).BodyCenter;
    Positions(f).LLLegStart = RLBasis*(RLBasis'*LULegBasis(:,2)*(Variables(f).ULegSize(1)).*[-1;1;1])+Positions(f).LULegStart;
    Positions(f).LFootStart = RLBasis*(RLBasis'*LLLegBasis(:,2)*(Variables(f).LLegSize(1)).*[-1;1;1])+Positions(f).LLLegStart;
    Positions(f).LToesStart = RLBasis*(RLBasis'*LFootBasis(:,2)*(Variables(f).FootSize(1)).*[-1;1;1])+Positions(f).LFootStart;
    
    Positions(f).Chest = (Positions(f).LUArmStart+Positions(f).RUArmStart)*0.5;
    Positions(f).Crotch = (Positions(f).LULegStart+Positions(f).RULegStart)*0.5;
    Positions(f).BodyCenter = Variables(f).BodyCenter;
    
    LUArmBasis = RUBasis*diag([1, -1, 1])*(RUBasis'*LUArmBasis);
    LLArmBasis = RUBasis*diag([1, -1, 1])*(RUBasis'*LLArmBasis);
    LULegBasis = RLBasis*diag([-1, 1, 1])*RLBasis'*LULegBasis;
    LLLegBasis = RLBasis*diag([-1, 1, 1])*RLBasis'*LLLegBasis;
    LFootBasis = RLBasis*diag([-1, 1, 1])*RLBasis'*LFootBasis;
    
    Bases(f).BodyBasis = Basis;
    Bases(f).RLBasis = RLBasis;
    Bases(f).RUBasis = RUBasis;
    Bases(f).HeadBasis = HeadBasis;
    
    Bases(f).RUArmBasis = RUArmBasis;
    Bases(f).RLArmBasis = RLArmBasis;
    Bases(f).LUArmBasis = LUArmBasis;
    Bases(f).LLArmBasis = LLArmBasis;
    
    Bases(f).RULegBasis = RULegBasis;
    Bases(f).RLLegBasis = RLLegBasis;
    Bases(f).RFootBasis = RFootBasis;
    Bases(f).LULegBasis = LULegBasis;
    Bases(f).LLLegBasis = LLLegBasis;
    Bases(f).LFootBasis = LFootBasis;
end
obj.Poses.Positions = Positions;
obj.Poses.Variables = Variables;
obj.Poses.Bases = Bases;
obj.Angles = [Variables.BodyAngles
    Variables.HeadAngles
    Variables.LUArmAngles
    Variables.RUArmAngles
    Variables.LLArmAngles
    Variables.RLArmAngles
    Variables.LULegAngles
    Variables.RULegAngles
    Variables.LLLegAngles
    Variables.RLLegAngles
    Variables.LFootAngles
    Variables.RFootAngles];
obj.strPositions = fieldnames(obj.Poses.Positions);
end

function Rot = RotateBasisExp(Basis,Angles)
Rot = Basis;
%for i = 1:3
%    Rot = ExpMap(Rot(:,i),Angles(i))*Rot;
%end
Rot = ExpMap(Rot(:,1),Angles(1))*Rot;
Rot = ExpMap(Rot(:,3),Angles(3))*Rot;
Rot = ExpMap(Rot(:,2),Angles(2))*Rot;
end

function R = ExpMap(w,t)
W = SkewSymmetric(w);
ct = cos(t);
st = sin(t);
R = eye(3)+W*st+W*W*(1-ct);
end

function W = SkewSymmetric(w)
W = [0 -w(3) w(2);w(3) 0 -w(1);-w(2) w(1) 0];
end