function Flow = GetFlow(obj,f,local)
if nargin == 2
    local = true;
end
if local
    mode = '/LocalFlow/';
else
    mode = '/Flow/';
end

if exist([obj.Path mode num2str(obj.TimeStamps(f)) '.bin'],'file')
    fid = fopen([obj.Path mode num2str(obj.TimeStamps(f)) '.bin'],'r');
    Flow = fread(fid,[3,inf],'single');
    fclose(fid);
else
    Flow = [];
end
end