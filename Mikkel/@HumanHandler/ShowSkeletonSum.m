function ShowSkeletonSum(obj)
Head = [obj.Poses.Positions.HeadCenter];
LArm{1} = [obj.Poses.Positions.LLArmStart];
RArm{1} = [obj.Poses.Positions.RLArmStart];
LArm{2} = [obj.Poses.Positions.LHandStart];
RArm{2} = [obj.Poses.Positions.RHandStart];
LLeg{1} = [obj.Poses.Positions.LULegStart];
RLeg{1} = [obj.Poses.Positions.RULegStart];
LLeg{2} = [obj.Poses.Positions.LLLegStart];
RLeg{2} = [obj.Poses.Positions.RLLegStart];
LLeg{3} = [obj.Poses.Positions.LFootStart];
RLeg{3} = [obj.Poses.Positions.RFootStart];
LLeg{4} = [obj.Poses.Positions.LToesStart];
RLeg{4} = [obj.Poses.Positions.RToesStart];

plot(Head(1,:),Head(2,:),'y');
hold on
id = 1:10:obj.nFrames;
for i = 1:2
    plot(LArm{i}(1,id),LArm{i}(2,id),'-','color',[0.5*i 0 0]);
    plot(RArm{i}(1,id),RArm{i}(2,id),'-','color',[0 0.5*i 0]);
end
for i = 1:4
    plot(LLeg{i}(1,id),LLeg{i}(2,id),'-','color',[0.25*i 0 1]);
    plot(RLeg{i}(1,id),RLeg{i}(2,id),'-','color',[0 0.25*i 1]);
end
end