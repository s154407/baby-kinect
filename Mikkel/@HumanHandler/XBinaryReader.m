function [X,Timestamps,Date] = XBinaryReader(obj,Filename)
if ~exist(Filename,'file')
    error(['"' Filename '" Not Found!']);
end
fileinfo = dir(Filename);
Date = fileinfo.date;
nVariables = 62;
nFrames = fileinfo.bytes/((nVariables+1)*8);
X = zeros(nVariables,nFrames);
Timestamps = zeros(1,nFrames);
fid = fopen(Filename,'r');
for f = 1:nFrames
    Timestamps(f) = fread(fid,1,'int64');
    X(:,f) = fread(fid,nVariables,'double');
end
fclose(fid);
end