function hPoseAxis = ShowPose(obj,p,hPoseAxis,offset)
if(nargin ==3)
    offset = [0;0;0];
elseif(nargin == 2)
    hPoseAxis = gca;
    offset = [0;0;0];
elseif(nargin == 1)
    p = 1;
    hPoseAxis = gca;
    offset = [0;0;0];
end
Positions = obj.Poses.Positions(p);
h = get(hPoseAxis,'UserData');
if isempty(h) || nargin ~= 3
    h = [];
    axes(hPoseAxis)
    hold on
    h(1) = PlotLimb([],offset+Positions.BodyCenter,offset+Positions.Chest);
    h(2) = PlotLimb([],offset+Positions.Chest,offset+Positions.HeadCenter);
    h(3) = PlotLimb([],offset+Positions.Chest,offset+Positions.LUArmStart);
    h(4) = PlotLimb([],offset+Positions.LUArmStart,offset+Positions.LLArmStart,'r');
    h(5) = PlotLimb([],offset+Positions.LLArmStart,offset+Positions.LHandStart,'r');
    h(6) = PlotLimb([],offset+Positions.Chest,offset+Positions.RUArmStart);
    h(7) = PlotLimb([],offset+Positions.RUArmStart,offset+Positions.RLArmStart,'g');
    h(8) = PlotLimb([],offset+Positions.RLArmStart,offset+Positions.RHandStart,'g');
    h(9) = PlotLimb([],offset+Positions.BodyCenter,offset+Positions.Crotch);
    h(10) = PlotLimb([],offset+Positions.Crotch,offset+Positions.LULegStart);
    h(11) = PlotLimb([],offset+Positions.LULegStart,offset+Positions.LLLegStart,'r');
    h(12) = PlotLimb([],offset+Positions.LLLegStart,offset+Positions.LFootStart,'r');
    h(13) = PlotLimb([],offset+Positions.LFootStart,offset+Positions.LToesStart,'r');
    h(14) = PlotLimb([],offset+Positions.Crotch,offset+Positions.RULegStart);
    h(15) = PlotLimb([],offset+Positions.RULegStart,offset+Positions.RLLegStart,'g');
    h(16) = PlotLimb([],offset+Positions.RLLegStart,offset+Positions.RFootStart,'g');
    h(17) = PlotLimb([],offset+Positions.RFootStart,offset+Positions.RToesStart,'g');
    axis equal
    xlim([-0.5 0.5])
    ylim([-0.5 0.5])
    zlim([0 1.5])
    set(hPoseAxis,'UserData',h);
else
    PlotLimb(h(1),offset+Positions.BodyCenter,offset+Positions.Chest);
    PlotLimb(h(2),offset+Positions.Chest,offset+Positions.HeadCenter);
    PlotLimb(h(3),offset+Positions.Chest,offset+Positions.LUArmStart);
    PlotLimb(h(4),offset+Positions.LUArmStart,offset+Positions.LLArmStart);
    PlotLimb(h(5),offset+Positions.LLArmStart,offset+Positions.LHandStart);
    PlotLimb(h(6),offset+Positions.Chest,offset+Positions.RUArmStart);
    PlotLimb(h(7),offset+Positions.RUArmStart,offset+Positions.RLArmStart);
    PlotLimb(h(8),offset+Positions.RLArmStart,offset+Positions.RHandStart);
    PlotLimb(h(9),offset+Positions.BodyCenter,offset+Positions.Crotch);
    PlotLimb(h(10),offset+Positions.Crotch,offset+Positions.LULegStart);
    PlotLimb(h(11),offset+Positions.LULegStart,offset+Positions.LLLegStart);
    PlotLimb(h(12),offset+Positions.LLLegStart,offset+Positions.LFootStart);
    PlotLimb(h(13),offset+Positions.LFootStart,offset+Positions.LToesStart);
    PlotLimb(h(14),offset+Positions.Crotch,offset+Positions.RULegStart);
    PlotLimb(h(15),offset+Positions.RULegStart,offset+Positions.RLLegStart);
    PlotLimb(h(16),offset+Positions.RLLegStart,offset+Positions.RFootStart);
    PlotLimb(h(17),offset+Positions.RFootStart,offset+Positions.RToesStart);
end
end

function h = PlotLimb(h,A,B,bonecolor,jointcolor)

if(nargin==3)
    jointcolor = 'r';
    bonecolor = 'b';
end
if(nargin==4)
    jointcolor = 'r';
end
    

if ~isempty(h)
    if length(A) == 2
        set(h,'XData',[A(1) B(1)],'YData',[A(2) B(2)]);
    else
        set(h,'XData',[A(1) B(1)],'YData',[A(2) B(2)],'ZData',[A(3) B(3)]);
    end
else
    if length(A) == 2
        h = line([A(1) B(1)],[A(2) B(2)],'Marker','.','Color',bonecolor,'MarkerEdgeColor',jointcolor,'LineWidth',2);
        %h = plot([A(1) B(1)],[A(2) B(2)],'.-','Color','b','MarkerEdgeColor','r');
    else
        h = line([A(1) B(1)],[A(2) B(2)],[A(3) B(3)],'Marker','.','Color',bonecolor,'MarkerEdgeColor',jointcolor,'LineWidth',2);
        %h = plot3([A(1) B(1)],[A(2) B(2)],[A(3) B(3)],'.-','Color','b','MarkerEdgeColor','r');
    end
end
end