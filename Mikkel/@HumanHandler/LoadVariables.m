function LoadVariables(obj)
obj.strVariables = {'BodyCenter',3
'BodySize',3
'BodyAngles',3
'HeadLocation',3
'HeadSize',1
'HeadAngles',3
'ArmLocation',3
'UArmSize',2
'LUArmAngles',3
'RUArmAngles',3
'LArmSize',2
'LLArmAngles',3
'RLArmAngles',3
'LegLocation',3
'ULegSize',2
'LULegAngles',3
'RULegAngles',3
'LLegSize',2
'LLLegAngles',3
'RLLegAngles',3
'FootSize',2
'LFootAngles',3
'RFootAngles',3};
obj.SizeVariables = [obj.strVariables{:,2}];
obj.nVariables = sum(obj.SizeVariables);
obj.cSumVariables = cumsum(obj.SizeVariables)-[obj.SizeVariables];
obj.strAngles = obj.strVariables(cellfun(@(x) ~isempty(x),strfind(obj.strVariables(:,1),'Angles')),1);
obj.strVariables = obj.strVariables(:,1);
end